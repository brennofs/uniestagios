<?php
require_once 'dao/CursoDAO.php';


class CursoModel {

	private $ID_CURSO;
	private $NOME_CURSO;
	private $DURACAO_CURSO;
	private $ID_INST;

	public function setIDCurso ($ID_CURSO) {
	    $this->ID_CURSO = $ID_CURSO;
		return $this;
	}

	public function getIDCurso () {
	    return $this->ID_CURSO;
	}

	public function setNomeCurso ($NOME_CURSO) {
		$this->NOME_CURSO = $NOME_CURSO;
		return $this;
	}

	public function getNomeCurso () {
	    return $this->NOME_CURSO;
	}

	public function setDuracaoCurso ($DURACAO_CURSO) {
	    $this->DURACAO_CURSO = $DURACAO_CURSO;
		return $this;
	}

	public function getDuracaoCurso () {
	    return $this->DURACAO_CURSO;
	}

	public function setIDInstCurso ($ID_INST) {
	    $this->ID_INST = $ID_INST;
		return $this;
	}

	public function getIDInstCurso () {
	    return $this->ID_INST;
	}

	public function listarCursos() {
	    $cursos = array();
	    $curso_DAO = new CursoDAO();
		$cursos = $curso_DAO->listarCursosDAO();
		
	    return $cursos;
	}

	public function removerCurso($ID_CURSO) {
		$curso_DAO = new CursoDAO();
		$curso_DAO->removerCursoDAO($ID_CURSO);
	}

	public function cadastrarCurso($curso) {
	    $curso_DAO = new CursoDAO();
	    $curso_DAO->cadastrarCursoDAO($curso);

	}

	public function visualizarAlterarCurso($ID_CURSO) {
		$curso_DAO = new CursoDAO();
		$info_curso = $curso_DAO->visualizarAlterarCursoDAO($ID_CURSO);
		
		return $info_curso;
	}

	public function alterarCurso($curso) {
		$curso_DAO = new CursoDAO();
	    $curso_DAO->alterarCursoDAO($curso);
	}

}
?>