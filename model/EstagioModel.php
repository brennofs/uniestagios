<?php
require_once 'dao/EstagioDAO.php';


class EstagioModel {

	private $ID_ESTAGIO;
	private $DURACAO;
	private $SIT_ESTAG; // (1 ativo, 2 finalizado, 3 cancelado)
	private $HORARIO_ENTRADA;
	private $HORARIO_SAIDA;
	private $HORAS_SEMANAIS;
	private $REPRESENTANTE_LEGAL;
	private $CARGO_REPRESENTANTE;
	private $SUPERVISOR_ESTAGIO;
	private $FORMACAO_ACADEMICA_SUPERVISOR;
	private $REGISTRO_PROFISSIONAL_SUPERVISOR;
	private $ORGAO_REGISTRO_SUPERVISOR;
	private $NUMERO_APOLICE;
	private $NOME_SEGURADORA;
	private $VALOR_SEGURO;
	private $VALOR_BOLSA;
	private $VALOR_VALE_TRANSPORTE;
	private $DATA_INICIO;
	private $DATA_FIM;
	private $DATA_CELEBRADA_APARTIR;
	private $MESES_ADITIVO;
	private $NOTIFICADA_POR; // (1 unidade concedente, 2 estagiário, 3 IFSP)
	private $ID_ALUNO;
	private $ID_VAGA;
	private $ID_INSTITUICAO;
	private $ID_UNIDADE_CONCEDENTE;
	private $NOME_ALUNO;
	private $NOME_VAGA;
	private $NOME_CURSO;
	private $NOME_EMPRESA;

	public function setIDEstagio ($ID_ESTAGIO) {
	    $this->ID_ESTAGIO = $ID_ESTAGIO;
		return $this;
	}

	public function getIDEstagio () {
	    return $this->ID_ESTAGIO;
	}

	public function setDuracao ($DURACAO) {
		$this->DURACAO = $DURACAO;
		return $this;
	}

	public function getDuracao () {
	    return $this->DURACAO;
	}

	public function setSitEstagio ($SIT_ESTAG) {
	    $this->SIT_ESTAG = $SIT_ESTAG;
		return $this;
	}

	public function getSitEstagio () {
	    return $this->SIT_ESTAG;
	}

	public function setHorarioEntrada ($HORARIO_ENTRADA) {
	    $this->HORARIO_ENTRADA = $HORARIO_ENTRADA;
		return $this;
	}

	public function getHorarioEntrada () {
	    return $this->HORARIO_ENTRADA;
	}

	public function setHorarioSaida ($HORARIO_SAIDA) {
	    $this->HORARIO_SAIDA = $HORARIO_SAIDA;
		return $this;
	}

	public function getHorarioSaida () {
	    return $this->HORARIO_SAIDA;
	}

	public function setHorasSemanais ($HORAS_SEMANAIS) {
	    $this->HORAS_SEMANAIS = $HORAS_SEMANAIS;
		return $this;
	}

	public function getHorasSemanais () {
	    return $this->HORAS_SEMANAIS;
	}

	public function setRepLegal ($REPRESENTANTE_LEGAL) {
	    $this->REPRESENTANTE_LEGAL = $REPRESENTANTE_LEGAL;
		return $this;
	}

	public function getRepLegal () {
	    return $this->REPRESENTANTE_LEGAL;
	}

	public function setCargoRep ($CARGO_REPRESENTANTE) {
	    $this->CARGO_REPRESENTANTE = $CARGO_REPRESENTANTE;
		return $this;
	}

	public function getCargoRep () {
	    return $this->CARGO_REPRESENTANTE;
	}

	public function setSupEstagio ($SUPERVISOR_ESTAGIO) {
	    $this->SUPERVISOR_ESTAGIO = $SUPERVISOR_ESTAGIO;
		return $this;
	}

	public function getSupEstagio () {
	    return $this->SUPERVISOR_ESTAGIO;
	}

	public function setFormAcaSup ($FORMACAO_ACADEMICA_SUPERVISOR) {
	    $this->FORMACAO_ACADEMICA_SUPERVISOR = $FORMACAO_ACADEMICA_SUPERVISOR;
		return $this;
	}

	public function getFormAcaSup () {
	    return $this->FORMACAO_ACADEMICA_SUPERVISOR;
	}

	public function setRegProfSup ($REGISTRO_PROFISSIONAL_SUPERVISOR) {
	    $this->REGISTRO_PROFISSIONAL_SUPERVISOR = $REGISTRO_PROFISSIONAL_SUPERVISOR;
		return $this;
	}

	public function getRegProfSup () {
	    return $this->REGISTRO_PROFISSIONAL_SUPERVISOR;
	}

	public function setOrgaoRegSup ($ORGAO_REGISTRO_SUPERVISOR) {
	    $this->ORGAO_REGISTRO_SUPERVISOR = $ORGAO_REGISTRO_SUPERVISOR;
		return $this;
	}

	public function getOrgaoRegSup () {
	    return $this->ORGAO_REGISTRO_SUPERVISOR;
	}

	public function setNumeroApolice ($NUMERO_APOLICE) {
	    $this->NUMERO_APOLICE = $NUMERO_APOLICE;
		return $this;
	}

	public function getNumeroApolice () {
	    return $this->NUMERO_APOLICE;
	}

	public function setNomeSeguradora ($NOME_SEGURADORA) {
	    $this->NOME_SEGURADORA = $NOME_SEGURADORA;
		return $this;
	}

	public function getNomeSeguradora () {
	    return $this->NOME_SEGURADORA;
	}

	public function setValorSeguro ($VALOR_SEGURO) {
	    $this->VALOR_SEGURO = $VALOR_SEGURO;
		return $this;
	}

	public function getValorSeguro () {
	    return $this->VALOR_SEGURO;
	}

	public function setValorBolsa ($VALOR_BOLSA) {
	    $this->VALOR_BOLSA = $VALOR_BOLSA;
		return $this;
	}

	public function getValorBolsa () {
	    return $this->VALOR_BOLSA;
	}

	public function setValorValeTransporte ($VALOR_VALE_TRANSPORTE) {
	    $this->VALOR_VALE_TRANSPORTE = $VALOR_VALE_TRANSPORTE;
		return $this;
	}

	public function getValorValeTransporte () {
	    return $this->VALOR_VALE_TRANSPORTE;
	}

	public function setDataInicio ($DATA_INICIO) {
	    $this->DATA_INICIO = $DATA_INICIO;
		return $this;
	}

	public function getDataInicio () {
	    return $this->DATA_INICIO;
	}

	public function setDataFim ($DATA_FIM) {
	    $this->DATA_FIM = $DATA_FIM;
		return $this;
	}

	public function getDataFim () {
	    return $this->DATA_FIM;
	}

	public function setDataCelebradaApartir ($DATA_CELEBRADA_APARTIR) {
	    $this->DATA_CELEBRADA_APARTIR = $DATA_CELEBRADA_APARTIR;
		return $this;
	}

	public function getDataCelebradaApartir () {
	    return $this->DATA_CELEBRADA_APARTIR;
	}

	public function setMesesAditivo ($MESES_ADITIVO) {
	    $this->MESES_ADITIVO = $MESES_ADITIVO;
		return $this;
	}

	public function getMesesAditivo () {
	    return $this->MESES_ADITIVO;
	}

	public function setNotificadaPor ($NOTIFICADA_POR) {
	    $this->NOTIFICADA_POR = $NOTIFICADA_POR;
		return $this;
	}

	public function getNotificadaPor () {
	    return $this->NOTIFICADA_POR;
	}

	public function setIDAlunoEstagio ($ID_ALUNO) {
	    $this->ID_ALUNO = $ID_ALUNO;
		return $this;
	}

	public function getIDAlunoEstagio () {
	    return $this->ID_ALUNO;
	}

	public function setIDVagaEstagio ($ID_VAGA) {
	    $this->ID_VAGA = $ID_VAGA;
		return $this;
	}

	public function getIDVagaEstagio () {
	    return $this->ID_VAGA;
	}

	public function setIDInstEstagio ($ID_INSTITUICAO) {
	    $this->ID_INSTITUICAO = $ID_INSTITUICAO;
		return $this;
	}

	public function getIDInstEstagio () {
	    return $this->ID_INSTITUICAO;
	}

	public function setIDUnidadeConcedenteEstagio ($ID_UNIDADE_CONCEDENTE) {
	    $this->ID_UNIDADE_CONCEDENTE = $ID_UNIDADE_CONCEDENTE;
		return $this;
	}

	public function getIDUnidadeConcedenteEstagio () {
	    return $this->ID_UNIDADE_CONCEDENTE;
	}

	public function setNomeAluno ($NOME_ALUNO) {
		$this->NOME_ALUNO = $NOME_ALUNO;
		return $this;
	}

	public function getNomeAluno () {
		return $this->NOME_ALUNO;
	}

	public function setNomeVaga ($NOME_VAGA) {
		$this->NOME_VAGA = $NOME_VAGA;
		return $this;
	}

	public function getNomeVaga () {
		return $this->NOME_VAGA;
	}

	public function setNomeCurso ($NOME_CURSO) {
		$this->NOME_CURSO = $NOME_CURSO;
		return $this;
	}

	public function getNomeCurso () {
		return $this->NOME_CURSO;
	}

	public function setNomeEmpresa ($NOME_EMPRESA) {
		$this->NOME_EMPRESA = $NOME_EMPRESA;
		return $this;
	}

	public function getNomeEmpresa () {
		return $this->NOME_EMPRESA;
	}

	public function buscarNomeAluno ($ID_ALUNO) {
		$estagio_DAO = new EstagioDAO();
		$nome_aluno = "";
		$nome_aluno = $estagio_DAO->buscarNomeAlunoDAO($ID_ALUNO);

		return $nome_aluno;
	}

	public function buscarNomeVaga ($ID_VAGA) {
		$estagio_DAO = new EstagioDAO();
		$nome_vaga = "";
		$nome_vaga = $estagio_DAO->buscarNomeVagaDAO($ID_VAGA);

		return $nome_vaga;
	}

	public function buscarNomeCurso ($ID_CURSO) {
		$estagio_DAO = new EstagioDAO();
		$nome_curso = "";
		$nome_curso = $estagio_DAO->buscarNomeCursoDAO($ID_CURSO);

		return $nome_curso;
	}

	public function buscarNomeEmpresa ($ID_VAGA) {
		$estagio_DAO = new EstagioDAO();
		$nome_vaga = "";
		$nome_vaga = $estagio_DAO->buscarNomeEmpresaDAO($ID_VAGA);

		return $nome_vaga;
	}

	public function cadastroEstagio($estagio) {
		$estagio_DAO = new EstagioDAO();
		$estagio_DAO->cadastroEstagioDAO($estagio);
	}

	public function listarEstagios() {
	    $estagios = array();
	    $estagio_DAO = new EstagioDAO();
		$estagios = $estagio_DAO->listarEstagiosDAO();
		
	    return $estagios;
	}

	public function listarEstagiosFinalizados() {
	    $estagios = array();
	    $estagio_DAO = new EstagioDAO();
		$estagios = $estagio_DAO->listarEstagiosFinalizadosDAO();
		
	    return $estagios;
	}

	public function listarEstagiosCancelados() {
	    $estagios = array();
	    $estagio_DAO = new EstagioDAO();
		$estagios = $estagio_DAO->listarEstagiosCanceladosDAO();
		
	    return $estagios;
	}

	public function listarEstagioAluno($ID_ALUNO) {
		$estagio_DAO = new EstagioDAO();
		$id_estagio = $estagio_DAO->listarEstagioAlunoDAO($ID_ALUNO);

		return $id_estagio;
	}

	public function visualizarAlterarEstagio($ID_ESTAGIO) {
		$estagio_DAO = new EstagioDAO();
		$info_estagio = $estagio_DAO->visualizarAlterarEstagioDAO($ID_ESTAGIO);
		
		return $info_estagio;
	}

	public function alterarEstagio($estagio) {
		$estagio_DAO = new EstagioDAO();
	    $estagio_DAO->alterarEstagioDAO($estagio);
	}

	public function cancelarEstagio($ID_ESTAGIO) {
		$estagio_DAO = new EstagioDAO();
		$estagio_DAO->cancelarEstagioDAO($ID_ESTAGIO);
	}

	public function finalizarEstagio($ID_ESTAGIO) {
		$estagio_DAO = new EstagioDAO();
		$estagio_DAO->finalizarEstagioDAO($ID_ESTAGIO);
	}

	public function AdicionarIDUnidadeConcedente($estagio) {
		$estagio_DAO = new EstagioDAO();
		$estagio_DAO->AdicionarIDUnidadeConcedenteDAO($estagio);
	}

	public function AdicionarDocumentoAceite($estagio) {
		$estagio_DAO = new EstagioDAO();
		$estagio_DAO->AdicionarDocumentoAceiteDAO($estagio);
	}

	public function AdicionarDocumentoAditivo($estagio) {
		$estagio_DAO = new EstagioDAO();
		$estagio_DAO->AdicionarDocumentoAditivoDAO($estagio);
	}

	public function AdicionarDocumentoRescisao($estagio) {
		$estagio_DAO = new EstagioDAO();
		$estagio_DAO->AdicionarDocumentoRescisaoDAO($estagio);
	}

}
?>