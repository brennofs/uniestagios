<?php
require_once 'dao/VagaDAO.php';


class VagaModel {

	private $ID_VAGA;
	private $NOME;
	private $EMPRESA;
	private $DESCRICAO;
	private $SALARIO;
	private $HOR_ENTRADA;
	private $HOR_SAIDA;
	private $BENEFICIOS;
	private $SIT_VAGA; // (1 ativa, 2 ocupada, 3 finalizada ou arquivada)
	private $TIPO_VAGA; // (1 estagio, 2 clt)
	private $ID_INSTITUICAO;
	private $OFERTA_VAGA;

	public function setIDVaga ($ID_VAGA) {
	    $this->ID_VAGA = $ID_VAGA;
		return $this;
	}

	public function getIDVaga () {
	    return $this->ID_VAGA;
	}

	public function setNomeVaga ($NOME) {
	    $this->NOME = $NOME;
		return $this;
	}

	public function getNomeVaga () {
	    return $this->NOME;
	}

	public function setEmpresaVaga ($EMPRESA) {
	    $this->EMPRESA = $EMPRESA;
		return $this;
	}

	public function getEmpresaVaga () {
	    return $this->EMPRESA;
	}

	public function setDescricaoVaga ($DESCRICAO) {
		$this->DESCRICAO = $DESCRICAO;
		return $this;
	}

	public function getDescricaoVaga () {
	    return $this->DESCRICAO;
	}

	public function setSalarioVaga ($SALARIO) {
		$this->SALARIO = $SALARIO;
		return $this;
	}

	public function getSalarioVaga () {
	    return $this->SALARIO;
	}

	public function setHorEntradaVaga ($HOR_ENTRADA) {
		$this->HOR_ENTRADA = $HOR_ENTRADA;
		return $this;
	}

	public function getHorEntradaVaga () {
	    return $this->HOR_ENTRADA;
	}

	public function setHorSaidaVaga ($HOR_SAIDA) {
		$this->HOR_SAIDA = $HOR_SAIDA;
		return $this;
	}

	public function getHorSaidaVaga () {
	    return $this->HOR_SAIDA;
	}

	public function setBeneficiosVaga ($BENEFICIOS) {
		$this->BENEFICIOS = $BENEFICIOS;
		return $this;
	}

	public function getBeneficiosVaga () {
	    return $this->BENEFICIOS;
	}

	public function setSituacaoVaga ($SIT_VAGA) {
		$this->SIT_VAGA = $SIT_VAGA;
		return $this;
	}

	public function getSituacaoVaga () {
	    return $this->SIT_VAGA;
	}

	public function setTipoVaga ($TIPO_VAGA) {
		$this->TIPO_VAGA = $TIPO_VAGA;
		return $this;
	}

	public function getTipoVaga () {
	    return $this->TIPO_VAGA;
	}

	public function setIDInstituicaoVaga ($ID_INSTITUICAO) {
		$this->ID_INSTITUICAO = $ID_INSTITUICAO;
		return $this;
	}

	public function getIDInstituicaoVaga () {
	    return $this->ID_INSTITUICAO;
	}

	public function setOfertaVaga ($OFERTA_VAGA) {
		$this->OFERTA_VAGA = $OFERTA_VAGA;
		return $this;
	}

	public function getOfertaVaga () {
	    return $this->OFERTA_VAGA;
	}

	public function listarVagas() {
	    $vagas = array();
	    $vaga_DAO = new VagaDAO();
		$vagas = $vaga_DAO->listarVagasDAO();
		
	    return $vagas;
	}

	public function listarVagasAluno() {
	    $vagas = array();
	    $vaga_DAO = new VagaDAO();
		$vagas = $vaga_DAO->listarVagasAlunoDAO();
		
	    return $vagas;
	}

	public function visualizarAlterarVaga($ID_VAGA) {
		$vaga_DAO = new VagaDAO();
		$info_vaga = $vaga_DAO->visualizarAlterarVagaDAO($ID_VAGA);
		
		return $info_vaga;
	}

	public function removerVaga($ID_VAGA) {
		$vaga_DAO = new VagaDAO();
		$vaga_DAO->removerVagaDAO($ID_VAGA);
	}

	public function cadastrarVaga($vaga) {

	    $vaga_DAO = new VagaDAO();
	    $vaga_DAO->cadastrarVagaDAO($vaga);

	}

	public function alterarVaga($vaga) {

		$vaga_DAO = new VagaDAO();
	    $vaga_DAO->alterarVagaDAO($vaga);
	}

}
?>