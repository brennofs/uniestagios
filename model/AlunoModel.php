<?php
require_once 'dao/AlunoDAO.php';


class AlunoModel {

	private $ID_ALUNO;
	private $RA;
	private $SENHA;
	private $NOME;
	private $DATA_NASC;
	private $RG;
	private $CPF;
	private $ESTADO;
	private $CIDADE;
	private $CEP;
	private $BAIRRO;
	private $FONE;
	private $CEL;
	private $EMAIL;
	private $PORTADOR_DEF; // (1 nao, 2 sim)
	private $ESTAG_OBRIG; // (1 nao, 2 sim)
	private $SEMESTRE;
	private $PERIODO; // (1 matutino, 2 vespertino, 3 noturno, 4 integral)
	private $SIT_MAT; // (1 cursando, 2 formado ou 3 trancado)
	private $SIT_ESTAG; // (1 nao estagiando, 2 estagiando)
	private $ID_INSTITUICAO;
	private $ID_CURSO;
	private $NOME_CURSO;

	public function setIDAluno ($ID_ALUNO) {
	    $this->ID_ALUNO = $ID_ALUNO;
		return $this;
	}

	public function getIDAluno () {
	    return $this->ID_ALUNO;
	}

	public function setRAAluno ($RA) {
	    $this->RA = $RA;
		return $this;
	}

	public function getRAAluno () {
	    return $this->RA;
	}

	public function setSenhaAluno ($SENHA) {
	    $this->SENHA = $SENHA;
		return $this;
	}

	public function getSenhaAluno () {
	    return $this->SENHA;
	}

	public function setNomeAluno ($NOME) {
		$this->NOME = $NOME;
		return $this;
	}

	public function getNomeAluno () {
	    return $this->NOME;
	}

	public function setDataNascAluno ($DATA_NASC) {
		$this->DATA_NASC = $DATA_NASC;
		return $this;
	}

	public function getDataNascAluno () {
	    return $this->DATA_NASC;
	}

	public function setRGAluno ($RG) {
		$this->RG = $RG;
		return $this;
	}

	public function getRGAluno () {
	    return $this->RG;
	}

	public function setCPFAluno ($CPF) {
		$this->CPF = $CPF;
		return $this;
	}

	public function getCPFAluno () {
	    return $this->CPF;
	}

	public function setEstadoAluno ($ESTADO) {
		$this->ESTADO = $ESTADO;
		return $this;
	}

	public function getEstadoAluno () {
	    return $this->ESTADO;
	}

	public function setCidadeAluno ($CIDADE) {
		$this->CIDADE = $CIDADE;
		return $this;
	}

	public function getCidadeAluno () {
	    return $this->CIDADE;
	}

	public function setCEPAluno ($CEP) {
		$this->CEP = $CEP;
		return $this;
	}

	public function getCEPAluno () {
	    return $this->CEP;
	}

	public function setBairroAluno ($BAIRRO) {
		$this->BAIRRO = $BAIRRO;
		return $this;
	}

	public function getBairroAluno () {
	    return $this->BAIRRO;
	}

	public function setFoneAluno ($FONE) {
		$this->FONE = $FONE;
		return $this;
	}

	public function getFoneAluno () {
	    return $this->FONE;
	}

	public function setCelularAluno ($CEL) {
		$this->CEL = $CEL;
		return $this;
	}

	public function getCelularAluno () {
	    return $this->CEL;
	}

	public function setEmailAluno ($EMAIL) {
		$this->EMAIL = $EMAIL;
		return $this;
	}

	public function getEmailAluno () {
	    return $this->EMAIL;
	}

	public function setPortadorDefAluno ($PORTADOR_DEF) {
		$this->PORTADOR_DEF = $PORTADOR_DEF;
		return $this;
	}

	public function getPortadorDefAluno () {
	    return $this->PORTADOR_DEF;
	}

	public function setEstagObrigAluno ($ESTAG_OBRIG) {
		$this->ESTAG_OBRIG = $ESTAG_OBRIG;
		return $this;
	}

	public function getEstagObrigAluno () {
	    return $this->ESTAG_OBRIG;
	}

	public function setSemestreAluno ($SEMESTRE) {
		$this->SEMESTRE = $SEMESTRE;
		return $this;
	}

	public function getSemestreAluno () {
	    return $this->SEMESTRE;
	}

	public function setPeriodoAluno ($PERIODO) {
		$this->PERIODO = $PERIODO;
		return $this;
	}

	public function getPeriodoAluno () {
	    return $this->PERIODO;
	}

	public function setSitMatriculaAluno ($SIT_MAT) {
		$this->SIT_MAT = $SIT_MAT;
		return $this;
	}

	public function getSitMatriculaAluno () {
	    return $this->SIT_MAT;
	}

	public function setSitEstagioAluno ($SIT_ESTAG) {
		$this->SIT_ESTAG = $SIT_ESTAG;
		return $this;
	}

	public function getSitEstagioAluno () {
	    return $this->SIT_ESTAG;
	}

	public function setIDInstAluno ($ID_INSTITUICAO) {
		$this->ID_INSTITUICAO = $ID_INSTITUICAO;
		return $this;
	}

	public function getIDInstAluno () {
	    return $this->ID_INSTITUICAO;
	}

	public function setIDCursoAluno ($ID_CURSO) {
		$this->ID_CURSO = $ID_CURSO;
		return $this;
	}

	public function getIDCursoAluno () {
	    return $this->ID_CURSO;
	}

	public function setNomeCursoAluno ($NOME_CURSO) {
		$this->NOME_CURSO = $NOME_CURSO;
		return $this;
	}

	public function getNomeCursoAluno () {
		return $this->NOME_CURSO;
	}
	
	public function buscarNomeCursoAluno ($ID_CURSO) {
		$aluno_DAO = new AlunoDAO();
		$nome_curso = "";
		$nome_curso = $aluno_DAO->buscarNomeCursoAlunoDAO($ID_CURSO);

		return $nome_curso;
	}

	public function buscarIDCursoAluno ($ID_ALUNO) {
		$aluno_DAO = new AlunoDAO();
		$id_curso = "";
		$id_curso = $aluno_DAO->buscarIDCursoAlunoDAO($ID_ALUNO);

		return $id_curso;
	}

	public function listarAlunos() {
	    $alunos = array();
	    $aluno_DAO = new AlunoDAO();
		$alunos = $aluno_DAO->listarAlunosDAO();
		
	    return $alunos;
	}

	public function removerAluno($ID_ALUNO) {
		$aluno_DAO = new AlunoDAO();
		$aluno_DAO->removerAlunoDAO($ID_ALUNO);
	}

	public function cadastrarAluno($aluno) {
	    $aluno_DAO = new AlunoDAO();
	    $aluno_DAO->cadastrarAlunoDAO($aluno);

	}

	public function visualizarAlterarAluno($ID_ALUNO) {
		$aluno_DAO = new AlunoDAO();
		$info_aluno = $aluno_DAO->visualizarAlterarAlunoDAO($ID_ALUNO);
		
		return $info_aluno;
	}

	public function alterarAluno($aluno) {
		$aluno_DAO = new AlunoDAO();
	    $aluno_DAO->alterarAlunoDAO($aluno);
	}

	public function mudarSitEstagioEstagiandoAluno($ID_ALUNO) {
		$aluno_DAO = new AlunoDAO();
	    $aluno_DAO->mudarSitEstagioEstagiandoAlunoDAO($ID_ALUNO);
	}

	public function mudarSitEstagioSemEstagioAluno($ID_ALUNO) {
		$aluno_DAO = new AlunoDAO();
	    $aluno_DAO->mudarSitEstagioSemEstagioAlunoDAO($ID_ALUNO);
	}

}
?>