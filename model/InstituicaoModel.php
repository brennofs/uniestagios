<?php
require_once 'dao/InstituicaoDAO.php';


class InstituicaoModel {

	private $ID_INST;
	private $LOGIN_INST;
	private $SENHA_INST;
	private $NOME_INST;
	private $CNPJ_INST;
	private $ESTADO_INST;
	private $CIDADE_INST;
	private $ENDERECO_INST;
	private $NUMERO_INST;
	private $BAIRRO_INST;
	private $TELEFONE;
	private $DATA_CADASTRO_INST;
	private $VALIDADA_INST; //(1 nao foi validada, 2 rejeitada, 3 validada)

	public function setIDInst ($ID_INST) {
	    $this->ID_INST = $ID_INST;
		return $this;
	}

	public function getIDInst () {
	    return $this->ID_INST;
	}

	public function setLoginInst ($LOGIN_INST) {
		$this->LOGIN_INST = $LOGIN_INST;
		return $this;
	}

	public function getLoginInst () {
	    return $this->LOGIN_INST;
	}

	public function setSenhaInst ($SENHA_INST) {
		$this->SENHA_INST = $SENHA_INST;
		return $this;
	}

	public function getSenhaInst () {
	    return $this->SENHA_INST;
	}

	public function setNomeInst ($NOME_INST) {
		$this->NOME_INST = $NOME_INST;
		return $this;
	}

	public function getNomeInst () {
	    return $this->NOME_INST;
	}

	public function setCNPJInst ($CNPJ_INST) {
		$this->CNPJ_INST = $CNPJ_INST;
		return $this;
	}

	public function getCNPJInst () {
	    return $this->CNPJ_INST;
	}

	public function setEstInst ($ESTADO_INST) {
		$this->ESTADO_INST = $ESTADO_INST;
		return $this;
	}

	public function getEstInst () {
	    return $this->ESTADO_INST;
	}

	public function setCidInst ($CIDADE_INST) {
		$this->CIDADE_INST = $CIDADE_INST;
		return $this;
	}

	public function getCidInst () {
	    return $this->CIDADE_INST;
	}

	public function setEndInst ($ENDERECO_INST) {
		$this->ENDERECO_INST = $ENDERECO_INST;
		return $this;
	}

	public function getEndInst () {
	    return $this->ENDERECO_INST;
	}

	public function setNumInst ($NUMERO_INST) {
		$this->NUMERO_INST = $NUMERO_INST;
		return $this;
	}

	public function getNumInst () {
	    return $this->NUMERO_INST;
	}

	public function setBairroInst ($BAIRRO_INST) {
		$this->BAIRRO_INST = $BAIRRO_INST;
		return $this;
	}

	public function getBairroInst () {
	    return $this->BAIRRO_INST;
	}

	public function setTelefoneInst ($TELEFONE) {
		$this->TELEFONE = $TELEFONE;
		return $this;
	}

	public function getTelefoneInst () {
	    return $this->TELEFONE;
	}

	public function setDataCadastroInst ($DATA_CADASTRO_INST) {
		$this->DATA_CADASTRO_INST = $DATA_CADASTRO_INST;
		return $this;
	}

	public function getDataCadastroInst () {
		return $this->DATA_CADASTRO_INST;
	}

	public function setValidadaInst ($VALIDADA_INST) {
		$this->VALIDADA_INST = $VALIDADA_INST;
		return $this;
	}

	public function getValidadaInst () {
	    return $this->VALIDADA_INST;
	}
	
	public function listarInstituicoesNaoValidadas() {
	    $instituicoes = array();
	    $instituicao_DAO = new InstituicaoDAO();
		$instituicoes = $instituicao_DAO->listarInstituicoesNaoValidadasDAO();
		
	    return $instituicoes;
	}

	public function listarInstituicoesValidadas() {
	    $instituicoes = array();
	    $instituicao_DAO = new InstituicaoDAO();
		$instituicoes = $instituicao_DAO->listarInstituicoesValidadasDAO();
		
	    return $instituicoes;
	}

	public function listarInstituicoesRejeitadas() {
	    $instituicoes = array();
	    $instituicao_DAO = new InstituicaoDAO();
		$instituicoes = $instituicao_DAO->listarInstituicoesRejeitadasDAO();
		
	    return $instituicoes;
	}

	public function validarInstituicao($ID_INST) {
		$instituicao_DAO = new InstituicaoDAO();
		$instituicao_DAO->validarInstituicaoDAO($ID_INST);
	}

	public function rejeitarInstituicao($ID_INST) {
		$instituicao_DAO = new InstituicaoDAO();
		$instituicao_DAO->rejeitarInstituicaoDAO($ID_INST);
	}

	public function removerInstituicao($ID_INST) {
		$instituicao_DAO = new InstituicaoDAO();
		$instituicao_DAO->removerInstituicaoDAO($ID_INST);
	}

	public function cadastrarInstituicao($instituicao) {

	    $instituicao_DAO = new InstituicaoDAO();
	    $instituicao_DAO->cadastrarInstituicaoDAO($instituicao);

	}

}
?>