<?php
require_once 'dao/UnidadeConcedenteDAO.php';


class UnidadeConcedenteModel {

	private $ID_UNIDADE_CONCEDENTE;
	private $RAZAO_SOCIAL;
	private $INSCRICAO_ESTADUAL;
	private $CNPJ;
	private $CPF;
	private $TELEFONE;
	private $ESTADO;
	private $CIDADE;
	private $CEP;
	private $ENDERECO;
	private $BAIRRO;

	public function setIDUnidadeConcedente ($ID_UNIDADE_CONCEDENTE) {
	    $this->ID_UNIDADE_CONCEDENTE = $ID_UNIDADE_CONCEDENTE;
		return $this;
	}

	public function getIDUnidadeConcedente () {
	    return $this->ID_UNIDADE_CONCEDENTE;
	}

	public function setRazaoSocial ($RAZAO_SOCIAL) {
	    $this->RAZAO_SOCIAL = $RAZAO_SOCIAL;
		return $this;
	}

	public function getRazaoSocial () {
	    return $this->RAZAO_SOCIAL;
	}

	public function setInscEstadual ($INSCRICAO_ESTADUAL) {
	    $this->INSCRICAO_ESTADUAL = $INSCRICAO_ESTADUAL;
		return $this;
	}

	public function getInscEstadual () {
	    return $this->INSCRICAO_ESTADUAL;
	}

	public function setCNPJ ($CNPJ) {
	    $this->CNPJ = $CNPJ;
		return $this;
	}

	public function getCNPJ () {
	    return $this->CNPJ;
	}

	public function setCPF ($CPF) {
	    $this->CPF = $CPF;
		return $this;
	}

	public function getCPF () {
	    return $this->CPF;
	}

	public function setTelefone ($TELEFONE) {
	    $this->TELEFONE = $TELEFONE;
		return $this;
	}

	public function getTelefone () {
	    return $this->TELEFONE;
	}

	public function setEstado ($ESTADO) {
	    $this->ESTADO = $ESTADO;
		return $this;
	}

	public function getEstado () {
	    return $this->ESTADO;
	}

	public function setCidade ($CIDADE) {
	    $this->CIDADE = $CIDADE;
		return $this;
	}

	public function getCidade () {
	    return $this->CIDADE;
	}

	public function setCEP ($CEP) {
	    $this->CEP = $CEP;
		return $this;
	}

	public function getCEP () {
	    return $this->CEP;
	}

	public function setEndereco ($ENDERECO) {
	    $this->ENDERECO = $ENDERECO;
		return $this;
	}

	public function getEndereco () {
	    return $this->ENDERECO;
	}

	public function setBairro ($BAIRRO) {
	    $this->BAIRRO = $BAIRRO;
		return $this;
	}

	public function getBairro () {
	    return $this->BAIRRO;
	}

	public function listarUnidadesConcedentes(){
		$unidades_concedentes = array();
		$unidade_concedente_DAO = new UnidadeConcedenteDAO();
		$unidades_concedentes = $unidade_concedente_DAO->listarUnidadesConcedentesDAO();
			
		return $unidades_concedentes;
	}

	public function listarUnidadeConcedente($ID_UNIDADE_CONCEDENTE) {
		$unidade_concedente = array();
		$unidade_concedente_DAO = new UnidadeConcedenteDAO();
		$unidade_concedente = $unidade_concedente_DAO->listarUnidadeConcedenteDAO($ID_UNIDADE_CONCEDENTE);

		return $unidade_concedente;
	}

	public function cadastrarUnidadeConcedente($unidade_concedente, $id_estagio) {
		$unidade_concedente_DAO = new UnidadeConcedenteDAO();
		$unidade_concedente_DAO->cadastrarUnidadeConcedenteDAO($unidade_concedente, $id_estagio);
	}

}
?>