<?php
require_once 'dao/OfertaVagaDAO.php';


class OfertaVagaModel {

	private $ID_OFERTA_VAGA;
	private $ID_VAGA;
	private $ID_CURSO;
	private $ID_INSTITUICAO;
	private $NOME_VAGA;
	private $NOME_CURSO;

	public function setIDOfertaVaga ($ID_OFERTA_VAGA) {
	    $this->ID_OFERTA_VAGA = $ID_OFERTA_VAGA;
		return $this;
	}

	public function getIDOfertaVaga () {
	    return $this->ID_OFERTA_VAGA;
	}

	public function setIDVaga ($ID_VAGA) {
	    $this->ID_VAGA = $ID_VAGA;
		return $this;
	}

	public function getIDVaga () {
	    return $this->ID_VAGA;
	}

	public function setIDCurso ($ID_CURSO) {
	    $this->ID_CURSO = $ID_CURSO;
		return $this;
	}

	public function getIDCurso () {
	    return $this->ID_CURSO;
	}

	public function setIDInstituicaoOfertaVaga ($ID_INSTITUICAO) {
	    $this->ID_INSTITUICAO = $ID_INSTITUICAO;
		return $this;
	}

	public function getIDInstituicaoOfertaVaga () {
	    return $this->ID_INSTITUICAO;
	}

	public function setNomeVaga ($NOME_VAGA) {
		$this->NOME_VAGA = $NOME_VAGA;
		return $this;
	}

	public function getNomeVaga () {
		return $this->NOME_VAGA;
	}

	public function setNomeCurso ($NOME_CURSO) {
		$this->NOME_CURSO = $NOME_CURSO;
		return $this;
	}

	public function getNomeCurso () {
		return $this->NOME_CURSO;
	}

	public function buscarNomeVaga ($ID_VAGA) {
		$oferta_vaga_DAO = new OfertaVagaDAO();
		$nome_vaga = "";
		$nome_vaga = $oferta_vaga_DAO->buscarNomeVagaDAO($ID_VAGA);

		return $nome_vaga;
	}

	public function buscarNomeCurso ($ID_CURSO) {
		$oferta_vaga_DAO = new OfertaVagaDAO();
		$nome_curso = "";
		$nome_curso = $oferta_vaga_DAO->buscarNomeCursoDAO($ID_CURSO);

		return $nome_curso;
	}

	public function buscarOfertaVagasAluno($ID_INSTITUICAO, $ID_CURSO) {
	    $oferta_vagas = array();
	    $oferta_vaga_DAO = new OfertaVagaDAO();
		$oferta_vagas = $oferta_vaga_DAO->buscarOfertaVagasAlunoDAO($ID_INSTITUICAO, $ID_CURSO);
		
	    return $oferta_vagas;
	}

	public function listarOfertaVagas($ID_VAGA) {
	    $oferta_vagas = array();
	    $oferta_vaga_DAO = new OfertaVagaDAO();
		$oferta_vagas = $oferta_vaga_DAO->listarOfertaVagasDAO($ID_VAGA);
		
	    return $oferta_vagas;
	}

	public function removerOfertaVaga($ID_OFERTA_VAGA) {
		$oferta_vaga_DAO = new OfertaVagaDAO();
		$oferta_vaga_DAO->removerOfertaVagaDAO($ID_OFERTA_VAGA);
	}

	public function cadastrarOfertaVaga($oferta_vaga) {
	    $oferta_vaga_DAO = new OfertaVagaDAO();
	    $oferta_vaga_DAO->cadastrarOfertaVagaDAO($oferta_vaga);

	}

	public function visualizarAdicionar($ID_VAGA) {
		$oferta_vaga_DAO = new OfertaVagaDAO();
		$info_vaga = $oferta_vaga_DAO->visualizarAdicionarDAO($ID_VAGA);
		
		return $info_vaga;
	}

	public function adicionarOfertaVaga($oferta_vaga) {
		$oferta_vaga_DAO = new OfertaVagaDAO();
	    $oferta_vaga_DAO->adicionarOfertaVagaDAO($oferta_vaga);
	}

}
?>