<?php

session_start();

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Curso</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Cadastro Curso</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6 mx-auto">

                        <form method='post' action="?controle=Curso&acao=cadastroCurso">
                            <div class="form-group">
                                <label for="cursoNome">Nome:</label>
                                <input type="text" class="form-control" name="cursoNome" placeholder="Digite o nome do curso" required>
                            </div>
                            <div class="form-group">
                                <label for="cursoDuracao">Duração:</label>
                                <input type="number" class="form-control" name="cursoDuracao" placeholder="Digite a duração do curso (em anos)" min="1" required>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>
                            
                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>