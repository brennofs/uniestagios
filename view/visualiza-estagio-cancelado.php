<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$estagios = $v_params['estagios'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Vizualizar Estagios Cancelados</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="internship-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Estagios Cancelados</b>
                </h3>

                <div class="table-responsive">
                    <table class="table table-hover table-active">
                    <?php 
                        if(!empty($estagios)) {
                    ?>
                        <tr>
                            <th class="text-center">
                                Aluno
                            </th>
                            <th class="text-center">
                                Empresa
                            </th>
                            <th class="text-center">
                                Vaga
                            </th>
                            <th class="text-center">
                                Duração
                            </th>
                            <th class="text-center">
                                Situação
                            </th>
                            <th class="text-center">
                                Curso
                            </th>
                        </tr>
                        <?php
                            foreach($estagios AS $estagio) {
                        ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo $estagio->getNomeAluno();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $estagio->getNomeEmpresa();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $estagio->getNomeVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $estagio->getDuracao();?> Semestres 
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($estagio->getSitEstagio() == 1) { 
                                            echo "Ativo";
                                        } else if ($estagio->getSitEstagio() == 2) {
                                            echo "Finalizado/Arquivado";
                                        } else {
                                            echo "Cancelado";
                                        }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $estagio->getNomeCurso();?>
                                </td>
                                
                            </tr>
                        <?php
                            }
                        } else {
                            echo "<tr><td><h5 class='text-center'>Não há nenhum estágio cadastrado no momento!</h5></td></tr>";

                            echo
                            "<tr>
                                <td class='text-center desk'>
                                    <a href='?controle=Instituicao&acao=instituicao'><p><i class='fas fa-desktop'></i></p></a>
                                </td>
                            </tr>";
                        }
                        ?>
                    </table>
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>