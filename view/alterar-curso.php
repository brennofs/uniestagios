<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$curso = $v_params['curso'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Alterar Curso</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Alterar Curso</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6 mx-auto">

                    <?php 
                        if(!empty($curso)) {

                            foreach($curso AS $info_curso) {
                    ?>
                        <form method='post' action="?controle=Curso&acao=alterar">
                            <input type="hidden" name="IDCurso" value="<?php echo $info_curso->getIDCurso(); ?>">
                            <div class="form-group">
                                <label for="cursoNome">Nome:</label>
                                <input type="text" class="form-control" name="cursoNome" placeholder="Digite o nome do curso" value="<?php echo $info_curso->getNomeCurso(); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="cursoDuracao">Duração:</label>
                                <input type="number" class="form-control" name="cursoDuracao" placeholder="Digite a duração do curso (em anos)" value="<?php echo $info_curso->getDuracaoCurso(); ?>" min="1" required>
                            </div>

                        <?php
                            }
                        } else {
                            Application::redirect('?controle=Curso&acao=visualizarCurso');
                        }
                        ?>

                            <button type="submit" class="btn btn-success">Enviar</button>
                            
                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>