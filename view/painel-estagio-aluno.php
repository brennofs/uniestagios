<?php

if (!isset($_SESSION['AcessoAluno'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$estagio = $v_params['estagio'];

?>

<!doctype html>
<html lang="en">

<head>

    <?php 
        include ("style/head.php");   
    ?>

    <title>Administrar Estágio</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="admin-control-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Administrar Estágio</b>
                </h3>

                <hr>

                <div class="text-center">

                <?php

                    if (empty($estagio[0]->getIDUnidadeConcedenteEstagio())) {

                        echo
                        "<a class='btn btn-dark' href='?controle=UnidadeConcedente&acao=cadastrarUnidade&v=" . $estagio[0]->getIDEstagio() . "' role='button'>Cadastrar Unidade Concedente</a>
                        <a class='btn btn-dark disabled' href='?controle=Documento&acao=cadastrarDocumentoAceite' role='button'>Gerar Documento de Aceite</a>
                        <a class='btn btn-dark disabled' href='?controle=Documento&acao=cadastrarDocumentoAditivo' role='button'>Gerar Documento Aditivo</a>
                        <a class='btn btn-dark disabled' href='?controle=Documento&acao=cadastrarDocumentoRescisao' role='button'>Gerar Documento de Rescisão</a>";

                    } else {

                        echo
                        "<a class='btn btn-dark disabled' href='?controle=UnidadeConcedente&acao=cadastrarUnidade' role='button'>Cadastrar Unidade Concedente</a>
                        <a class='btn btn-dark' href='?controle=Documento&acao=cadastrarDocumentoAceite&v=" . $estagio[0]->getIDEstagio() . "' role='button'>Gerar Documento de Aceite</a>
                        <a class='btn btn-dark' href='?controle=Documento&acao=cadastrarDocumentoAditivo&v=" . $estagio[0]->getIDEstagio() . "' role='button'>Gerar Documento Aditivo</a>
                        <a class='btn btn-dark' href='?controle=Documento&acao=cadastrarDocumentoRescisao&v=" . $estagio[0]->getIDEstagio() . "' role='button'>Gerar Documento de Rescisão</a>";
                    }
                ?>

                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>