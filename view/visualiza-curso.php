<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$cursos = $v_params['cursos'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Vizualizar Cursos</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="courses-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Cursos</b>
                </h3>

                <div class="table-responsive">
                    <table class="table table-hover table-active">
                    <?php 
                        if(!empty($cursos)) {
                    ?>
                        <tr>
                            <th class="text-center">
                                Nome
                            </th>
                            <th class="text-center">
                                Duração
                            </th>
                            <th class="text-center">
                                Alterar
                            </th>
                            <th class="text-center">
                                Deletar
                            </th>
                        </tr>
                        <?php
                            foreach($cursos AS $curso) {
                        ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo $curso->getNomeCurso();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $curso->getDuracaoCurso();?> Anos
                                </td>

                                <td align="center" class="alter">
                                    <a href='?controle=Curso&acao=visualizarAlterar&q=<?php echo $curso->getIDCurso()?>' ><p><i class="fas fa-edit"></i></p></a>
                                </td>
                                
                                <td align="center" class="delete">
                                    <a href='?controle=Curso&acao=remover&q=<?php echo $curso->getIDCurso()?>' ><p><i class="fas fa-trash"></i></p></a>
                                </td>
                                
                            </tr>
                        <?php
                            }
                        } else {
                            echo "<tr><td><h5 class='text-center'>Não há nenhum curso cadastrado no momento!</h5></td></tr>";

                            echo
                            "<tr>
                                <td class='text-center desk'>
                                    <a href='?controle=Instituicao&acao=instituicao'><p><i class='fas fa-desktop'></i></p></a>
                                </td>
                            </tr>";
                        }
                        ?>
                    </table>
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>