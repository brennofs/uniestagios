<?php

session_start();

if (!isset($_SESSION['AcessoAluno'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$estagio = $v_params['estagio'];
$unidades_concedentes = $v_params['unidades_concedentes'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Selecione Unidade Concedente</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Unidade Concedente</b>
                </h3>

                <hr>

                <h6 class="text-center">
                    <b>Selecione abaixo sua unidade concedente caso a mesma já esteja cadastrada em nosso banco.</b>
                </h6>

                <hr>

                <div class="row form">

                    <div class="col-md-6 mx-auto">

                        <form method='post' action="?controle=UnidadeConcedente&acao=selecioneUnidadeConcedente">
                            <input type="hidden" name="idEstagio" value="<?php echo $estagio; ?>">
                            <div class="form-group">
                                <label for="idUnidadeConcedente">Unidade Concedente:</label>
                                <select class="form-control" name="idUnidadeConcedente" required>

                                <?php
                                    if (!empty($unidades_concedentes)) {
                                        foreach($unidades_concedentes AS $unidade) {
                                ?>

                                    <option value="<?php echo $unidade->getIDUnidadeConcedente();?>"><?php echo $unidade->getRazaoSocial();?></option>

                                <?php
                                        }
                                    } else {
                                ?>
                                    <option value="" disabled selected>Nenhuma unidade cadastrada</option>
                                <?php   
                                    }
                                ?>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>
                            
                        </form>
                    
                        <hr>

                        <h6 class="text-center">
                            <b>Caso sua empresa ainda não tenha sido cadastrada, faça o cadastro clicando no botão abaixo.</b>
                        </h6>

                        <div class="text-center add">
                            <a href="?controle=UnidadeConcedente&acao=cadastrarNovaUnidade&v=<?php echo $estagio; ?>" ><p><i class="fas fa-plus-square"></i></p></a>
                        </div>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>