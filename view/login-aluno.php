<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Login Aluno</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="login-body">

        <div class="container">

            <div class="col-4 login-aluno mx-auto">

                <h3 class="text-center">
                    <b>Login Alunos</b>
                </h3>

                <hr>

                <form method='post' action="?controle=Acesso&acao=validarAcessoAluno">
                    <div class="form-group">
                        <p>RA Aluno</p>
                        <input type="text" class="form-control" name="login" placeholder="Digite seu RA" required>
                    </div>
                    <div class="form-group">
                        <p>Senha</p>
                        <input type="password" class="form-control" name="senha" placeholder="Digite sua Senha" required>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Acessar</button>
                    </div>
                </form>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");   
    ?>

</body>

</html>