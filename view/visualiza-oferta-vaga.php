<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$oferta_vagas = $v_params['oferta_vagas'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Vizualizar Cursos Ofertados</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="offer-courses-body">

        <div class="container">

            <div class="col-12 home mx-auto">
                <?php 
                    if(!empty($oferta_vagas)) {
                ?>

                <h3 class="text-center">
                    <b>Cursos Ofertados</b>
                </h3>

                <div class="table-responsive">
                    <table class="table table-hover table-active">
                        <tr>
                            <th class="text-center">
                                Curso
                            </th>
                            <th class="text-center">
                                Deletar
                            </th>
                        </tr>
                        <?php
                            foreach($oferta_vagas AS $oferta) {
                        ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo $oferta->getNomeCurso();?>
                                </td>
                                
                                <td align="center" class="delete">
                                    <a href='?controle=OfertaVaga&acao=remover&q=<?php echo $oferta->getIDOfertaVaga()?>' ><p><i class="fas fa-trash"></i></p></a>
                                </td>
                                
                            </tr>
                        <?php
                            }
                        ?>
                            <tr>
                                <td class="text-center">
                                    <b>Adicionar Novo Curso</b>
                                </td>
                                <td class="text-center add">
                                    <a href='?controle=OfertaVaga&acao=visualizarAdicionar&v=<?php echo $oferta_vagas[0]->getIDVaga()?>' ><p><i class="fas fa-plus-square"></i></p></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <b>Voltar ao Painel</b>
                                </td>
                                <td class='text-center desk'>
                                    <a href='?controle=Instituicao&acao=instituicao'><p><i class='fas fa-desktop'></i></p></a>
                                </td>
                            </tr>
                    <?php
                    } else {
                        echo "<tr><td><h5 class='text-center'>Esta vaga não esta sendo ofertada para nenhum curso no momento!</h5></td></tr>";

                        echo
                        "<div class='table-responsive'>
                            <table class='table table-hover table-active'>
                                <tr>
                                    <td class='text-center add'>
                                        <a href='?controle=OfertaVaga&acao=visualizarAdicionar&v=" . $_GET['v'] . "'><p><i class='fas fa-plus-square'></i></p></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class='text-center desk'>
                                        <a href='?controle=Instituicao&acao=instituicao'><p><i class='fas fa-desktop'></i></p></a>
                                    </td>
                                </tr>";
                    }
                    ?>
                    </table>
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>