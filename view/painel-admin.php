<?php

session_start();

if (!isset($_SESSION['AcessoAdmin'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

?>

<!doctype html>
<html lang="en">

<head>

    <?php 
        include ("style/head.php");   
    ?>

    <title>Painel de Controle</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="admin-control-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Painel de Controle</b>
                </h3>

                <hr>

                <div class="text-center">

                    <a class="btn btn-danger" href="?controle=Instituicao&acao=validarInstituicao" role="button">Validar Instituições</a>
                    <a class="btn btn-danger" href="?controle=Instituicao&acao=visualizarRejeitada" role="button">Instituições Rejeitadas</a>
                    <a class="btn btn-danger" href="?controle=Instituicao&acao=visualizarInstituicao" role="button">Visualizar Instituições</a>

                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>