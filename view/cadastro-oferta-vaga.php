<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$cursos = $v_params['cursos'];
$oferta_vaga = $v_params['oferta_vaga'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Oferta Vaga</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Ofertar no Curso</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6 mx-auto">

                        <form method='post' action="?controle=OfertaVaga&acao=adicionar">
                        <?php
                            if (!empty($oferta_vaga)) {
                        ?>

                            <input type="hidden" name="IDVaga" value="<?php echo $_GET['v']; ?>">
                            <div class="form-group">
                                <label for="ofertaCursoVaga">Ofertar também esta vaga para o curso de:</label>
                                <select class="form-control" name="ofertaCursoVaga" required>
                                    <option value="">Selecione o Curso</option>
                                <?php
                                    if (!empty($cursos)) {
                                        foreach($cursos AS $curso) {
                                ?>

                                    <option value="<?php echo $curso->getIDCurso();?>" <?php foreach($oferta_vaga AS $oferta) { if ($oferta->getIDCurso() == $curso->getIDCurso()) { echo "disabled"; } } ?>><?php echo $curso->getNomeCurso();?></option>

                                <?php
                                        }
                                    } else {
                                ?>
                                    <option value="" disabled>Nenhum curso cadastrado</option>
                                <?php   
                                    }
                                ?>
                                </select>
                            </div>

                            <?php
                            } else {
                            ?>
                            
                            <input type="hidden" name="IDVaga" value="<?php echo $_GET['v']; ?>">
                            <div class="form-group">
                                <label for="ofertaCursoVaga">Ofertar esta vaga no curso de:</label>
                                <select class="form-control" name="ofertaCursoVaga" required>

                                <?php
                                    if (!empty($cursos)) {
                                        foreach($cursos AS $curso) {
                                ?>

                                    <option value="<?php echo $curso->getIDCurso();?>"><?php echo $curso->getNomeCurso();?></option>

                                <?php
                                        }
                                    } else {
                                ?>
                                    <option value="" disabled selected>Nenhum curso cadastrado</option>
                                <?php   
                                    }
                                ?>
                                </select>
                            </div>

                            <?php
                            }
                            ?>

                            <button type="submit" class="btn btn-success">Enviar</button>

                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>