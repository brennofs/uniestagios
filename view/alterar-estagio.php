<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$estagio = $v_params['estagio'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Alterar Duração Estágio</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Alterar Duração Estágio</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6 mx-auto">

                    <?php 
                        if(!empty($estagio)) {

                            foreach($estagio AS $info_estagio) {
                    ?>
                        <form method='post' action="?controle=Estagio&acao=alterarDuracao">
                            <input type="hidden" name="IDEstagio" value="<?php echo $info_estagio->getIDEstagio(); ?>">
                            <p class="text-center">Digite a nova duração total do estágio</p>
                            <div class="form-group">
                                <label for="estagioDuracao">Duração:</label>
                                <input type="number" class="form-control" name="estagioDuracao" placeholder="Digite a nova duração do estágio (em semestres)" value="<?php echo $info_estagio->getDuracao(); ?>" min="1" required>
                            </div>

                        <?php
                            }
                        } else {
                            Application::redirect('?controle=Estagio&acao=visualizarEstagio');
                        }
                        ?>

                            <button type="submit" class="btn btn-success">Enviar</button>
                            
                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>