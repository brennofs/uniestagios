<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$aluno = $v_params['aluno'];
$cursos = $v_params['cursos'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Alterar Aluno</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Alterar Aluno</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6">

                    <?php 
                        if(!empty($aluno)) {

                            foreach($aluno AS $info_aluno) {
                    ?>
                        <form method='post' action="?controle=Aluno&acao=alterar">
                            <input type="hidden" name="IDAluno" value="<?php echo $info_aluno->getIDAluno(); ?>">
                            <div class="form-group">
                                <label for="alunoRA">RA:</label>
                                <input type="text" class="form-control" name="alunoRA" placeholder="Digite o RA" value="<?php echo $info_aluno->getRAAluno(); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoNome">Nome:</label>
                                <input type="text" class="form-control" name="alunoNome" placeholder="Digite o nome do aluno" value="<?php echo $info_aluno->getNomeAluno(); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoSemestre">Semestre:</label>
                                <input type="number" class="form-control" name="alunoSemestre" placeholder="Digite o semestre do aluno" value="<?php echo $info_aluno->getSemestreAluno(); ?>" min="1" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoDataNasc">Data Nascimento:</label>
                                <input type="text" class="form-control" name="alunoDataNasc" maxlength="10" placeholder="DD/MM/AAAA" value="<?php echo $info_aluno->getDataNascAluno(); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoRG">RG:</label>
                                <input type="text" class="form-control" name="alunoRG" placeholder="88.888.888-8" maxlength="12" value="<?php echo $info_aluno->getRGAluno(); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoCPF">CPF:</label>
                                <input type="text" class="form-control" name="alunoCPF" placeholder="888.888.888-88" maxlength="14" value="<?php echo $info_aluno->getCPFAluno(); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoPeriodo">Periodo:</label>
                                <select class="form-control" name="alunoPeriodo">
                                    <option value="1" <?php if ($info_aluno->getPeriodoAluno() == 1) { echo "selected"; } ?> >Matutino</option>
                                    <option value="2" <?php if ($info_aluno->getPeriodoAluno() == 2) { echo "selected"; } ?> >Vespertino</option>
                                    <option value="3" <?php if ($info_aluno->getPeriodoAluno() == 3) { echo "selected"; } ?> >Noturno</option>
                                    <option value="4" <?php if ($info_aluno->getPeriodoAluno() == 4) { echo "selected"; } ?> >Integral</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alunoSitMatricula">Situação Matrícula:</label>
                                <select class="form-control" name="alunoSitMatricula">
                                    <option value="1" <?php if ($info_aluno->getSitMatriculaAluno() == 1) { echo "selected"; } ?> >Cursando</option>
                                    <option value="2" <?php if ($info_aluno->getSitMatriculaAluno() == 2) { echo "selected"; } ?> >Formado</option>
                                    <option value="3" <?php if ($info_aluno->getSitMatriculaAluno() == 3) { echo "selected"; } ?> >Trancado</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>

                    </div>

                    <div class="col-6">

                            <div class="row">

                                <div class="form-group col-6">
                                    <label for="alunoEstado">Estado:</label>
                                    <input type="text" class="form-control" name="alunoEstado" placeholder="Selecione o estado" value="<?php echo $info_aluno->getEstadoAluno(); ?>" maxlength="2" required>
                                </div>
                                <div class="form-group col-6">
                                    <label for="alunoCidade">Cidade:</label>
                                    <input type="text" class="form-control" name="alunoCidade" placeholder="Selecione a cidade" value="<?php echo $info_aluno->getCidadeAluno(); ?>" required>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="alunoCEP">CEP:</label>
                                <input type="text" class="form-control" name="alunoCEP" placeholder="88888-888" value="<?php echo $info_aluno->getCEPAluno(); ?>" maxlength="9" required>
                            </div>

                            <div class="form-group">
                                <label for="alunoBairro">Bairro:</label>
                                <input type="text" class="form-control" name="alunoBairro" placeholder="Digite o bairro do aluno" value="<?php echo $info_aluno->getBairroAluno(); ?>" required>
                            </div>

                            <div class="row">

                                <div class="form-group col-6">
                                    <label for="alunoTelefone">Telefone:</label>
                                    <input type="text" class="form-control" name="alunoTelefone" placeholder="(88) 8888-8888" value="<?php echo $info_aluno->getFoneAluno(); ?>" maxlength="14">
                                </div>
                                <div class="form-group col-6">
                                    <label for="alunoCelular">Celular:</label>
                                    <input type="text" class="form-control" name="alunoCelular" placeholder="(88) 88888-8888" value="<?php echo $info_aluno->getCelularAluno(); ?>" maxlength="15">
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="alunoEmail">Email:</label>
                                <input type="text" class="form-control" name="alunoEmail" placeholder="Digite o email do aluno" value="<?php echo $info_aluno->getEmailAluno(); ?>" required>
                            </div>

                            <div class="form-group">
                                <label for="alunoPortadorDef">Portador Deficiência:</label>
                                <select class="form-control" name="alunoPortadorDef" required>
                                    <option value="" disabled selected>Selecione</option>
                                    <option value="1" <?php if ($info_aluno->getPortadorDefAluno() == 1) { echo "selected"; } ?> >Não</option>
                                    <option value="2" <?php if ($info_aluno->getPortadorDefAluno() == 2) { echo "selected"; } ?> >Sim</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="alunoEstagObrig">Estágio Obrigatório:</label>
                                <select class="form-control" name="alunoEstagObrig" required>
                                    <option value="" disabled selected>Selecione</option>
                                    <option value="1" <?php if ($info_aluno->getEstagObrigAluno() == 1) { echo "selected"; } ?> >Não</option>
                                    <option value="2" <?php if ($info_aluno->getEstagObrigAluno() == 2) { echo "selected"; } ?> >Sim</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="alunoCurso">Curso:</label>
                                <select class="form-control" name="alunoCurso" required>

                                <?php
                                    if (!empty($cursos)) {
                                        foreach($cursos AS $curso) {
                                ?>

                                    <option value="<?php echo $curso->getIDCurso();?>" <?php if ($info_aluno->getIDCursoAluno() == $curso->getIDCurso()) { echo "selected"; } ?> ><?php echo $curso->getNomeCurso();?></option>

                                <?php
                                        }
                                    } else {
                                ?>
                                    <option value="" disabled selected>Nenhum curso cadastrado</option>
                                <?php   
                                    }
                                ?>
                                </select>
                            </div>

                        <?php
                            }
                        } else {
                            Application::redirect('?controle=Aluno&acao=visualizarAluno');
                        }
                        ?>

                        </form>
                    
                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>