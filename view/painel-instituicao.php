<?php

session_start();

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

?>

<!doctype html>
<html lang="en">

<head>

    <?php 
        include ("style/head.php");   
    ?>

    <title>Painel de Controle</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="admin-control-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Painel de Controle</b>
                </h3>

                <hr>

                <div class="text-center">

                    <a class="btn btn-primary" href="?controle=Curso&acao=cadastrarCurso" role="button">Cadastrar Cursos</a>
                    <a class="btn btn-primary" href="?controle=Curso&acao=visualizarCurso" role="button">Visualizar Cursos</a>

                </div>

                <div><br></div>

                <div class="text-center">

                    <a class="btn btn-dark" href="?controle=Aluno&acao=cadastrarAluno" role="button">Cadastrar Alunos</a>
                    <a class="btn btn-dark" href="?controle=Aluno&acao=visualizarAluno" role="button">Visualizar Alunos</a>

                </div>

                <div><br></div>

                <div class="text-center">

                    <a class="btn btn-secondary" href="?controle=Vaga&acao=cadastrarVaga" role="button">Cadastrar Vagas</a>
                    <a class="btn btn-secondary" href="?controle=Vaga&acao=visualizarVaga" role="button">Visualizar Vagas</a>

                </div>

                <div><br></div>

                <div class="text-center">

                    <a class="btn btn-success" href="?controle=Estagio&acao=visualizarEstagio" role="button">Estágios Ativos</a>
                    <a class="btn btn-success" href="?controle=Estagio&acao=visualizarEstagioFinalizado" role="button">Estágios Finalizados</a>
                    <a class="btn btn-success" href="?controle=Estagio&acao=visualizarEstagioCancelado" role="button">Estágios Cancelados</a>

                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>