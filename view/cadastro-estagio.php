<?php

session_start();

if (!isset($_SESSION['AcessoAluno'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$vaga = $v_params['vaga'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Estágio</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Cadastro Estágio</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6">

                        <form method='post' action="?controle=Estagio&acao=cadastroEstagio">
                            <input type="hidden" name="idVaga" value="<?php echo $vaga ?>">
                            <div class="form-group">
                                <label for="duracao">Duração:</label>
                                <input type="number" class="form-control" name="duracao" placeholder="Digite a duração do estágio (em meses)" min="1" required>
                            </div>
                            <div class="form-group">
                                <label for="dataInicio">Data Inicio:</label>
                                <input type="date" class="form-control" name="dataInicio" placeholder="Digite a data de inicio do estágio" required>
                            </div>
                            <div class="form-group">
                                <label for="dataFim">Data Fim:</label>
                                <input type="date" class="form-control" name="dataFim" placeholder="Digite a data final de estágio" required>
                            </div>
                            <div class="form-group">
                                <label for="horarioEntrada">Horário de Entrada:</label>
                                <input type="time" class="form-control" name="horarioEntrada" placeholder="Digite o horário de entrada" required>
                            </div>
                            <div class="form-group">
                                <label for="horarioSaida">Horário de Saída:</label>
                                <input type="time" class="form-control" name="horarioSaida" placeholder="Digite o horário de saída" required>
                            </div>
                            <div class="form-group">
                                <label for="horasSemanais">Horas Semanais:</label>
                                <input type="number" class="form-control" name="horasSemanais" placeholder="Digite as horas semanais" min="1" required>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>

                    </div>

                    <div class="col-md-6">

                            <div class="form-group">
                                <label for="repLegal">Representante Legal:</label>
                                <input type="text" class="form-control" name="repLegal" placeholder="Digite o representante legal" required>
                            </div>

                            <div class="form-group">
                                <label for="cargoRep">Cargo Representante:</label>
                                <input type="text" class="form-control" name="cargoRep" placeholder="Digite o cargo" required>
                            </div>

                            <div class="form-group">
                                <label for="supEstagio">Supervisor de Estágio:</label>
                                <input type="text" class="form-control" name="supEstagio" placeholder="Digite o supervisor de estágio" required>
                            </div>

                            <div class="form-group">
                                <label for="formAcademicaSup">Formação Acadêmica Supervisor:</label>
                                <input type="text" class="form-control" name="formAcademicaSup" placeholder="Digite a formação acadêmica" required>
                            </div>

                           <div class="row">

                                <div class="form-group col-6">
                                    <label for="regProfissionalSup">Registro Profissional:</label>
                                    <input type="text" class="form-control" name="regProfissionalSup" placeholder="888.888.888-8" maxlength="13" required>
                                </div>
                                <div class="form-group col-6">
                                    <label for="orgaoRegSup">Orgão:</label>
                                    <input type="text" class="form-control" name="orgaoRegSup" placeholder="Selecione o estado" maxlength="2" required>
                                </div>

                            </div>

                        </form>
                    
                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>