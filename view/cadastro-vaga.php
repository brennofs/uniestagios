<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$cursos = $v_params['cursos'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Vaga</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Cadastro Vaga</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6">

                        <form method='post' action="?controle=Vaga&acao=cadastroVaga">
                            <div class="form-group">
                                <label for="nomeVaga">Nome:</label>
                                <input type="text" class="form-control" name="nomeVaga" placeholder="Digite o nome" required>
                            </div>
                            <div class="form-group">
                                <label for="empresaVaga">Empresa:</label>
                                <input type="text" class="form-control" name="empresaVaga" placeholder="Digite a empresa" required>
                            </div>
                            <div class="form-group">
                                <label for="descVaga">Descrição:</label>
                                <textarea class="form-control" name="descVaga" rows="5" placeholder="Digite a descrição" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="salarioVaga">Salário:</label>
                                <input type="number" step="any" class="form-control" name="salarioVaga" min="1" placeholder="Digite o salario da vaga">
                            </div>
                            <div class="form-group">
                                <label for="beneficiosVaga">Benefícios:</label>
                                <textarea class="form-control" name="beneficiosVaga" rows="5" placeholder="Digite os benefícios"></textarea>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>

                    </div>

                    <div class="col-md-6">
                    
                        <div class="row">

                            <div class="form-group col-6">
                                <label for="horEntradaVaga">Horário Entrada:</label>
                                <input type="time" class="form-control" name="horEntradaVaga">
                            </div>
                            <div class="form-group col-6">
                                <label for="horSaidaVaga">Horário Saída:</label>
                                <input type="time" class="form-control" name="horSaidaVaga">
                            </div>

                        </div>

                            <div class="form-group">
                                <label for="tipoVaga">Tipo Vaga:</label>
                                <select class="form-control" name="tipoVaga" required>
                                    <option value="1">Estágio</option>
                                    <option value="2">CLT</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="ofertaCursoVaga[]">Ofertar para cursos:</label>
                                <select multiple class="form-control" name="ofertaCursoVaga[]" required>

                                <?php
                                    if (!empty($cursos)) {
                                        foreach($cursos AS $curso) {
                                ?>

                                    <option value="<?php echo $curso->getIDCurso();?>"><?php echo $curso->getNomeCurso();?></option>

                                <?php
                                        }
                                    } else {
                                ?>
                                    <option value="" disabled>Nenhum curso cadastrado</option>
                                <?php   
                                    }
                                ?>
                                </select>
                            </div>

                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>