<?php

session_start();

if (!isset($_SESSION['AcessoAluno'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$estagio = $v_params['estagio'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Documento Aceite</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Documento Aceite</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6 mx-auto">

                        <form method='post' action="?controle=Documento&acao=gerarDocumentoAceite">
                            <input type="hidden" name="idEstagio" value="<?php echo $estagio; ?>">

                            <div class="row">

                                <div class="form-group col-6">
                                    <label for="numeroApoliceSeguro">Numero da Apólice:</label>
                                    <input type="text" class="form-control" name="numeroApoliceSeguro" placeholder="Digite o numero da apólice" required>
                                </div>

                                <div class="form-group col-6">
                                    <label for="nomeSeguradora">Nome da Seguradora:</label>
                                    <input type="text" class="form-control" name="nomeSeguradora" placeholder="Digite o nome da seguradora" required>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="valorSeguro">Valor do Seguro:</label>
                                <input type="number" step="any" class="form-control" name="valorSeguro" min="0.00" placeholder="Digite o valor do seguro" >
                            </div>

                            <div class="form-group">
                                <label for="valorBolsa">Valor da Bolsa Auxílio:</label>
                                <input type="number" step="any" class="form-control" name="valorBolsa" min="0.00" placeholder="Digite o salario da vaga">
                            </div>

                            <div class="form-group">
                                <label for="valorValeTransporte">Valor do Vale Transporte:</label>
                                <input type="number" step="any" class="form-control" name="valorValeTransporte" min="0.00" placeholder="Digite o valor do vale transporte">
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>
                        
                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>