<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$alunos = $v_params['alunos'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Vizualizar Alunos</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="students-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Alunos</b>
                </h3>

                <div class="table-responsive">
                    <table class="table table-hover table-active">
                    <?php 
                        if(!empty($alunos)) {
                    ?>
                        <tr>
                            <th class="text-center">
                                RA
                            </th>
                            <th class="text-center">
                                Nome
                            </th>
                            <th class="text-center">
                                Semestre
                            </th>
                            <th class="text-center">
                                Periodo
                            </th>
                            <th class="text-center">
                                Situação Matrícula
                            </th>
                            <th class="text-center">
                                Situação Estágio
                            </th>
                            <th class="text-center">
                                Curso
                            </th>
                            <th class="text-center">
                                Alterar
                            </th>
                            <th class="text-center">
                                Deletar
                            </th>
                        </tr>
                        <?php
                            foreach($alunos AS $aluno) {
                        ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo $aluno->getRAAluno();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $aluno->getNomeAluno();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $aluno->getSemestreAluno();?>º
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($aluno->getPeriodoAluno() == 1) { 
                                            echo "Matutino";
                                        } else if ($aluno->getPeriodoAluno() == 2) {
                                            echo "Vespertino";
                                        } else if ($aluno->getPeriodoAluno() == 3) {
                                            echo "Noturno";
                                        } else {
                                            echo "Integral";
                                        }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($aluno->getSitMatriculaAluno() == 1) { 
                                            echo "Cursando";
                                        } else if ($aluno->getSitMatriculaAluno() == 2) {
                                            echo "Formado";
                                        } else {
                                            echo "Trancado";
                                        }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($aluno->getSitEstagioAluno() == 1) { 
                                            echo "Sem estágio";
                                        } else {
                                            echo "Estagiando";
                                        }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $aluno->getNomeCursoAluno();?>
                                </td>
                                
                                <td align="center" class="alter">
                                    <a href='?controle=Aluno&acao=visualizarAlterar&q=<?php echo $aluno->getIDAluno()?>' ><p><i class="fas fa-edit"></i></p></a>
                                </td>
                                
                                <td align="center" class="delete">
                                    <a href='?controle=Aluno&acao=remover&q=<?php echo $aluno->getIDAluno()?>' ><p><i class="fas fa-trash"></i></p></a>
                                </td>
                                
                            </tr>
                        <?php
                            }
                        } else {
                            echo "<tr><td><h5 class='text-center'>Não há nenhum aluno cadastrado no momento!</h5></td></tr>";

                            echo
                            "<tr>
                                <td class='text-center desk'>
                                    <a href='?controle=Instituicao&acao=instituicao'><p><i class='fas fa-desktop'></i></p></a>
                                </td>
                            </tr>";
                        }
                        ?>
                    </table>
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>