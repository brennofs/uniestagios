<?php

session_start();

if (!isset($_SESSION['AcessoAdmin'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$instituicoes = $v_params['instituicoes'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Vizualizar Instituições</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="institutions-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Instituições</b>
                </h3>

                <div class="table-responsive">
                    <table class="table table-hover table-active">
                    <?php 
                        if(!empty($instituicoes)) {
                    ?>
                        <tr>
                            <th class="text-center">
                                Nome
                            </th>
                            <th class="text-center">
                                CNPJ
                            </th>
                            <th class="text-center">
                                Estado
                            </th>
                            <th class="text-center">
                                Cidade
                            </th>
                            <th class="text-center">
                                Endereço
                            </th>
                            <th class="text-center">
                                Numero
                            </th>
                            <th class="text-center">
                                Bairro
                            </th>
                            <th class="text-center">
                                Telefone
                            </th>
                            <th class="text-center">
                                Data Cadastro
                            </th>
                            <th class="text-center">
                                Deletar
                            </th>
                        </tr>
                        <?php
                            foreach($instituicoes AS $instituicao) {
                        ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo $instituicao->getNomeInst();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $instituicao->getCNPJInst();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $instituicao->getEstInst();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $instituicao->getCidInst();?>  
                                </td>
                                <td class="text-center">
                                    <?php echo $instituicao->getEndInst();?>  
                                </td>
                                <td class="text-center">
                                    <?php echo $instituicao->getNumInst();?>  
                                </td>
                                <td class="text-center">
                                    <?php echo $instituicao->getBairroInst();?>  
                                </td>
                                <td class="text-center">
                                    <?php echo $instituicao->getTelefoneInst();?>  
                                </td>
                                <td class="text-center">
                                    <?php $date = new DateTime($instituicao->getDataCadastroInst()); echo date_format($date, "d/m/Y H:i:s"); ?>  
                                </td>
                                
                                <td align="center" class="delete">
                                    <a href='?controle=Instituicao&acao=remover&q=<?php echo $instituicao->getIDInst()?>' ><p><i class="fas fa-trash"></i></p></a>
                                </td>
                                
                            </tr>
                        <?php
                            }
                        } else {
                            echo "<tr><td><h5 class='text-center'>Não há nenhuma instituição com cadastro validado no momento!</h5></td></tr>";

                            echo
                            "<tr>
                                <td class='text-center desk'>
                                    <a href='?controle=Admin&acao=admin'><p><i class='fas fa-desktop'></i></p></a>
                                </td>
                            </tr>";
                        }
                        ?>
                    </table>
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>