<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$cursos = $v_params['cursos'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Aluno</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Cadastro Aluno</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6">

                        <form method='post' action="?controle=Aluno&acao=cadastroAluno">
                            <div class="form-group">
                                <label for="alunoRA">RA:</label>
                                <input type="text" class="form-control" name="alunoRA" placeholder="888888-8" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoSenha">Senha:</label>
                                <input type="password" class="form-control" name="alunoSenha" placeholder="Digite a senha" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoNome">Nome:</label>
                                <input type="text" class="form-control" name="alunoNome" placeholder="Digite o nome do aluno" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoDataNasc">Data Nascimento:</label>
                                <input type="text" class="form-control" name="alunoDataNasc" maxlength="10" placeholder="DD/MM/AAAA" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoRG">RG:</label>
                                <input type="text" class="form-control" name="alunoRG" placeholder="88.888.888-8" maxlength="12" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoCPF">CPF:</label>
                                <input type="text" class="form-control" name="alunoCPF" placeholder="888.888.888-88" maxlength="14" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoSemestre">Semestre:</label>
                                <input type="number" class="form-control" name="alunoSemestre" placeholder="Digite o semestre do aluno" min="1" required>
                            </div>
                            <div class="form-group">
                                <label for="alunoPeriodo">Periodo:</label>
                                <select class="form-control" name="alunoPeriodo">
                                    <option value="1">Matutino</option>
                                    <option value="2">Vespertino</option>
                                    <option value="3">Noturno</option>
                                    <option value="4">Integral</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>

                    </div>

                    <div class="col-md-6">

                            <div class="row">

                                <div class="form-group col-6">
                                    <label for="alunoEstado">Estado:</label>
                                    <input type="text" class="form-control" name="alunoEstado" placeholder="Selecione o estado" maxlength="2" required>
                                </div>
                                <div class="form-group col-6">
                                    <label for="alunoCidade">Cidade:</label>
                                    <input type="text" class="form-control" name="alunoCidade" placeholder="Selecione a cidade" required>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="alunoCEP">CEP:</label>
                                <input type="text" class="form-control" name="alunoCEP" placeholder="88888-888" maxlength="9" required>
                            </div>

                            <div class="form-group">
                                <label for="alunoBairro">Bairro:</label>
                                <input type="text" class="form-control" name="alunoBairro" placeholder="Digite o bairro do aluno" required>
                            </div>

                            <div class="row">

                                <div class="form-group col-6">
                                    <label for="alunoTelefone">Telefone:</label>
                                    <input type="text" class="form-control" name="alunoTelefone" placeholder="(88) 8888-8888" maxlength="14">
                                </div>
                                <div class="form-group col-6">
                                    <label for="alunoCelular">Celular:</label>
                                    <input type="text" class="form-control" name="alunoCelular" placeholder="(88) 88888-8888" maxlength="15">
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="alunoEmail">Email:</label>
                                <input type="text" class="form-control" name="alunoEmail" placeholder="Digite o email do aluno" required>
                            </div>

                            <div class="form-group">
                                <label for="alunoPortadorDef">Portador Deficiência:</label>
                                <select class="form-control" name="alunoPortadorDef" required>
                                    <option value="" disabled selected>Selecione</option>
                                    <option value="1">Não</option>
                                    <option value="2">Sim</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="alunoEstagObrig">Estágio Obrigatório:</label>
                                <select class="form-control" name="alunoEstagObrig" required>
                                    <option value="" disabled selected>Selecione</option>
                                    <option value="1">Não</option>
                                    <option value="2">Sim</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="alunoCurso">Curso:</label>
                                <select class="form-control" name="alunoCurso" required>

                                <?php
                                    if (!empty($cursos)) {
                                        foreach($cursos AS $curso) {
                                ?>

                                    <option value="<?php echo $curso->getIDCurso();?>"><?php echo $curso->getNomeCurso();?></option>

                                <?php
                                        }
                                    } else {
                                ?>
                                    <option value="" disabled selected>Nenhum curso cadastrado</option>
                                <?php   
                                    }
                                ?>
                                </select>
                            </div>

                        </form>
                    
                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>