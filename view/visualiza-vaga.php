<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$vagas = $v_params['vagas'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Vizualizar Vagas</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="vacancies-body">

        <div class="container-fluid">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Vagas</b>
                </h3>

                <div class="table-responsive">
                    <table class="table table-hover table-active">
                    <?php 
                        if(!empty($vagas)) {
                    ?>
                        <tr>
                            <th class="text-center">
                                Nome
                            </th>
                            <th class="text-center">
                                Empresa
                            </th>
                            <th class="text-center">
                                Descrição
                            </th>
                            <th class="text-center">
                                Salário
                            </th>
                            <th class="text-center">
                                Horário Entrada
                            </th>
                            <th class="text-center">
                                Horário Saída
                            </th>
                            <th class="text-center">
                                Benefícios
                            </th>
                            <th class="text-center">
                                Situação
                            </th>
                            <th class="text-center">
                                Tipo
                            </th>
                            <th class="text-center">
                                Cursos Ofertados
                            </th>
                            <th class="text-center">
                                Ofertar Curso
                            </th>
                            <th class="text-center">
                                Alterar
                            </th>
                            <th class="text-center">
                                Deletar
                            </th>
                        </tr>
                        <?php
                            foreach($vagas AS $vaga) {
                        ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo $vaga->getNomeVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getEmpresaVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getDescricaoVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($vaga->getSalarioVaga() == 0.00) { 
                                            echo "Não informado";
                                        } else {
                                            echo "R$ ", $vaga->getSalarioVaga();
                                        }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getHorEntradaVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getHorSaidaVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getBeneficiosVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($vaga->getSituacaoVaga() == 1) { 
                                            echo "Ativa";
                                        } else if ($vaga->getSituacaoVaga() == 2) {
                                            echo "Ocupada";
                                        } else {
                                            echo "Finalizada/Arquivada";
                                        }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($vaga->getTipoVaga() == 1) { 
                                            echo "Estágio";
                                        } else {
                                            echo "CLT";
                                        }
                                    ?>
                                </td>

                                <td class="text-center offer">
                                    <a href='?controle=OfertaVaga&acao=visualizarOfertaVaga&v=<?php echo $vaga->getIDVaga()?>'><p><i class="fas fa-eye"></i></p></a>
                                </td>

                                <td class="text-center add">
                                    <a href='?controle=OfertaVaga&acao=visualizarAdicionar&v=<?php echo $vaga->getIDVaga()?>' ><p><i class="fas fa-plus-square"></i></p></a>
                                </td>

                                <td class="text-center alter">
                                    <a href='?controle=Vaga&acao=visualizarAlterar&q=<?php echo $vaga->getIDVaga()?>' ><p><i class="fas fa-edit"></i></p></a>
                                </td>
                                
                                <td class="text-center delete">
                                    <a href='?controle=Vaga&acao=remover&q=<?php echo $vaga->getIDVaga()?>' ><p><i class="fas fa-trash"></i></p></a>
                                </td>
                                
                            </tr>
                        <?php
                            }
                        } else {
                            echo "<tr><td><h5 class='text-center'>Não há nenhuma vaga cadastrada no momento!</h5></td></tr>";

                            echo
                            "<tr>
                                <td class='text-center desk'>
                                    <a href='?controle=Instituicao&acao=instituicao'><p><i class='fas fa-desktop'></i></p></a>
                                </td>
                            </tr>";
                        }
                        ?>
                    </table>
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>