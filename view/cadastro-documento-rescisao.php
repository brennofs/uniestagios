<?php

session_start();

if (!isset($_SESSION['AcessoAluno'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$estagio = $v_params['estagio'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Documento Rescisão</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Documento Rescisão</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6 mx-auto">

                        <form method='post' action="?controle=Documento&acao=gerarDocumentoRescisao">
                            <input type="hidden" name="idEstagio" value="<?php echo $estagio; ?>">
        
                            <div class="form-group">
                                <label for="notificadaPor">Decisão de Rescisão por:</label>
                                <select class="form-control" name="notificadaPor" required>
                                    <option value="" disabled selected>Selecione</option>
                                    <option value="1">Unidade Concedente</option>
                                    <option value="2">Estagiário</option>
                                    <option value="3">Instituição de Ensino</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>

                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>