<?php

session_start();

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>UniEstagios</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="index-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>UniEstagios Integração Aluno/Instituição</b>
                </h3>

                <hr>

                Bem vindo ao UniEstágios

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>