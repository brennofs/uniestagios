<?php

if (!isset($_SESSION['AcessoAluno'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$vagas = $v_params['vagas'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Vizualizar Vagas</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="students-vacancies-body">

        <div class="container-fluid">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Vagas</b>
                </h3>

                <div class="table-responsive">
                    <table class="table table-hover table-active">
                    <?php 
                        if(!empty($vagas)) {
                    ?>
                        <tr>
                            <th class="text-center">
                                Nome
                            </th>
                            <th class="text-center">
                                Empresa
                            </th>
                            <th class="text-center">
                                Descrição
                            </th>
                            <th class="text-center">
                                Salário
                            </th>
                            <th class="text-center">
                                Horário Entrada
                            </th>
                            <th class="text-center">
                                Horário Saída
                            </th>
                            <th class="text-center">
                                Benefícios
                            </th>
                            <th class="text-center">
                                Tipo
                            </th>
                            <th class="text-center">
                                Iniciar Estágio
                            </th>
                        </tr>
                        <?php
                            foreach($vagas AS $vaga) {
                        ?>
                            <tr>
                                <td class="text-center">
                                    <?php echo $vaga->getNomeVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getEmpresaVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getDescricaoVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($vaga->getSalarioVaga() == 0.00) { 
                                            echo "Não informado";
                                        } else {
                                            echo "R$ ", $vaga->getSalarioVaga();
                                        }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getHorEntradaVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getHorSaidaVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php echo $vaga->getBeneficiosVaga();?>
                                </td>
                                <td class="text-center">
                                    <?php 
                                        if ($vaga->getTipoVaga() == 1) { 
                                            echo "Estágio";
                                        } else {
                                            echo "CLT";
                                        }
                                    ?>
                                </td>

                                <?php 

                                if ($_SESSION['SIT_ESTAG'] == 1) {

                                    echo
                                    "<td align='center' class='intership'>
                                        <a href='?controle=Estagio&acao=cadastrarEstagio&q=" . $vaga->getIDVaga() . "' ><p><i class='fas fa-check'></i></p></a>
                                    </td>";

                                } else {

                                    echo
                                    "<td align='center' class='internship'>
                                        <p>Você já está em um estágio</p>
                                    </td>";
                                }
                                ?>
                                
                            </tr>
                        <?php
                            }
                        } else {
                            echo "<tr><td><h5 class='text-center'>Não há nenhuma vaga para você no momento!</h5></td></tr>";

                            echo
                            "<tr>
                                <td class='text-center desk'>
                                    <a href='?controle=Aluno&acao=aluno'><p><i class='fas fa-desktop'></i></p></a>
                                </td>
                            </tr>";
                        }
                        ?>
                    </table>
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>