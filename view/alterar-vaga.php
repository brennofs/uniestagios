<?php

if (!isset($_SESSION['AcessoInstituicao'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$vaga = $v_params['vaga'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Alterar Vaga</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Alterar Vaga</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6">

                    <?php 
                        if(!empty($vaga)) {

                            foreach($vaga AS $info_vaga) {
                    ?>

                        <form method='post' action="?controle=Vaga&acao=alterar">
                        <input type="hidden" name="IDVaga" value="<?php echo $info_vaga->getIDVaga(); ?>">
                            <div class="form-group">
                                <label for="nomeVaga">Nome:</label>
                                <input type="text" class="form-control" name="nomeVaga" placeholder="Digite o nome" value="<?php echo $info_vaga->getNomeVaga(); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="empresaVaga">Empresa:</label>
                                <input type="text" class="form-control" name="empresaVaga" placeholder="Digite a empresa" value="<?php echo $info_vaga->getEmpresaVaga(); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="descVaga">Descrição:</label>
                                <textarea class="form-control" name="descVaga" rows="5" placeholder="Digite a descrição" required><?php echo $info_vaga->getDescricaoVaga(); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="salarioVaga">Salário:</label>
                                <input type="number" step="any" class="form-control" name="salarioVaga" placeholder="Digite o salario da vaga" value="<?php echo $info_vaga->getSalarioVaga(); ?>" min="1">
                            </div>
                            <div class="form-group">
                                <label for="beneficiosVaga">Benefícios:</label>
                                <textarea class="form-control" name="beneficiosVaga" rows="5" placeholder="Digite os benefícios"><?php echo $info_vaga->getBeneficiosVaga(); ?></textarea>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>

                    </div>

                    <div class="col-md-6">
                    
                        <div class="row">

                            <div class="form-group col-6">
                                <label for="horEntradaVaga">Horário Entrada:</label>
                                <input type="time" class="form-control" name="horEntradaVaga" value="<?php echo $info_vaga->getHorEntradaVaga(); ?>">
                            </div>
                            <div class="form-group col-6">
                                <label for="horSaidaVaga">Horário Saída:</label>
                                <input type="time" class="form-control" name="horSaidaVaga" value="<?php echo $info_vaga->getHorSaidaVaga(); ?>">
                            </div>

                        </div>

                            <div class="form-group">
                                <label for="tipoVaga">Tipo Vaga:</label>
                                <select class="form-control" name="tipoVaga" required>
                                    <option value="1" <?php if ($info_vaga->getTipoVaga() == 1) { echo "selected"; } ?>>Estágio</option>
                                    <option value="2" <?php if ($info_vaga->getTipoVaga() == 2) { echo "selected"; } ?>>CLT</option>
                                </select>
                            </div>

                        <?php
                            }
                        } else {
                            Application::redirect('?controle=Vaga&acao=visualizarVaga');
                        }
                        ?>

                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>