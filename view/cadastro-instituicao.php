<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Instituição</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Cadastro Instituição</b>
                </h3>

                <hr>

                <div class="row form">

                    <div class="col-md-6">

                        <form method='post' action="?controle=Instituicao&acao=cadastroInstituicao">
                            <div class="form-group">
                                <label for="instLogin">Login:</label>
                                <input type="text" class="form-control" name="instLogin" placeholder="Digite o login" required>
                            </div>
                            <div class="form-group">
                                <label for="instSenha">Senha:</label>
                                <input type="password" class="form-control" name="instSenha" placeholder="Digite a senha" required>
                            </div>
                            <div class="form-group">
                                <label for="instNome">Nome:</label>
                                <input type="text" class="form-control" name="instNome" placeholder="Digite o nome da instituição" required>
                            </div>
                            <div class="form-group">
                                <label for="instCNPJ">CNPJ:</label>
                                <input type="text" class="form-control" name="instCNPJ" placeholder="00.000.000/0000-00" maxlength="18" required>
                            </div>
                            <div class="form-group">
                                <label for="instTelefone">Telefone:</label>
                                <input type="text" class="form-control" name="instTelefone" placeholder="(00) 0000-0000" maxlength="14" required>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>

                    </div>

                    <div class="col-md-6">

                            <div class="row">

                                <div class="form-group col-6">
                                    <label for="instEstad">Estado:</label>
                                    <input type="text" class="form-control" name="instEstad" placeholder="Selecione o estado" maxlength="2" required>
                                </div>
                                <div class="form-group col-6">
                                    <label for="instCid">Cidade:</label>
                                    <input type="text" class="form-control" name="instCid" placeholder="Selecione a cidade" required>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="instEnd">Endereço:</label>
                                <input type="text" class="form-control" name="instEnd" placeholder="Digite o endereço" required>
                            </div>
                            <div class="form-group">
                                <label for="instNum">Numero:</label>
                                <input type="number" class="form-control" name="instNum" placeholder="Digite o numero do endereço" min="1" required>
                            </div>
                                
                            <div class="form-group">
                                <label for="instBairro">Bairro:</label>
                                <input type="text" class="form-control" name="instBairro" placeholder="Digite o bairro" required>
                            </div>

                        </form>

                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>