<?php

session_start();

if (!isset($_SESSION['AcessoAluno'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

?>

<!doctype html>
<html lang="en">

<head>

    <?php 
        include ("style/head.php");   
    ?>

    <title>Painel de Controle</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="admin-control-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Painel de Controle</b>
                </h3>

                <hr>

                <div class="text-center">

                    <a class="btn btn-primary" href="?controle=Vaga&acao=visualizarVagasAluno" role="button">Visualizar Vagas</a>

                    <?php if ($_SESSION['SIT_ESTAG'] == 1) {
                        echo 
                        "<a class='btn btn-success disabled' href='?controle=Estagio&acao=visualizarEstagios' role='button'>Visualizar Estágio</a>";
                    } else if ($_SESSION['SIT_ESTAG'] == 2) {
                        echo 
                        "<a class='btn btn-success' href='?controle=Estagio&acao=administrarEstagio' role='button'>Administrar Estágio</a>";
                    }
                    ?>

                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>