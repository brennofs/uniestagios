<?php

session_start();

if (!isset($_SESSION['AcessoAluno'])) {
    
    session_destroy();
    Application::redirect('?controle=Index&acao=index');

}

$v_params = $this->getParams();
$estagio = $v_params['estagio'];

?>

<!doctype html>
<html lang="en">

<head>
    
    <?php 
        include ("style/head.php");   
    ?>

    <title>Cadastro Unidade Concedente</title>
</head>

<body>

    <?php 
        include ("style/navbar.php");   
    ?>

    <div class="sign-up-body">

        <div class="container">

            <div class="col-12 home mx-auto">

                <h3 class="text-center">
                    <b>Unidade Concedente</b>
                </h3>

                <hr>

                <h6 class="text-center">
                    <b>Digite abaixo as informações referentes a unidade concedente</b>
                </h6>

                <hr>

                <div class="row form">

                    <div class="col-md-6">

                        <form method='post' action="?controle=UnidadeConcedente&acao=cadastroUnidadeConcedente">
                            <input type="hidden" name="idEstagio" value="<?php echo $estagio; ?>">
                            <div class="form-group">
                                <label for="razaoSocialDocumento">Razão Social:</label>
                                <input type="text" class="form-control" name="razaoSocialDocumento" required>
                            </div>
                            <div class="form-group">
                                <label for="CNPJDocumento">CNPJ:</label>
                                <input type="text" class="form-control" name="CNPJDocumento" placeholder="88.888.888/8888-88" maxlength="18">
                            </div>
                            <div class="form-group">
                                <label for="CPFDocumento">CPF:</label>
                                <input type="text" class="form-control" name="CPFDocumento" placeholder="888.888.888-88" maxlength="14">
                            </div>
                            <div class="form-group">
                                <label for="inscEstadualDocumento">Inscrição Estadual:</label>
                                <input type="text" class="form-control" name="inscEstadualDocumento" placeholder="888.888.888.888" maxlength="15" required>
                            </div>
                            <div class="form-group">
                                <label for="telefoneDocumento">Telefone:</label>
                                <input type="text" class="form-control" name="telefoneDocumento" placeholder="(88) 8888-8888" maxlength="14" required>
                            </div>

                            <button type="submit" class="btn btn-success">Enviar</button>

                    </div>

                    <div class="col-md-6">

                            <div class="row">

                                <div class="form-group col-6">
                                    <label for="estadoDocumento">Estado:</label>
                                    <input type="text" class="form-control" name="estadoDocumento" placeholder="Selecione o estado" maxlength="2" required>
                                </div>
                                <div class="form-group col-6">
                                    <label for="cidadeDocumento">Cidade:</label>
                                    <input type="text" class="form-control" name="cidadeDocumento" placeholder="Selecione a cidade" required>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="CEPDocumento">CEP:</label>
                                <input type="text" class="form-control" name="CEPDocumento" placeholder="88888-888" maxlength="9" required>
                            </div>

                            <div class="form-group">
                                <label for="enderecoDocumento">Endereço:</label>
                                <input type="text" class="form-control" name="enderecoDocumento" placeholder="Digite o endereço" required>
                            </div>

                            <div class="form-group">
                                <label for="bairroDocumento">Bairro:</label>
                                <input type="text" class="form-control" name="bairroDocumento" placeholder="Digite o bairro" required>
                            </div>

                        </form>
                    
                    </div>
                
                </div>

            </div>

        </div>

    </div>

    <?php 
        include ("style/footer.php");
    ?>

</body>

</html>