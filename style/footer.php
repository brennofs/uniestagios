<footer>

    <div class="container">

        <div class="row footer">

            <div class="col-12 col-md-6 text-center">
                <p>© UniEstagios Todos os direitos reservados.</p>
            </div>

            <div class="col-12 col-md-6 text-center">
                <p>UniEstagios Facilitando sua busca por estágios!</p>
            </div>

        </div>

    </div>

</footer>