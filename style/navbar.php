<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="?controle=Index&acao=index">
            <img src="images/uniestagios.png" class="img-fluid">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="?controle=Index&acao=index">Home</a>
                </li>
                <?php 

                if (!isset($_SESSION['AcessoAluno']) && !isset($_SESSION['AcessoInstituicao']) && !isset($_SESSION['AcessoAdmin'])) {
                
                    echo
                    "<li class='nav-item dropdown'>
                        <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true'
                            aria-expanded='false'>
                            Login
                        </a>
                        <div class='dropdown-menu' aria-labelledby='navbarDropdown'>
                            <a class='dropdown-item painel' href='?controle=Admin&acao=LoginAdmin'>
                                <p>Admin
                                    <i class='fas fa-user-secret'></i>
                                </p>
                            </a>
                            <a class='dropdown-item aluno' href='?controle=Aluno&acao=loginAluno'>
                                <p>Alunos
                                    <i class='fa fa-user'></i>
                                </p>
                            </a>
                            <a class='dropdown-item instituicao' href='?controle=Instituicao&acao=loginInstituicao'>
                                <p>Instituições
                                    <i class='fa fa-users'></i>
                                </p>
                            </a>
                            <a class='dropdown-item btn disabled empresa' href='?controle=Empresa&acao=loginEmpresa'>
                                <p>Empresas
                                    <i class='fa fa-handshake'></i>
                                </p>
                            </a>
                        </div>
                    </li>
                    <li class='nav-item dropdown'>
                        <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown2' role='button' data-toggle='dropdown' aria-haspopup='true'
                            aria-expanded='false'>
                            Cadastre-se
                        </a>
                        <div class='dropdown-menu' aria-labelledby='navbarDropdown2'>
                            <a class='dropdown-item instituicao' href='?controle=Instituicao&acao=cadastrarInstituicao'>
                                <p>Instituições
                                    <i class='fa fa-users'></i>
                                </p>
                            </a>
                            <a class='dropdown-item btn disabled empresa' href='?controle=Empresa&acao=empresa'>
                                <p>Empresas
                                    <i class='fa fa-handshake'></i>
                                </p>
                            </a>
                        </div>
                    </li>";

                } else {

                    if (isset($_SESSION['AcessoAluno'])) {

                        echo
                        "<li class='nav-item dropdown'>
                            <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown2' role='button' data-toggle='dropdown' aria-haspopup='true'
                                aria-expanded='false'>"
                                . $_SESSION['NOME'] . 
                                " <i class='fa fa-user'></i>
                            </a>
                            <div class='dropdown-menu' aria-labelledby='navbarDropdown2'>
                                <a class='dropdown-item painel' href='?controle=Aluno&acao=aluno'>
                                    <p>Painel de Controle
                                        <i class='fas fa-desktop'></i>
                                    </p>
                                </a>
                                <a class='dropdown-item instituicao' href='?controle=Acesso&acao=sairAcesso'>
                                    <p>Sair
                                        <i class='fas fa-sign-out-alt'></i>
                                    </p>
                                </a>
                            </div>
                        </li>";
                    } else if (isset($_SESSION['AcessoInstituicao'])) {

                        echo
                        "<li class='nav-item dropdown'>
                            <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown2' role='button' data-toggle='dropdown' aria-haspopup='true'
                                aria-expanded='false'>"
                                . $_SESSION['NOME'] .
                                " <i class='fa fa-users'></i>
                            </a>
                            <div class='dropdown-menu' aria-labelledby='navbarDropdown2'>
                                <a class='dropdown-item painel' href='?controle=Instituicao&acao=instituicao'>
                                    <p>Painel de Controle
                                        <i class='fas fa-desktop'></i>
                                    </p>
                                </a>
                                <a class='dropdown-item instituicao' href='?controle=Acesso&acao=sairAcesso'>
                                    <p>Sair
                                        <i class='fas fa-sign-out-alt'></i>
                                    </p>
                                </a>
                            </div>
                        </li>";

                    } else {

                        echo
                        "<li class='nav-item dropdown'>
                            <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown2' role='button' data-toggle='dropdown' aria-haspopup='true'
                                aria-expanded='false'>"
                                . $_SESSION['NOME'] .
                                " <i class='fas fa-user-secret'></i>
                            </a>
                            <div class='dropdown-menu' aria-labelledby='navbarDropdown2'>
                                <a class='dropdown-item painel' href='?controle=Admin&acao=admin'>
                                    <p>Painel de Controle
                                        <i class='fas fa-desktop'></i>
                                    </p>
                                </a>
                                <a class='dropdown-item instituicao' href='?controle=Acesso&acao=sairAcesso'>
                                    <p>Sair
                                        <i class='fas fa-sign-out-alt'></i>
                                    </p>
                                </a>
                            </div>
                        </li>";

                    }
                }
                ?>
            </ul>
        </div>
    </div>
</nav>