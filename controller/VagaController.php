<?php
require_once 'model/VagaModel.php';
require_once 'model/CursoModel.php';

class VagaController {

	public function cadastrarVagaAction() {
		$curso = new CursoModel();
		
		$cursos = $curso->listarCursos();

		$o_view = new View('view/cadastro-vaga.php');

		$o_view->setParams(array('cursos' => $cursos));

		$o_view->showContents();
	}

	public function visualizarVagaAction() {
		$vaga = new VagaModel();
		
		$vagas = $vaga->listarVagas();
		
		$o_view = new View('view/visualiza-vaga.php');
		
		$o_view->setParams(array('vagas' => $vagas));
		
		$o_view->showContents();
	}

	public function visualizarVagasAlunoAction() {
		$vaga = new VagaModel();
		
		$vagas = $vaga->listarVagasAluno();
		
		$o_view = new View('view/visualiza-vaga-aluno.php');
		
		$o_view->setParams(array('vagas' => $vagas));
		
		$o_view->showContents();
	}

	public function removerAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$vaga = new VagaModel();
			
			$vaga->removerVaga($_GET['q']);
			
		}else{
			echo "ERRO: carregarRemoverAction ";
			exit;
		}

		Application::redirect('?controle=Vaga&acao=visualizarVaga');
	}

	public function visualizarAlterarAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$objeto_vaga = new VagaModel();
			
			$vaga = $objeto_vaga->visualizarAlterarVaga($_GET['q']);

			$o_view = new View('view/alterar-vaga.php');
		
			$o_view->setParams(array('vaga' => $vaga));
		
			$o_view->showContents();
			
		} else{
			echo "ERRO: carregarVisualizarAlterarAction ";
			exit;
		}

	}

	public function alterarAction() {
		$vaga = new VagaModel();

		if(!empty($_POST['IDVaga']) && !empty($_POST['nomeVaga']) && !empty($_POST['empresaVaga']) && !empty($_POST['descVaga']) && !empty($_POST['tipoVaga'])) {

			$ID_VAGA = $_POST['IDVaga'];
			$NOME = $_POST['nomeVaga'];
			$EMPRESA = $_POST['empresaVaga'];
			$DESCRICAO = $_POST['descVaga'];

			if (!empty($_POST['salarioVaga'])) { 
				$SALARIO = $_POST['salarioVaga'];
			} else { 
				$SALARIO = 0.00;
			};

			if (!empty($_POST['horEntradaVaga'])) { 
				$HORARIO_ENTRADA = $_POST['horEntradaVaga'];
			} else { 
				$HORARIO_ENTRADA = "Não informado";
			};

			if (!empty($_POST['horSaidaVaga'])) { 
				$HORARIO_SAIDA = $_POST['horSaidaVaga'];
			} else { 
				$HORARIO_SAIDA = "Não informado";
			};
			
			if (!empty($_POST['beneficiosVaga'])) { 
				$BENEFICIOS = $_POST['beneficiosVaga'];
			} else { 
				$BENEFICIOS = "Não informado";
			};

			$TIPO_VAGA = $_POST['tipoVaga'];

			$vaga->setIDVaga($ID_VAGA);
			$vaga->setNomeVaga($NOME);
			$vaga->setEmpresaVaga($EMPRESA);
			$vaga->setDescricaoVaga($DESCRICAO);
			$vaga->setSalarioVaga($SALARIO);
			$vaga->setBeneficiosVaga($BENEFICIOS);
			$vaga->setHorEntradaVaga($HORARIO_ENTRADA);
			$vaga->setHorSaidaVaga($HORARIO_SAIDA);
			$vaga->setTipoVaga($TIPO_VAGA);

			$vaga->alterarVaga($vaga);
		
		} else {
			Application::redirect('?controle=Vaga&acao=visualizarVaga');

		} 
		
		Application::redirect('?controle=Vaga&acao=visualizarVaga');

	}

	public function cadastroVagaAction() {

		session_start();

		$vaga = new VagaModel();

		if(!empty($_POST['nomeVaga']) && !empty($_POST['empresaVaga']) && !empty($_POST['descVaga']) && !empty($_POST['tipoVaga']) && !empty($_POST['ofertaCursoVaga'])) {

			$NOME = $_POST['nomeVaga'];
			$EMPRESA = $_POST['empresaVaga'];
			$DESCRICAO = $_POST['descVaga'];

			if (!empty($_POST['salarioVaga'])) { 
				$SALARIO = $_POST['salarioVaga'];
			} else { 
				$SALARIO = 0.00;
			};

			if (!empty($_POST['horEntradaVaga'])) { 
				$HORARIO_ENTRADA = $_POST['horEntradaVaga'];
			} else { 
				$HORARIO_ENTRADA = "Não informado";
			};

			if (!empty($_POST['horSaidaVaga'])) { 
				$HORARIO_SAIDA = $_POST['horSaidaVaga'];
			} else { 
				$HORARIO_SAIDA = "Não informado";
			};
			
			if (!empty($_POST['beneficiosVaga'])) { 
				$BENEFICIOS = $_POST['beneficiosVaga'];
			} else { 
				$BENEFICIOS = "Não informado";
			};
			
			$SITUACAO_VAGA = 1;
			$TIPO_VAGA = $_POST['tipoVaga'];

			$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];
			
			$ID_CURSOS_OFERTA = $_POST['ofertaCursoVaga'];

			$vaga->setNomeVaga($NOME);
			$vaga->setEmpresaVaga($EMPRESA);
			$vaga->setDescricaoVaga($DESCRICAO);
			$vaga->setSalarioVaga($SALARIO);
			$vaga->setHorEntradaVaga($HORARIO_ENTRADA);
			$vaga->setHorSaidaVaga($HORARIO_SAIDA);
			$vaga->setBeneficiosVaga($BENEFICIOS);
			$vaga->setSituacaoVaga($SITUACAO_VAGA);
			$vaga->setTipoVaga($TIPO_VAGA);
			$vaga->setIDInstituicaoVaga($ID_INSTITUICAO);
			$vaga->setOfertaVaga($ID_CURSOS_OFERTA);

			$vaga->cadastrarVaga($vaga);
		
		} else {
			Application::redirect('?controle=Instituicao&acao=instituicao');
			
		} 
		
		Application::redirect('?controle=Instituicao&acao=instituicao');

	}
}
?>