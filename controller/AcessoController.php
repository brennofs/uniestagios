<?php

class AcessoController {

	public function validarAcessoAdminAction() {
			
		$login = DataFilter::cleanString($_POST['login']);
		$senha = DataFilter::cleanString($_POST['senha']);

		$sql = new Sql();

		if (isset($login) && isset($senha)){
		
			$result = $sql->result("SELECT * FROM ADMINS WHERE LOGIN_ADM = :LOGIN_ADM AND SENHA = :SENHA", array(
			":LOGIN_ADM"=>$login,
			":SENHA"=>$senha));
		
			if (count($result) == 1) {
		
				$row = $result[0];
		
				session_start();
				$_SESSION['AcessoAdmin'] = true;
				$_SESSION['ID_ADMIN'] = $row['ID_ADMIN'];
				$_SESSION['NOME'] = $row['NOME'];
		
				Application::redirect('?controle=Admin&acao=admin');
		
			} else {
			   session_destroy();
			   Application::redirect('?controle=Index&acao=index');
		   }
		} else {
		   session_destroy();
		   Application::redirect('?controle=Index&acao=index');
	   }

   }
	
	public function validarAcessoAlunoAction() {
			
		 $login = DataFilter::cleanString($_POST['login']);
		 $senha = DataFilter::cleanString($_POST['senha']);

		 $sql = new Sql();

		 if (isset($login) && isset($senha)){
		 
			 $result = $sql->result("SELECT * FROM aluno WHERE RA = :RA AND SENHA = :SENHA", array(
			 ":RA"=>$login,
			 ":SENHA"=>$senha));
		 
			 if (count($result) == 1) {
		 
				 $row = $result[0];
		 
				 session_start();
				 $_SESSION['AcessoAluno'] = true;
				 $_SESSION['ID_ALUNO'] = $row['ID_ALUNO'];
				 $_SESSION['RA'] = $row['RA'];
				 $_SESSION['NOME'] = $row['NOME'];
				 $_SESSION['DATA_NASC'] = $row['DATA_NASC'];
				 $_SESSION['RG'] = $row['RG'];
				 $_SESSION['CPF'] = $row['CPF'];
				 $_SESSION['ESTADO'] = $row['ESTADO'];
				 $_SESSION['CIDADE'] = $row['CIDADE'];
				 $_SESSION['CEP'] = $row['CEP'];
				 $_SESSION['BAIRRO'] = $row['BAIRRO'];
				 $_SESSION['FONE'] = $row['FONE'];
				 $_SESSION['CEL'] = $row['CEL'];
				 $_SESSION['EMAIL'] = $row['EMAIL'];
				 $_SESSION['PORTADOR_DEF'] = $row['PORTADOR_DEF'];
				 $_SESSION['ESTAG_OBRIG'] = $row['ESTAG_OBRIG'];
				 $_SESSION['SEMESTRE'] = $row['SEMESTRE'];
				 $_SESSION['PERIODO'] = $row['PERIODO'];
				 $_SESSION['SIT_MAT'] = $row['SIT_MAT'];
				 $_SESSION['SIT_ESTAG'] = $row['SIT_ESTAG'];
				 $_SESSION['ID_INSTITUICAO'] = $row['ID_INSTITUICAO'];
				 $_SESSION['ID_CURSO'] = $row['ID_CURSO'];
		 
				 Application::redirect('?controle=Aluno&acao=aluno');
		 
			 } else {
				session_destroy();
				Application::redirect('?controle=Index&acao=index');
			}
		 } else {
			session_destroy();
			Application::redirect('?controle=Index&acao=index');
		}

	}

	public function validarAcessoInstituicaoAction() {
			
		$login = DataFilter::cleanString($_POST['login']);
		$senha = DataFilter::cleanString($_POST['senha']);

		$sql = new Sql();

		if (isset($login) && isset($senha)){
		
			$result = $sql->result("SELECT * FROM instituicao WHERE LOGIN_INST = :LOGIN_INST AND SENHA = :SENHA", array(
			":LOGIN_INST"=>$login,
			":SENHA"=>$senha));
		
			if (count($result) == 1) {
		
				$row = $result[0];

				if ($row['INST_VALIDADA'] == 3) {

					session_start();
					$_SESSION['AcessoInstituicao'] = true;
					$_SESSION['ID_INSTITUICAO'] = $row['ID_INSTITUICAO'];
					$_SESSION['NOME'] = $row['NOME'];
					$_SESSION['CNPJ'] = $row['CNPJ'];
					$_SESSION['ESTADO'] = $row['ESTADO'];
					$_SESSION['CIDADE'] = $row['CIDADE'];
					$_SESSION['ENDERECO'] = $row['ENDERECO'];
					$_SESSION['NUMERO'] = $row['NUMERO'];
					$_SESSION['BAIRRO'] = $row['BAIRRO'];
					$_SESSION['INST_VALIDADA'] = $row['INST_VALIDADA'];

					Application::redirect('?controle=Instituicao&acao=instituicao');

				} else {
					session_destroy();
			   		Application::redirect('?controle=Index&acao=index');
				}
			} else {
			   session_destroy();
			   Application::redirect('?controle=Index&acao=index');
		   }
		} else {
		   session_destroy();
		   Application::redirect('?controle=Index&acao=index');
	   }

   }
	
	public function sairAcessoAction() {
	
		session_start();
		session_destroy();
		Application::redirect('?controle=Index&acao=index');
		
	}
	
}
?>