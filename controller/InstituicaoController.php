<?php
require_once 'model/InstituicaoModel.php';

class InstituicaoController {

	public function instituicaoAction() {
		//redirecionando para a pagina inicial
		$o_view = new View('view/painel-instituicao.php');
		$o_view->showContents();
	}

	public function cadastrarInstituicaoAction() {
		$o_view = new View('view/cadastro-instituicao.php');
		$o_view->showContents();
	}

	public function loginInstituicaoAction() {
		$o_view = new View('view/login-instituicao.php');
		$o_view->showContents();
	}

	public function validarInstituicaoAction() {
		$instituicao = new InstituicaoModel();
		
		$instituicoes = $instituicao->listarInstituicoesNaoValidadas();
		
		$o_view = new View('view/valida-instituicao.php');
		
		$o_view->setParams(array('instituicoes' => $instituicoes));
		
		$o_view->showContents();
	}

	public function visualizarInstituicaoAction() {
		$instituicao = new InstituicaoModel();
		
		$instituicoes = $instituicao->listarInstituicoesValidadas();
		
		$o_view = new View('view/visualiza-instituicao.php');
		
		$o_view->setParams(array('instituicoes' => $instituicoes));
		
		$o_view->showContents();
	}

	public function visualizarRejeitadaAction() {
		$instituicao = new InstituicaoModel();
		
		$instituicoes = $instituicao->listarInstituicoesRejeitadas();
		
		$o_view = new View('view/visualiza-instituicao-rejeitada.php');
		
		$o_view->setParams(array('instituicoes' => $instituicoes));
		
		$o_view->showContents();
	}

	public function validarAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$instituicao = new InstituicaoModel();
			
			$instituicao->validarInstituicao($_GET['q']);
			
		}else{
			echo "ERRO: carregarValidarAction ";
			exit;
		}

		Application::redirect('?controle=Instituicao&acao=validarInstituicao');
	}

	public function rejeitarAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$instituicao = new InstituicaoModel();
			
			$instituicao->rejeitarInstituicao($_GET['q']);
			
		}else{
			echo "ERRO: carregarRejeitarAction ";
			exit;
		}

		Application::redirect('?controle=Instituicao&acao=validarInstituicao');
	}

	public function validarRejeitadaAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$instituicao = new InstituicaoModel();
			
			$instituicao->validarInstituicao($_GET['q']);
			
		}else{
			echo "ERRO: carregarValidarRejeitadaAction ";
			exit;
		}

		Application::redirect('?controle=Instituicao&acao=visualizarRejeitada');
	}

	public function removerRejeitadaAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$instituicao = new InstituicaoModel();
			
			$instituicao->removerInstituicao($_GET['q']);
			
		}else{
			echo "ERRO: carregarRemoverRejeitadaAction ";
			exit;
		}

		Application::redirect('?controle=Instituicao&acao=visualizarRejeitada');
	}

	public function removerAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$instituicao = new InstituicaoModel();
			
			$instituicao->removerInstituicao($_GET['q']);
			
		}else{
			echo "ERRO: carregarRemoverAction ";
			exit;
		}

		Application::redirect('?controle=Instituicao&acao=visualizarInstituicao');
	}

	public function cadastroInstituicaoAction() {
		date_default_timezone_set('America/Sao_Paulo');
		$instituicao = new InstituicaoModel();

		if(!empty($_POST['instLogin']) && !empty($_POST['instSenha']) && !empty($_POST['instNome']) && !empty($_POST['instCNPJ']) && !empty($_POST['instEstad']) && !empty($_POST['instCid']) && !empty($_POST['instEnd']) && !empty($_POST['instNum']) && !empty($_POST['instBairro']) && !empty($_POST['instTelefone'])) {

			$LOGIN_INST	= $_POST['instLogin'];
			$SENHA_INST = $_POST['instSenha'];
			$NOME_INST  = $_POST['instNome'];
			$CNPJ_INST 	= $_POST['instCNPJ'];
			$ESTADO_INST = $_POST['instEstad'];
			$CIDADE_INST = $_POST['instCid'];
			$ENDERECO_INST = $_POST['instEnd'];
			$NUMERO_INST = $_POST['instNum'];
			$BAIRRO_INST = $_POST['instBairro'];
			$TELEFONE_INST = $_POST['instTelefone'];
			$DATA_CADASTRO_INST = date("Y-m-d H:i:s");
			$VALIDADA_INST = 1;

			$instituicao->setLoginInst($LOGIN_INST);
			$instituicao->setSenhaInst($SENHA_INST);
			$instituicao->setNomeInst($NOME_INST);
			$instituicao->setCNPJInst($CNPJ_INST);
			$instituicao->setEstInst($ESTADO_INST);
			$instituicao->setCidInst($CIDADE_INST);
			$instituicao->setEndInst($ENDERECO_INST);
			$instituicao->setNumInst($NUMERO_INST);
			$instituicao->setBairroInst($BAIRRO_INST);
			$instituicao->setTelefoneInst($TELEFONE_INST);
			$instituicao->setDataCadastroInst($DATA_CADASTRO_INST);
			$instituicao->setValidadaInst($VALIDADA_INST);

			$instituicao->cadastrarInstituicao($instituicao);
		
		} else {
			Application::redirect('?controle=Index&acao=index');

		} 
		
		Application::redirect('?controle=Index&acao=index');

	}
}
?>