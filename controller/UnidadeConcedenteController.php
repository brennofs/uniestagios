<?php
require_once 'model/UnidadeConcedenteModel.php';
require_once 'model/EstagioModel.php';

class UnidadeConcedenteController {
	
	public function cadastrarUnidadeAction() {
		//redirecionando para a pagina inicial
		if (DataValidator::isNumeric($_GET['v']) == 1){

			$id_estagio = $_GET['v'];

			$unidade_concedente = new UnidadeConcedenteModel();
		
			$unidades_concedentes = $unidade_concedente->listarUnidadesConcedentes();

			if (!empty($unidades_concedentes)) {

				$o_view = new View('view/selecione-unidade-concedente.php');

				$o_view->setParams(array('estagio' => $id_estagio, 'unidades_concedentes' => $unidades_concedentes));

				$o_view->showContents();

			} else {
				$o_view = new View('view/cadastro-unidade-concedente.php');

				$o_view->setParams(array('estagio' => $id_estagio));

				$o_view->showContents();
			}
			
		}else{
			echo "ERRO: carregarCadastrarUnidadeAction ";
			exit;
		}

		Application::redirect('?controle=Estagio&acao=administrarEstagio');
	}

	public function cadastrarNovaUnidadeAction() {
		//redirecionando para a pagina inicial
		if (DataValidator::isNumeric($_GET['v']) == 1){

			$id_estagio = $_GET['v'];

			$o_view = new View('view/cadastro-unidade-concedente.php');

			$o_view->setParams(array('estagio' => $id_estagio));

			$o_view->showContents();
			
		}else{
			echo "ERRO: carregarCadastrarNovaUnidadeAction ";
			exit;
		}

		Application::redirect('?controle=Estagio&acao=administrarEstagio');
	}

	public function selecioneUnidadeConcedenteAction() {
		
		$estagio = new EstagioModel();

		if(!empty($_POST['idEstagio']) && !empty($_POST['idUnidadeConcedente'])) {

			$id_estagio = $_POST['idEstagio'];
			$id_unidade_concedente = $_POST['idUnidadeConcedente'];

            $estagio->setIDEstagio($id_estagio);
            $estagio->setIDUnidadeConcedenteEstagio($id_unidade_concedente);
    
			$estagio->AdicionarIDUnidadeConcedente($estagio);
			
			Application::redirect('?controle=Estagio&acao=administrarEstagio');

		} else {
			Application::redirect('?controle=Estagio&acao=administrarEstagio');
		}
	}

	public function cadastroUnidadeConcedenteAction() {

		$unidade_concedente = new UnidadeConcedenteModel();

		if(!empty($_POST['idEstagio']) && !empty($_POST['razaoSocialDocumento']) && !empty($_POST['inscEstadualDocumento']) && !empty($_POST['telefoneDocumento']) && !empty($_POST['estadoDocumento'])
		&& !empty($_POST['cidadeDocumento']) && !empty($_POST['CEPDocumento']) && !empty($_POST['enderecoDocumento']) && !empty($_POST['bairroDocumento']) 
		&& (!empty($_POST['CNPJDocumento']) || !empty($_POST['CPFDocumento']))) {

			$id_estagio = $_POST['idEstagio'];
			$razao = $_POST['razaoSocialDocumento'];
			$insc_estadual = $_POST['inscEstadualDocumento'];
			$telefone = $_POST['telefoneDocumento'];
			$estado = $_POST['estadoDocumento'];
			$cidade = $_POST['cidadeDocumento'];
			$cep = $_POST['CEPDocumento'];
			$endereco = $_POST['enderecoDocumento'];
			$bairro = $_POST['bairroDocumento'];

			if (!empty($_POST['CNPJDocumento'])) { 
				$CNPJ = $_POST['CNPJDocumento'];
			} else { 
				$CNPJ = "";
			};

			if (!empty($_POST['CPFDocumento'])) { 
				$CPF = $_POST['CPFDocumento'];
			} else { 
				$CPF = "";
			};

			$unidade_concedente->setRazaoSocial($razao);
			$unidade_concedente->setInscEstadual($insc_estadual);
			$unidade_concedente->setCNPJ($CNPJ);
			$unidade_concedente->setCPF($CPF);
			$unidade_concedente->setTelefone($telefone);
			$unidade_concedente->setEstado($estado);
			$unidade_concedente->setCidade($cidade);
			$unidade_concedente->setCEP($cep);
			$unidade_concedente->setEndereco($endereco);
			$unidade_concedente->setBairro($bairro);

			$unidade_concedente->cadastrarUnidadeConcedente($unidade_concedente, $id_estagio);
		
		} else {
			Application::redirect('?controle=Estagio&acao=administrarEstagio');

		} 
		Application::redirect('?controle=Estagio&acao=administrarEstagio');
	}

}
?>