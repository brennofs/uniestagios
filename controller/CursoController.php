<?php
require_once 'model/CursoModel.php';

class CursoController {

	public function cadastrarCursoAction() {
		$o_view = new View('view/cadastro-curso.php');
		$o_view->showContents();
	}


	public function visualizarCursoAction() {
		$curso = new CursoModel();
		
		$cursos = $curso->listarCursos();
		
		$o_view = new View('view/visualiza-curso.php');
		
		$o_view->setParams(array('cursos' => $cursos));
		
		$o_view->showContents();
	}

	public function removerAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$curso = new CursoModel();
			
			$curso->removerCurso($_GET['q']);
			
		}else{
			echo "ERRO: carregarRemoverAction ";
			exit;
		}

		Application::redirect('?controle=Curso&acao=visualizarCurso');
	}

	public function visualizarAlterarAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$objeto_curso = new CursoModel();
			
			$curso = $objeto_curso->visualizarAlterarCurso($_GET['q']);

			$o_view = new View('view/alterar-curso.php');
		
			$o_view->setParams(array('curso' => $curso));
		
			$o_view->showContents();
			
		} else{
			echo "ERRO: carregarAlterarAction ";
			exit;
		}

	}

	public function alterarAction() {
		$curso = new CursoModel();

		if(!empty($_POST['IDCurso']) && !empty($_POST['cursoNome']) && !empty($_POST['cursoDuracao'])) {

			$ID_CURSO = $_POST['IDCurso'];
			$NOME = $_POST['cursoNome'];
			$DURACAO = $_POST['cursoDuracao'];

			$curso->setIDCurso($ID_CURSO);
			$curso->setNomeCurso($NOME);
			$curso->setDuracaoCurso($DURACAO);

			$curso->alterarCurso($curso);
		
		} else {
			Application::redirect('?controle=Curso&acao=visualizarCurso');

		} 
		
		Application::redirect('?controle=Curso&acao=visualizarCurso');

	}

	public function cadastroCursoAction() {
		$curso = new CursoModel();

		if(!empty($_POST['cursoNome']) && !empty($_POST['cursoDuracao'])) {

			$NOME = $_POST['cursoNome'];
			$DURACAO = $_POST['cursoDuracao'];

			session_start();
			$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];

			$curso->setNomeCurso($NOME);
			$curso->setDuracaoCurso($DURACAO);
			$curso->setIDInstCurso($ID_INSTITUICAO);

			$curso->cadastrarCurso($curso);
		
		} else {
			Application::redirect('?controle=Instituicao&acao=instituicao');

		} 
		
		Application::redirect('?controle=Instituicao&acao=instituicao');

	}
}
?>