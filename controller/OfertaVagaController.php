<?php
require_once 'model/OfertaVagaModel.php';
require_once 'model/CursoModel.php';

class OfertaVagaController {

	public function visualizarOfertaVagaAction() {

		if (DataValidator::isNumeric($_GET['v']) == 1){
			
			$oferta_vaga = new OfertaVagaModel();

			$oferta_vagas = $oferta_vaga->listarOfertaVagas($_GET['v']);

			$o_view = new View('view/visualiza-oferta-vaga.php');
		
			$o_view->setParams(array('oferta_vagas' => $oferta_vagas));
		
			$o_view->showContents();
			
		}else{
			echo "ERRO: carregarRemoverAction ";
			exit;
		}

	}

	public function visualizarAdicionarAction() {
		
		if (DataValidator::isNumeric($_GET['v']) == 1){

			$curso = new CursoModel();
		
			$cursos = $curso->listarCursos();
			
			$objeto_oferta_vaga = new OfertaVagaModel();
			
			$oferta_vaga = $objeto_oferta_vaga->visualizarAdicionar($_GET['v']);

			$o_view = new View('view/cadastro-oferta-vaga.php');
		
			$o_view->setParams(array('oferta_vaga' => $oferta_vaga, 'cursos' => $cursos));
		
			$o_view->showContents();
			
		} else{
			echo "ERRO: carregarVisualizarAdicionarAction ";
			exit;
		}

	}

	public function adicionarAction() {

		session_start();
		
		$oferta_vaga = new OfertaVagaModel();

		if(!empty($_POST['IDVaga']) && !empty($_POST['ofertaCursoVaga'])) {

			$ID_VAGA = $_POST['IDVaga'];
			$OFERTA_CURSO = $_POST['ofertaCursoVaga'];
			
			$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];

			$oferta_vaga->setIDVaga($ID_VAGA);
			$oferta_vaga->setIDCurso($OFERTA_CURSO);
			$oferta_vaga->setIDInstituicaoOfertaVaga($ID_INSTITUICAO);

			$oferta_vaga->adicionarOfertaVaga($oferta_vaga);
		
		} else {
			Application::redirect('?controle=OfertaVaga&acao=visualizarOfertaVaga&v='. $ID_VAGA);

		} 

		Application::redirect('?controle=OfertaVaga&acao=visualizarOfertaVaga&v='. $ID_VAGA);

	}

	public function removerAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$oferta_vaga = new OfertaVagaModel();
			
			$oferta_vaga->removerOfertaVaga($_GET['q']);
			
		}else{
			echo "ERRO: carregarRemoverAction ";
			exit;
		}
		
	}
}
?>