<?php
require_once 'model/EstagioModel.php';
require_once 'model/AlunoModel.php';

class EstagioController {

	public function visualizarEstagioAction() {
		$estagio = new EstagioModel();
		
		$estagios = $estagio->listarEstagios();
		
		$o_view = new View('view/visualiza-estagio.php');
		
		$o_view->setParams(array('estagios' => $estagios));
		
		$o_view->showContents();
	}

	public function visualizarEstagioFinalizadoAction() {
		$estagio = new EstagioModel();
		
		$estagios = $estagio->listarEstagiosFinalizados();
		
		$o_view = new View('view/visualiza-estagio-finalizado.php');
		
		$o_view->setParams(array('estagios' => $estagios));
		
		$o_view->showContents();
	}

	public function visualizarEstagioCanceladoAction() {
		$estagio = new EstagioModel();
		
		$estagios = $estagio->listarEstagiosCancelados();
		
		$o_view = new View('view/visualiza-estagio-cancelado.php');
		
		$o_view->setParams(array('estagios' => $estagios));
		
		$o_view->showContents();
	}

	public function cancelarAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1 && DataValidator::isNumeric($_GET['a']) == 1){
			
			$estagio = new EstagioModel();
			$aluno = new AlunoModel();
			
			$estagio->cancelarEstagio($_GET['q']);
			$aluno->mudarSitEstagioSemEstagioAluno($_GET['a']);
			
		}else{
			echo "ERRO: carregarCancelarAction ";
			exit;
		}

		Application::redirect('?controle=Estagio&acao=visualizarEstagio');
	}

	public function finalizarAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1 && DataValidator::isNumeric($_GET['a']) == 1){
			
			$estagio = new EstagioModel();
			$aluno = new AlunoModel();
			
			$estagio->finalizarEstagio($_GET['q']);
			$aluno->mudarSitEstagioSemEstagioAluno($_GET['a']);
			
		}else{
			echo "ERRO: carregarFinalizarAction ";
			exit;
		}

		Application::redirect('?controle=Estagio&acao=visualizarEstagio');
	}

	public function visualizarAlterarDuracaoAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$objeto_estagio = new EstagioModel();
			
			$estagio = $objeto_estagio->visualizarAlterarEstagio($_GET['q']);

			$o_view = new View('view/alterar-estagio.php');
		
			$o_view->setParams(array('estagio' => $estagio));
		
			$o_view->showContents();
			
		} else{
			echo "ERRO: carregarVisualizarAlterarDuracaoAction ";
			exit;
		}

	}

	public function alterarDuracaoAction() {
		$estagio = new EstagioModel();

		if(!empty($_POST['IDEstagio']) && !empty($_POST['estagioDuracao'])) {

			$ID_ESTAGIO = $_POST['IDEstagio'];
			$DURACAO = $_POST['estagioDuracao'];

			$estagio->setIDEstagio($ID_ESTAGIO);
			$estagio->setDuracao($DURACAO);

			$estagio->alterarEstagio($estagio);
		
		} else {
			Application::redirect('?controle=Estagio&acao=visualizarEstagio');

		} 
		
		Application::redirect('?controle=Estagio&acao=visualizarEstagio');

	}

	public function administrarEstagioAction() {
		//redirecionando para a pagina inicial

		session_start();

		$ID_ALUNO = $_SESSION['ID_ALUNO'];

		$estagio = new EstagioModel();
		
		$estagios = $estagio->listarEstagioAluno($ID_ALUNO);

		if (!empty($estagios)) {

			$o_view = new View('view/painel-estagio-aluno.php');

			$o_view->setParams(array('estagio' => $estagios));

			$o_view->showContents();

		} else {
			Application::redirect('?controle=Aluno&acao=aluno');
		}
	}

	public function cadastrarEstagioAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){

			$ID_VAGA = $_GET['q'];
		
			$o_view = new View('view/cadastro-estagio.php');
		
			$o_view->setParams(array('vaga' => $ID_VAGA));
		
			$o_view->showContents();
			
		}else{
			Application::redirect('?controle=Aluno&acao=aluno');
		}

		Application::redirect('?controle=Aluno&acao=aluno');
	}

	public function cadastroEstagioAction() {

		session_start();
			
		$estagio = new EstagioModel();
		$aluno = new AlunoModel();

		if(!empty($_POST['duracao']) && !empty($_POST['dataInicio']) && !empty($_POST['dataFim']) && !empty($_POST['horarioEntrada']) 
		&& !empty($_POST['horarioSaida']) && !empty($_POST['horasSemanais']) && !empty($_POST['repLegal']) 
		&& !empty($_POST['cargoRep']) && !empty($_POST['supEstagio']) && !empty($_POST['formAcademicaSup'])
		&& !empty($_POST['regProfissionalSup']) && !empty($_POST['orgaoRegSup']) && !empty($_POST['idVaga'])) {

			$DURACAO = $_POST['duracao'];
			$DATA_INICIO = $_POST['dataInicio'];
			$DATA_FIM = $_POST['dataFim'];
			$HORARIO_ENTRADA = $_POST['horarioEntrada'];
			$HORARIO_SAIDA = $_POST['horarioSaida'];
			$HORAS_SEMANAIS = $_POST['horasSemanais'];
			$REPRESENTANTE_LEGAL = $_POST['repLegal'];
			$CARGO_REPRESENTANTE = $_POST['cargoRep'];
			$SUPERVISOR_ESTAGIO = $_POST['supEstagio'];
			$FORMACAO_ACADEMICA_SUPERVISOR = $_POST['formAcademicaSup'];
			$REGISTRO_PROFISSIONAL_SUPERVISOR = $_POST['regProfissionalSup'];
			$ORGAO_REGISTRO_SUPERVISOR = $_POST['orgaoRegSup'];
			$SIT_ESTAG = 1;
			$ID_ALUNO = $_SESSION['ID_ALUNO'];
			$ID_VAGA = $_POST['idVaga'];
			$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];

			$estagio->setDuracao($DURACAO);
			$estagio->setDataInicio($DATA_INICIO);
			$estagio->setDataFim($DATA_FIM);
			$estagio->setHorarioEntrada($HORARIO_ENTRADA);
			$estagio->setHorarioSaida($HORARIO_SAIDA);
			$estagio->setHorasSemanais($HORAS_SEMANAIS);
			$estagio->setRepLegal($REPRESENTANTE_LEGAL);
			$estagio->setCargoRep($CARGO_REPRESENTANTE);
			$estagio->setSupEstagio($SUPERVISOR_ESTAGIO);
			$estagio->setFormAcaSup($FORMACAO_ACADEMICA_SUPERVISOR);
			$estagio->setRegProfSup($REGISTRO_PROFISSIONAL_SUPERVISOR);
			$estagio->setOrgaoRegSup($ORGAO_REGISTRO_SUPERVISOR);
			$estagio->setSitEstagio($SIT_ESTAG);
			$estagio->setIDAlunoEstagio($ID_ALUNO);
			$estagio->setIDVagaEstagio($ID_VAGA);
			$estagio->setIDInstEstagio($ID_INSTITUICAO);

			$estagio->cadastroEstagio($estagio);
			$aluno->mudarSitEstagioEstagiandoAluno($ID_ALUNO);
		
		} else {
			Application::redirect('?controle=Aluno&acao=aluno');

		} 
		
		Application::redirect('?controle=Aluno&acao=aluno');

	}
}
?>