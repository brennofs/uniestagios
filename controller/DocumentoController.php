<?php
require_once 'model/AlunoModel.php';
require_once 'model/EstagioModel.php';
require_once 'model/UnidadeConcedenteModel.php';
require_once 'fpdf/fpdf.php';

class DocumentoController {

	public function cadastrarDocumentoAceiteAction() {

		if (DataValidator::isNumeric($_GET['v']) == 1){

			$ID_ESTAGIO = $_GET['v'];
		
			$o_view = new View('view/cadastro-documento-aceite.php');
		
			$o_view->setParams(array('estagio' => $ID_ESTAGIO));
		
			$o_view->showContents();
			
		}else{
			Application::redirect('?controle=Estagio&acao=administrarEstagio');
		}

	}

	public function cadastrarDocumentoAditivoAction() {

		if (DataValidator::isNumeric($_GET['v']) == 1){

			$ID_ESTAGIO = $_GET['v'];
		
			$o_view = new View('view/cadastro-documento-aditivo.php');
		
			$o_view->setParams(array('estagio' => $ID_ESTAGIO));
		
			$o_view->showContents();
			
		}else{
			Application::redirect('?controle=Estagio&acao=administrarEstagio');
		}

	}

	public function cadastrarDocumentoRescisaoAction() {

		if (DataValidator::isNumeric($_GET['v']) == 1){

			$ID_ESTAGIO = $_GET['v'];
		
			$o_view = new View('view/cadastro-documento-rescisao.php');
		
			$o_view->setParams(array('estagio' => $ID_ESTAGIO));
		
			$o_view->showContents();
			
		}else{
			Application::redirect('?controle=Estagio&acao=administrarEstagio');
		}

	}

	public function gerarDocumentoAceiteAction() {

		session_start();

		if (!isset($_SESSION['AcessoAluno'])) {
    
			session_destroy();
			Application::redirect('?controle=Index&acao=index');
		
		}

		if(!empty($_POST['numeroApoliceSeguro']) && !empty($_POST['nomeSeguradora']) && !empty($_POST['idEstagio'])) {

			$estagio = new EstagioModel();
			$unidade_concedente = new UnidadeConcedenteModel();

			$id_estagio = $_POST['idEstagio'];
			$numero_apolice = $_POST['numeroApoliceSeguro'];
			$nome_seguradora = $_POST['nomeSeguradora'];
			$valor_seguro = $_POST['valorSeguro'];
			$valor_bolsa = $_POST['valorBolsa'];
			$valor_vale_transporte = $_POST['valorValeTransporte'];
			$data_apartir = date("d-m-Y");

			if (!empty($valor_seguro)) {
				$valor_seguro = $_POST['valorSeguro'];
			}

			if (!empty($valor_bolsa)) {
				$valor_bolsa = $_POST['valorBolsa'];
			}

			if (!empty($valor_vale_transporte)) {
				$valor_vale_transporte = $_POST['valorValeTransporte'];
			}

			$estagio->setIDEstagio($id_estagio);
			$estagio->setNumeroApolice($numero_apolice);
			$estagio->setNomeSeguradora($nome_seguradora);
			$estagio->setValorSeguro($valor_seguro);
			$estagio->setValorBolsa($valor_bolsa);
			$estagio->setValorValeTransporte($valor_vale_transporte);
			$estagio->setDataCelebradaApartir($data_apartir);

			$estagio->AdicionarDocumentoAceite($estagio);

			if (empty($valor_seguro)) {
				$valor_seguro = "0.00";
			}

			if (empty($valor_bolsa)) {
				$valor_bolsa = "0.00";
			}

			if (empty($valor_vale_transporte)) {
				$valor_vale_transporte = "0.00";
			}

			$info_doc = $estagio->listarEstagioAluno($_SESSION['ID_ALUNO']);

			$representante_legal = $info_doc[0]->getRepLegal();
			$cargo_representante = $info_doc[0]->getCargoRep();
			$supervisor_estagio = $info_doc[0]->getSupEstagio();
			$formacao_academica_supervisor = $info_doc[0]->getFormAcaSup();
			$registro_profissional_supervisor = $info_doc[0]->getRegProfSup();
			$orgao_registro_supervisor = $info_doc[0]->getOrgaoRegSup();
			$horario_entrada = $info_doc[0]->getHorarioEntrada();
			$horario_saida = $info_doc[0]->getHorarioSaida();
			$horas_semanais = $info_doc[0]->getHorasSemanais();
			$data_inicio = $info_doc[0]->getDataInicio();
			$data_fim = $info_doc[0]->getDataFim();
			$data_estagio_inicio = date("d-m-Y", strtotime($data_inicio));
			$data_estagio_fim = date("d-m-Y", strtotime($data_fim));

			$info_unidade_concedente = $unidade_concedente->listarUnidadeConcedente($info_doc[0]->getIDUnidadeConcedenteEstagio());

			$razao = $info_unidade_concedente[0]->getRazaoSocial();
			$insc_estadual = $info_unidade_concedente[0]->getInscEstadual();
			$CNPJ = $info_unidade_concedente[0]->getCNPJ();
			$CPF = $info_unidade_concedente[0]->getCPF();
			$telefone = $info_unidade_concedente[0]->getTelefone();
			$estado = $info_unidade_concedente[0]->getEstado();
			$cidade = $info_unidade_concedente[0]->getCidade();
			$cep = $info_unidade_concedente[0]->getCEP();
			$endereco = $info_unidade_concedente[0]->getEndereco();
			$bairro = $info_unidade_concedente[0]->getBairro();

			$pdf = new FPDF();
			
			$pdf->AddPage();
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Cell(65, 25, $pdf->Image('images/IFSP logo.JPG',10, 10), 1);

			$pdf->Cell(125, 25, "" , 1);
			$pdf->SetFontSize(10);
			$pdf->Text(80, 18, "Instituto Federal de Educação, Ciência e Tecnologia de São Paulo");
			$pdf->Text(100, 23, "Diretoria Geral do Campus Campinas SP");
			$pdf->Text(110, 28, "Coordenadoria de Extensão");

			$pdf->Ln(30);

			$pdf->Cell(190, 12, "" , 1);
			$pdf->SetFontSize(12);
			$pdf->Text(65, 45, "TERMO DE COMPROMISSO DE ESTÁGIO");
			$pdf->SetFontSize(11);
			$pdf->Text(73, 50, "(Técnico/Tecnológico/Bacharelado)");

			$pdf->Ln(15);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFontSize(16);
			$pdf->Text(70, 61, "INSTITUIÇÃO DE ENSINO");

			$pdf->Ln(10);

			$pdf->Cell(190, 11, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 70, "Instituição");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(32, 70, ": INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DE SÃO PAULO/IFSP");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(75, 74, "(doravante denominado IFSP)");

			$pdf->Ln(11);

			$pdf->Cell(190, 16, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 80, "Endereco");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(30, 80, ": à Rodovia D. Pedro I (SP65) km 143,6 - Terminal Intermodal de Cargas de Campinas (TICC)");
			$pdf->Text(12, 85, "Campinas, SP - Brasil, Centro de Tecnologia da Informação Renato Archer (CTI).");
			$pdf->Text(12, 90, "Campinas - SP - ");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(42, 90, "CEP 13069-901");

			$pdf->Ln(16);

			$pdf->Cell(190, 5, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 96, "Fone: (19) 3746-6128");
			$pdf->Text(122, 96, "CNPJ: 10.882.594/0029-66");

			$pdf->Ln(5);

			$pdf->Cell(190, 11, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 101, "Representada pelo");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(48, 101, "seu Diretor Geral, Prof. Eberval Oliveira Castro, nomeado pela Portaria nº 1.352");
			$pdf->Text(12, 106, "publicada no Diário Oficial da União de 11 de abril de 2017.");

			$pdf->Ln(15);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Text(70, 118, "UNIDADE CONCEDENTE");

			$pdf->Ln(10);

			$pdf->Cell(190, 10, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 126, "Razão Social:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(12, 131, $razao);
			$pdf->Text(155, 126, "(doravante denominada");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(155, 131, "CONCEDENTE)");

			$pdf->Ln(10);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 136, "CNPJ:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 136, $CNPJ);
			$pdf->Text(75, 136, "(empresa)");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 136, "Insc. Estadual:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(140, 136, $insc_estadual);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 142, "CPF:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 142, $CPF);
			$pdf->Text(75, 142, "(autônomo)");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 142, "Fone:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 142, $telefone);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 148, "Endereço:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(32, 148, $endereco);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 154, "CEP:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(23, 154, $cep);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 154, "Estado:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(127, 154, $estado);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 160, "Cidade:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(28, 160, $cidade);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 166, "Bairro:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 166, $bairro);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 172, "Representante Legal: ");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 172, $representante_legal);
			
			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 178, "Cargo:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 178, $cargo_representante);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 184, "Supervisor de Estágio:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(57, 184, $supervisor_estagio);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 190, "Formação Acadêmica:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 190, $formacao_academica_supervisor);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 196, "Registro Profissional:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 196, $registro_profissional_supervisor);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 196, "Orgão:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 196, $orgao_registro_supervisor);

			$pdf->Ln(10);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Text(85, 207, "ESTAGIÁRIO");

			$pdf->Ln(10);

			$pdf->Cell(190, 10, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 216, "Nome:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(12, 221, $_SESSION['NOME']);
			$pdf->Text(155, 216, "(doravante denominada");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(155, 221, "ESTAGIÁRIO)");

			$pdf->Ln(10);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 226, "Curso:");
			$pdf->SetFont('Arial', '', 11);

			$aluno_objeto = new AlunoModel();
			$nome_curso = "";
			$nome_curso = $aluno_objeto->buscarNomeCursoAluno($_SESSION['ID_CURSO']);

			$pdf->Text(25, 226, $nome_curso);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(150, 226, "Prontuário:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(172, 226, $_SESSION['RA']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 232, "Período:");
			$pdf->SetFont('Arial', '', 11);

			$periodo = "";
			if($_SESSION['PERIODO'] == 1) { 
				$periodo = "Matutino";
			} else if ($_SESSION['PERIODO'] == 2) { 
				$periodo = "Vespertino";
			} else if ($_SESSION['PERIODO'] == 3) {
				$periodo = "Noturno";
			} else {
				$periodo = "Integral";
			}

			$pdf->Text(30, 232, $periodo);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 232, "Data de Nascimento:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(152, 232, $_SESSION['DATA_NASC']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 239, "RG:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(22, 239, $_SESSION['RG']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 239, "CPF:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 239, $_SESSION['CPF']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 245, "CEP:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(23, 245, $_SESSION['CEP']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 245, "Estado:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(127, 245, $_SESSION['ESTADO']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 251, "Cidade:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(28, 251, $_SESSION['CIDADE']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 257, "Bairro:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 257, $_SESSION['BAIRRO']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 263, "Fone:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 263, $_SESSION['FONE']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 263, "Cel:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(122, 263, $_SESSION['CEL']);
			
			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 269, "Email:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 269, $_SESSION['EMAIL']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(132, 269, "Portador de Deficiência:");
			$pdf->SetFont('Arial', '', 11);

			$portador_def = "";
			if($_SESSION['PORTADOR_DEF'] == 1) { 
				$portador_def = "Não";
			} else { 
				$portador_def = "Sim";
			}

			$pdf->Text(180, 269, $portador_def);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 275, "Estágio Obrigatório:");
			$pdf->SetFont('Arial', '', 11);

			$estag_obrig = "";
			if($_SESSION['ESTAG_OBRIG'] == 1) { 
				$estag_obrig = "Não";
			} else { 
				$estag_obrig = "Sim";
			}

			$pdf->Text(52, 275, $estag_obrig);

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "As partes supracitadas resolvem celebrar o presente Termo de Compromisso de Estagio, para realizacao de Estagio Curricular, em conformidade com a Lei numero 11.788, de 25 de setembro de 2008, e das clausulas e condicoes a seguir estipuladas.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "CLAUSULA PRIMEIRA - DO OBJETO - Constitui objeto do presente Termo a concessao de estagio curricular, entendendo-se como tal, o ato educativo escolar supervisionado, desenvolvido no ambiente de trabalho, que visa preparar para a empregabilidade, para a vida cidada e para o trabalho, por meio do exercicio de atividades correlatas a sua pretendida formacao profissional, em complementacao ao conhecimento teorico adquirido na Instituio de Ensino.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "CLAUSULA SEGUNDA - DA VIGENCIA - O vinculo de estagio, objeto do presente Termo de Compromisso tera inicio em $data_estagio_inicio e termino em $data_estagio_fim , desde que mantido o vinculo do ESTAGIARIO com a Instituicao de Ensino, nos termos da Lei 11.788/2008.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "CLAUSULA TERCEIRA - DO HORARIO DA JORNADA DO ESTAGIO - O horario de estagio sera das $horario_entrada as $horario_saida , totalizando $horas_semanais horas semanais.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "CLAUSULA QUARTA - DO DESENVOLVIMENTO DO ESTAGIO - Durante a realizacao do estagio, o ESTAGIARIO estara coberto pela apolice de seguro numero $numero_apolice, da Seguradora $nome_seguradora no valor de R$ $valor_seguro contra Acidentes Pessoais.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "CLAUSULA QUINTA - DAS OBRIGACOES DAS PARTES - Compete a UNIDADE CONCEDENTE DE ESTAGIO: Efetuar pagamento de bolsa-auxilio no valor de R$ $valor_bolsa diretamente ao ESTAGIARIO, quando prevista, e, efetuar pagamento de auxilio transporte no valor de R$ $valor_vale_transporte diretamente ao ESTAGIARIO, quando previsto.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "CLAUSULA SEXTA - DA RESCISAO - O presente Termo de Compromisso de Estagio extinguir-se-a automaticamente:
			A.	Pela conclusao, trancamento, desligamento e abandono do curso;
			B.	Nao cumprimento dos termos de compromisso;
			C.	Pedido de qualquer uma das partes, a qualquer tempo;
			D.	Automaticamente, ao termino do estagio;
			E.	Quando atingido o periodo maximo permitido pela Lei numero 11.788/08 para realizacao de estagio.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "CLAUSULA SETIMA - DO FORO - Fica eleito o Foro da Secao Judiciaria de Sao Paulo da Justica Federal da terceira Regiao com renuncia de qualquer outro por mais privilegiado que seja, para dirimir quaisquer duvidas que se originarem deste Termo de Compromisso e que nao possam ser solucionadas amigavelmente, e por estarem de acordo com os termos do presente instrumento, as partes o assinam em 03 (tres) vias, na presenca de duas testemunhas, para todos os fins e efeitos de direito.");

			$pdf->Ln(32);

			$pdf->MultiCell(190, 1, "");

			$pdf->Text(35, 45, "____________________, ___________  de  ________________  de  20_______");
			$pdf->Text(35, 50, "(cidade)");

			$pdf->Text(65, 85, "__________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(85, 90, "Unidade Concedente");
			$pdf->SetFont('Arial', '', 12);

			$pdf->Ln(79);
			$pdf->MultiCell(190, 6, $representante_legal);

			$pdf->Text(65, 135, "__________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(95, 140, "Estagiário");
			$pdf->SetFont('Arial', '', 12);

			$pdf->Ln(45);
			$pdf->MultiCell(190, 6, $_SESSION['NOME']);

			$pdf->Text(15, 175, "____________________________________________________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(40, 180, "Instituto Federal de Educação, Ciência e Tecnologia de São Paulo");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(70, 185, "Nome Completo, Carimbo e Assinatura");

			$pdf->Text(15, 220, "___________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(35, 225, "Testemunha");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(15, 232, "Nome");
			$pdf->Text(15, 239, "RG");
			$pdf->Text(15, 246, "CPF");

			$pdf->Text(120, 220, "___________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(140, 225, "Testemunha");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(120, 232, "Nome");
			$pdf->Text(120, 239, "RG");
			$pdf->Text(120, 246, "CPF");

			$pdf->output();

		} else {
			Application::redirect('?controle=Estagio&acao=administrarEstagio');

		}
	}

	public function gerarDocumentoAditivoAction() {

		session_start();

		if (!isset($_SESSION['AcessoAluno'])) {
    
			session_destroy();
			Application::redirect('?controle=Index&acao=index');
		
		}

		if(!empty($_POST['prorrogarMeses']) && !empty($_POST['idEstagio'])) {

			$estagio = new EstagioModel();
			$unidade_concedente = new UnidadeConcedenteModel();

			$id_estagio = $_POST['idEstagio'];
			$meses_aditivo = $_POST['prorrogarMeses'];
			$data_celebrada = date("d-m-Y");

			$estagio->setIDEstagio($id_estagio);
			$estagio->setMesesAditivo($meses_aditivo);
			$estagio->setDataCelebradaApartir($data_celebrada);

			$estagio->AdicionarDocumentoAditivo($estagio);

			$info_doc = $estagio->listarEstagioAluno($_SESSION['ID_ALUNO']);

			$representante_legal = $info_doc[0]->getRepLegal();
			$cargo_representante = $info_doc[0]->getCargoRep();
			$supervisor_estagio = $info_doc[0]->getSupEstagio();
			$formacao_academica_supervisor = $info_doc[0]->getFormAcaSup();
			$registro_profissional_supervisor = $info_doc[0]->getRegProfSup();
			$orgao_registro_supervisor = $info_doc[0]->getOrgaoRegSup();

			$info_unidade_concedente = $unidade_concedente->listarUnidadeConcedente($info_doc[0]->getIDUnidadeConcedenteEstagio());

			$razao = $info_unidade_concedente[0]->getRazaoSocial();
			$insc_estadual = $info_unidade_concedente[0]->getInscEstadual();
			$CNPJ = $info_unidade_concedente[0]->getCNPJ();
			$CPF = $info_unidade_concedente[0]->getCPF();
			$telefone = $info_unidade_concedente[0]->getTelefone();
			$estado = $info_unidade_concedente[0]->getEstado();
			$cidade = $info_unidade_concedente[0]->getCidade();
			$cep = $info_unidade_concedente[0]->getCEP();
			$endereco = $info_unidade_concedente[0]->getEndereco();
			$bairro = $info_unidade_concedente[0]->getBairro();		

			$pdf = new FPDF();
			
			$pdf->AddPage();
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Cell(65, 25, $pdf->Image('images/IFSP logo.JPG',10, 10), 1);

			$pdf->Cell(125, 25, "" , 1);
			$pdf->SetFontSize(10);
			$pdf->Text(80, 18, "Instituto Federal de Educação, Ciência e Tecnologia de São Paulo");
			$pdf->Text(100, 23, "Diretoria Geral do Campus Campinas SP");
			$pdf->Text(110, 28, "Coordenadoria de Extensão");

			$pdf->Ln(30);

			$pdf->Cell(190, 12, "" , 1);
			$pdf->SetFontSize(12);
			$pdf->Text(88, 45, "TERMO ADITIVO");
			$pdf->SetFontSize(11);
			$pdf->Text(73, 50, "(Técnico/Tecnológico/Bacharelado)");

			$pdf->Ln(15);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFontSize(16);
			$pdf->Text(70, 61, "INSTITUIÇÃO DE ENSINO");

			$pdf->Ln(10);

			$pdf->Cell(190, 11, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 70, "Instituição");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(32, 70, ": INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DE SÃO PAULO/IFSP");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(75, 74, "(doravante denominado IFSP)");

			$pdf->Ln(11);

			$pdf->Cell(190, 16, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 80, "Endereco");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(30, 80, ": à Rodovia D. Pedro I (SP65) km 143,6 - Terminal Intermodal de Cargas de Campinas (TICC)");
			$pdf->Text(12, 85, "Campinas, SP - Brasil, Centro de Tecnologia da Informação Renato Archer (CTI).");
			$pdf->Text(12, 90, "Campinas - SP - ");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(42, 90, "CEP 13069-901");

			$pdf->Ln(16);

			$pdf->Cell(190, 5, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 96, "Fone: (19) 3746-6128");
			$pdf->Text(122, 96, "CNPJ: 10.882.594/0029-66");

			$pdf->Ln(5);

			$pdf->Cell(190, 11, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 101, "Representada pelo");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(48, 101, "seu Diretor Geral, Prof. Eberval Oliveira Castro, nomeado pela Portaria nº 1.352");
			$pdf->Text(12, 106, "publicada no Diário Oficial da União de 11 de abril de 2017.");

			$pdf->Ln(15);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Text(70, 118, "UNIDADE CONCEDENTE");

			$pdf->Ln(10);

			$pdf->Cell(190, 10, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 126, "Razão Social:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(12, 131, $razao);
			$pdf->Text(155, 126, "(doravante denominada");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(155, 131, "CONCEDENTE)");

			$pdf->Ln(10);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 136, "CNPJ:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 136, $CNPJ);
			$pdf->Text(75, 136, "(empresa)");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 136, "Insc. Estadual:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(140, 136, $insc_estadual);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 142, "CPF:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 142, $CPF);
			$pdf->Text(75, 142, "(autônomo)");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 142, "Fone:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 142, $telefone);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 148, "Endereço:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(32, 148, $endereco);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 154, "CEP:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(23, 154, $cep);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 154, "Estado:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(127, 154, $estado);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 160, "Cidade:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(28, 160, $cidade);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 166, "Bairro:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 166, $bairro);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 172, "Representante Legal: ");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 172, $representante_legal);
			
			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 178, "Cargo:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 178, $cargo_representante);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 184, "Supervisor de Estágio:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(57, 184, $supervisor_estagio);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 190, "Formação Acadêmica:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 190, $formacao_academica_supervisor);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 196, "Registro Profissional:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 196, $registro_profissional_supervisor);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 196, "Orgão:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 196, $orgao_registro_supervisor);

			$pdf->Ln(10);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Text(85, 207, "ESTAGIÁRIO");

			$pdf->Ln(10);

			$pdf->Cell(190, 10, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 216, "Nome:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(12, 221, $_SESSION['NOME']);
			$pdf->Text(155, 216, "(doravante denominada");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(155, 221, "ESTAGIÁRIO)");

			$pdf->Ln(10);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 226, "Curso:");
			$pdf->SetFont('Arial', '', 11);

			$aluno_objeto = new AlunoModel();
			$nome_curso = "";
			$nome_curso = $aluno_objeto->buscarNomeCursoAluno($_SESSION['ID_CURSO']);

			$pdf->Text(25, 226, $nome_curso);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(150, 226, "Prontuário:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(172, 226, $_SESSION['RA']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 232, "Período:");
			$pdf->SetFont('Arial', '', 11);

			$periodo = "";
			if($_SESSION['PERIODO'] == 1) { 
				$periodo = "Matutino";
			} else if ($_SESSION['PERIODO'] == 2) { 
				$periodo = "Vespertino";
			} else if ($_SESSION['PERIODO'] == 3) {
				$periodo = "Noturno";
			} else {
				$periodo = "Integral";
			}

			$pdf->Text(30, 232, $periodo);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 232, "Data de Nascimento:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(152, 232, $_SESSION['DATA_NASC']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 239, "RG:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(22, 239, $_SESSION['RG']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 239, "CPF:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 239, $_SESSION['CPF']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 245, "CEP:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(23, 245, $_SESSION['CEP']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 245, "Estado:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(127, 245, $_SESSION['ESTADO']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 251, "Cidade:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(28, 251, $_SESSION['CIDADE']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 257, "Bairro:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 257, $_SESSION['BAIRRO']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 263, "Fone:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 263, $_SESSION['FONE']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 263, "Cel:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(122, 263, $_SESSION['CEL']);
			
			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 269, "Email:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 269, $_SESSION['EMAIL']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(132, 269, "Portador de Deficiência:");
			$pdf->SetFont('Arial', '', 11);

			$portador_def = "";
			if($_SESSION['PORTADOR_DEF'] == 1) { 
				$portador_def = "Não";
			} else { 
				$portador_def = "Sim";
			}

			$pdf->Text(180, 269, $portador_def);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 275, "Estágio Obrigatório:");
			$pdf->SetFont('Arial', '', 11);

			$estag_obrig = "";
			if($_SESSION['ESTAG_OBRIG'] == 1) { 
				$estag_obrig = "Não";
			} else { 
				$estag_obrig = "Sim";
			}

			$pdf->Text(52, 275, $estag_obrig);

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "As partes supracitadas celebram o presente TERMO ADITIVO ao Termo de Compromisso de Estagio, tudo nos termos da Lei n 11.788/08 e demais legislacao vigente que dispoe sobre estagio.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "CLAUSULA PRIMEIRA: Por este instrumento de Aditamento ao Termo de Compromisso de Estagio, celebrado nesta data de $data_celebrada , fica aditado para constar que: ");

			$pdf->Ln(3);

			$pdf->SetFont('Arial', 'BU', 11);
			$pdf->Cell(190, 6, "É requesitado pelo aluno supracitado a prorrogação de seu estágio por $meses_aditivo meses.");

			$pdf->Ln(9);

			$pdf->SetFont('Arial', '', 11);
			$pdf->MultiCell(190, 6, "CLAUSULA SEGUNDA: E, por estarem inteiramente de acordo, com as clausulas estipuladas, as partes assinam este Termo Aditivo, em 03 (tres) vias de igual teor para todos os fins e efeitos de direito, juntamente com as testemunhas.");

			$pdf->Text(35, 90, "____________________, ___________  de  ________________  de  20_______");
			$pdf->Text(35, 95, "(cidade)");

			$pdf->Text(65, 125, "__________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(85, 130, "Unidade Concedente");
			$pdf->SetFont('Arial', '', 12);

			$pdf->Ln(60);
			$pdf->MultiCell(190, 6, $representante_legal);

			$pdf->Text(65, 165, "__________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(95, 170, "Estagiário");
			$pdf->SetFont('Arial', '', 12);

			$pdf->Ln(35);
			$pdf->MultiCell(190, 6, $_SESSION['NOME']);

			$pdf->Text(15, 210, "____________________________________________________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(40, 215, "Instituto Federal de Educação, Ciência e Tecnologia de São Paulo");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(70, 220, "Nome Completo, Carimbo e Assinatura");

			$pdf->Text(15, 255, "___________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(35, 260, "Testemunha");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(15, 267, "Nome");
			$pdf->Text(15, 274, "RG");
			$pdf->Text(15, 281, "CPF");

			$pdf->Text(120, 255, "___________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(140, 260, "Testemunha");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(120, 267, "Nome");
			$pdf->Text(120, 274, "RG");
			$pdf->Text(120, 281, "CPF");

			$pdf->output();

		} else {
			Application::redirect('?controle=Estagio&acao=administrarEstagio');

		}
	}

	public function gerarDocumentoRescisaoAction() {

		session_start();

		if (!isset($_SESSION['AcessoAluno'])) {
    
			session_destroy();
			Application::redirect('?controle=Index&acao=index');
		
		}

		if(!empty($_POST['notificadaPor']) && !empty($_POST['idEstagio'])) {

			$estagio = new EstagioModel();
			$unidade_concedente = new UnidadeConcedenteModel();

			$id_estagio = $_POST['idEstagio'];
			$notificada_por = $_POST['notificadaPor'];
			$data_apartir = date("d-m-Y");

			$estagio->setIDEstagio($id_estagio);
			$estagio->setNotificadaPor($notificada_por);
			$estagio->setDataCelebradaApartir($data_apartir);

			$estagio->AdicionarDocumentoRescisao($estagio);

			$info_doc = $estagio->listarEstagioAluno($_SESSION['ID_ALUNO']);

			$representante_legal = $info_doc[0]->getRepLegal();
			$cargo_representante = $info_doc[0]->getCargoRep();
			$supervisor_estagio = $info_doc[0]->getSupEstagio();
			$formacao_academica_supervisor = $info_doc[0]->getFormAcaSup();
			$registro_profissional_supervisor = $info_doc[0]->getRegProfSup();
			$orgao_registro_supervisor = $info_doc[0]->getOrgaoRegSup();
			$data_inicio = $info_doc[0]->getDataInicio();
			$data_fim = $info_doc[0]->getDataFim();
			$data_estagio_inicio = date("d-m-Y", strtotime($data_inicio));
			$data_estagio_fim = date("d-m-Y", strtotime($data_fim));

			$info_unidade_concedente = $unidade_concedente->listarUnidadeConcedente($info_doc[0]->getIDUnidadeConcedenteEstagio());

			$razao = $info_unidade_concedente[0]->getRazaoSocial();
			$insc_estadual = $info_unidade_concedente[0]->getInscEstadual();
			$CNPJ = $info_unidade_concedente[0]->getCNPJ();
			$CPF = $info_unidade_concedente[0]->getCPF();
			$telefone = $info_unidade_concedente[0]->getTelefone();
			$estado = $info_unidade_concedente[0]->getEstado();
			$cidade = $info_unidade_concedente[0]->getCidade();
			$cep = $info_unidade_concedente[0]->getCEP();
			$endereco = $info_unidade_concedente[0]->getEndereco();
			$bairro = $info_unidade_concedente[0]->getBairro();

			if ($notificada_por == 1) { 
				$notificada_por_concedente = "X";
			} else {
				$notificada_por_concedente = "";
			}

			if ($notificada_por == 2) { 
				$notificada_por_estagiario = "X";
			} else {
				$notificada_por_estagiario = "";
			}

			if ($notificada_por == 3) { 
				$notificada_por_ifsp = "X";
			} else {
				$notificada_por_ifsp = "";
			}

			$pdf = new FPDF();
		
			$pdf->AddPage();
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Cell(65, 25, $pdf->Image('images/IFSP logo.JPG',10, 10), 1);

			$pdf->Cell(125, 25, "" , 1);
			$pdf->SetFontSize(10);
			$pdf->Text(80, 18, "Instituto Federal de Educação, Ciência e Tecnologia de São Paulo");
			$pdf->Text(100, 23, "Diretoria Geral do Campus Campinas SP");
			$pdf->Text(110, 28, "Coordenadoria de Extensão");

			$pdf->Ln(30);

			$pdf->Cell(190, 12, "" , 1);
			$pdf->SetFontSize(12);
			$pdf->Text(38, 45, "TERMO DE RESCISÃO DO TERMO DE COMPROMISSO DE ESTÁGIO");
			$pdf->SetFontSize(11);
			$pdf->Text(73, 50, "(Técnico/Tecnológico/Bacharelado)");

			$pdf->Ln(15);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFontSize(16);
			$pdf->Text(70, 61, "INSTITUIÇÃO DE ENSINO");

			$pdf->Ln(10);

			$pdf->Cell(190, 11, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 70, "Instituição");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(32, 70, ": INSTITUTO FEDERAL DE EDUCAÇÃO, CIÊNCIA E TECNOLOGIA DE SÃO PAULO/IFSP");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(75, 74, "(doravante denominado IFSP)");

			$pdf->Ln(11);

			$pdf->Cell(190, 16, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 80, "Endereco");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(30, 80, ": à Rodovia D. Pedro I (SP65) km 143,6 - Terminal Intermodal de Cargas de Campinas (TICC)");
			$pdf->Text(12, 85, "Campinas, SP - Brasil, Centro de Tecnologia da Informação Renato Archer (CTI).");
			$pdf->Text(12, 90, "Campinas - SP - ");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(42, 90, "CEP 13069-901");

			$pdf->Ln(16);

			$pdf->Cell(190, 5, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 96, "Fone: (19) 3746-6128");
			$pdf->Text(122, 96, "CNPJ: 10.882.594/0029-66");

			$pdf->Ln(5);

			$pdf->Cell(190, 11, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 101, "Representada pelo");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(48, 101, "seu Diretor Geral, Prof. Eberval Oliveira Castro, nomeado pela Portaria nº 1.352");
			$pdf->Text(12, 106, "publicada no Diário Oficial da União de 11 de abril de 2017.");

			$pdf->Ln(15);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Text(70, 118, "UNIDADE CONCEDENTE");

			$pdf->Ln(10);

			$pdf->Cell(190, 10, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 126, "Razão Social:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(12, 131, $razao);
			$pdf->Text(155, 126, "(doravante denominada");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(155, 131, "CONCEDENTE)");

			$pdf->Ln(10);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 136, "CNPJ:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 136, $CNPJ);
			$pdf->Text(75, 136, "(empresa)");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 136, "Insc. Estadual:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(140, 136, $insc_estadual);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 142, "CPF:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 142, $CPF);
			$pdf->Text(75, 142, "(autônomo)");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 142, "Fone:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 142, $telefone);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 148, "Endereço:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(32, 148, $endereco);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 154, "CEP:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(23, 154, $cep);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 154, "Estado:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(127, 154, $estado);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 160, "Cidade:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(28, 160, $cidade);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 166, "Bairro:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 166, $bairro);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 172, "Representante Legal: ");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 172, $representante_legal);
			
			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 178, "Cargo:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 178, $cargo_representante);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 184, "Supervisor de Estágio:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(57, 184, $supervisor_estagio);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 190, "Formação Acadêmica:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 190, $formacao_academica_supervisor);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 196, "Registro Profissional:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(55, 196, $registro_profissional_supervisor);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 196, "Orgão:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 196, $orgao_registro_supervisor);

			$pdf->Ln(10);

			$pdf->Cell(190, 7, "" , 1);
			$pdf->SetFont('Arial', 'B', 16);
			$pdf->Text(85, 207, "ESTAGIÁRIO");

			$pdf->Ln(10);

			$pdf->Cell(190, 10, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 216, "Nome:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(12, 221, $_SESSION['NOME']);
			$pdf->Text(155, 216, "(doravante denominada");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(155, 221, "ESTAGIÁRIO)");

			$pdf->Ln(10);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 226, "Curso:");
			$pdf->SetFont('Arial', '', 11);

			$aluno_objeto = new AlunoModel();
			$nome_curso = "";
			$nome_curso = $aluno_objeto->buscarNomeCursoAluno($_SESSION['ID_CURSO']);

			$pdf->Text(25, 226, $nome_curso);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(150, 226, "Prontuário:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(172, 226, $_SESSION['RA']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 232, "Período:");
			$pdf->SetFont('Arial', '', 11);

			$periodo = "";
			if($_SESSION['PERIODO'] == 1) { 
				$periodo = "Matutino";
			} else if ($_SESSION['PERIODO'] == 2) { 
				$periodo = "Vespertino";
			} else if ($_SESSION['PERIODO'] == 3) {
				$periodo = "Noturno";
			} else {
				$periodo = "Integral";
			}

			$pdf->Text(30, 232, $periodo);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 232, "Data de Nascimento:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(152, 232, $_SESSION['DATA_NASC']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 239, "RG:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(22, 239, $_SESSION['RG']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 239, "CPF:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(125, 239, $_SESSION['CPF']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 245, "CEP:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(23, 245, $_SESSION['CEP']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 245, "Estado:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(127, 245, $_SESSION['ESTADO']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 251, "Cidade:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(28, 251, $_SESSION['CIDADE']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 257, "Bairro:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 257, $_SESSION['BAIRRO']);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 263, "Fone:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(25, 263, $_SESSION['FONE']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(112, 263, "Cel:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(122, 263, $_SESSION['CEL']);
			
			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 269, "Email:");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(27, 269, $_SESSION['EMAIL']);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(132, 269, "Portador de Deficiência:");
			$pdf->SetFont('Arial', '', 11);

			$portador_def = "";
			if($_SESSION['PORTADOR_DEF'] == 1) { 
				$portador_def = "Não";
			} else { 
				$portador_def = "Sim";
			}

			$pdf->Text(180, 269, $portador_def);

			$pdf->Ln(6);

			$pdf->Cell(190, 6, "" , 1);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(12, 275, "Estágio Obrigatório:");
			$pdf->SetFont('Arial', '', 11);

			$estag_obrig = "";
			if($_SESSION['ESTAG_OBRIG'] == 1) { 
				$estag_obrig = "Não";
			} else { 
				$estag_obrig = "Sim";
			}

			$pdf->Text(52, 275, $estag_obrig);

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "De acordo com a informacao notificada pelo ( $notificada_por_concedente ) Unidade Concedente, ( $notificada_por_estagiario ) Estagiario ou  ( $notificada_por_ifsp ) Instituto Federal de Educacao, Ciencia e Tecnologia de Sao Paulo, a partir de $data_apartir , encerrar  o Termo de Compromisso de Estagio ou ultimo Termo Aditivo, firmado entre as partes supra, para o periodo compreendido entre $data_estagio_inicio , e $data_estagio_fim , nos termos do que dispoem a Lei 11.788/08 e o Regulamento de Estagios do IFSP.");

			$pdf->Ln(6);

			$pdf->MultiCell(190, 6, "Fica eleito o Foro da Secao Judiciaria de Sao Paulo da Justica Federal da Terceira Regiao com renuncia de qualquer outro por mais privilegiado que seja para dirimir quaisquer duvidas que se originarem desta Rescisao. E por estarem de acordo com a condicao estabelecida, as partes o assinam em 03 (tres) vias, na presenca de 02 (duas) testemunhas, para todos os fins e efeitos de direito.");


			$pdf->Text(35, 85, "____________________, ___________  de  ________________  de  20_______");
			$pdf->Text(35, 90, "(cidade)");

			$pdf->Text(65, 125, "__________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(85, 130, "Unidade Concedente");
			$pdf->SetFont('Arial', '', 12);

			$pdf->Ln(60);
			$pdf->MultiCell(190, 6, $representante_legal);

			$pdf->Text(65, 165, "__________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(95, 170, "Estagiário");
			$pdf->SetFont('Arial', '', 12);

			$pdf->Ln(35);
			$pdf->MultiCell(190, 6, $_SESSION['NOME']);

			$pdf->Text(15, 210, "____________________________________________________________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(40, 215, "Instituto Federal de Educação, Ciência e Tecnologia de São Paulo");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(70, 220, "Nome Completo, Carimbo e Assinatura");

			$pdf->Text(15, 255, "___________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(35, 260, "Testemunha");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(15, 267, "Nome");
			$pdf->Text(15, 274, "RG");
			$pdf->Text(15, 281, "CPF");

			$pdf->Text(120, 255, "___________________________");
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Text(140, 260, "Testemunha");
			$pdf->SetFont('Arial', '', 11);
			$pdf->Text(120, 267, "Nome");
			$pdf->Text(120, 274, "RG");
			$pdf->Text(120, 281, "CPF");

			$pdf->output();

		} else {
			Application::redirect('?controle=Estagio&acao=administrarEstagio');

		}
	}

}
?>