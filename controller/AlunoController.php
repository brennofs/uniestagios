<?php
require_once 'model/AlunoModel.php';
require_once 'model/CursoModel.php';

class AlunoController {

	public function alunoAction() {
		//redirecionando para a pagina inicial
		$o_view = new View('view/painel-aluno.php');
		$o_view->showContents();
	}

	public function cadastrarAlunoAction() {
		$curso = new CursoModel();
		
		$cursos = $curso->listarCursos();

		$o_view = new View('view/cadastro-aluno.php');

		$o_view->setParams(array('cursos' => $cursos));

		$o_view->showContents();
	}

	public function loginAlunoAction() {
		$o_view = new View('view/login-aluno.php');
		$o_view->showContents();
	}

	public function visualizarAlunoAction() {
		$aluno = new AlunoModel();
		
		$alunos = $aluno->listarAlunos();
		
		$o_view = new View('view/visualiza-aluno.php');
		
		$o_view->setParams(array('alunos' => $alunos));
		
		$o_view->showContents();
	}

	public function removerAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){
			
			$aluno = new AlunoModel();
			
			$aluno->removerAluno($_GET['q']);
			
		}else{
			echo "ERRO: carregarRemoverAction ";
			exit;
		}

		Application::redirect('?controle=Aluno&acao=visualizarAluno');
	}

	public function visualizarAlterarAction() {
		
		if (DataValidator::isNumeric($_GET['q']) == 1){

			$curso = new CursoModel();
		
			$cursos = $curso->listarCursos();
			
			$objeto_aluno = new AlunoModel();
			
			$aluno = $objeto_aluno->visualizarAlterarAluno($_GET['q']);

			$o_view = new View('view/alterar-aluno.php');
		
			$o_view->setParams(array('aluno' => $aluno, 'cursos' => $cursos));
		
			$o_view->showContents();
			
		} else{
			echo "ERRO: carregarVisualizarAlterarAction ";
			exit;
		}

	}

	public function alterarAction() {
		$aluno = new AlunoModel();

		if(!empty($_POST['IDAluno']) && !empty($_POST['alunoRA']) && !empty($_POST['alunoNome']) && !empty($_POST['alunoDataNasc'])
		&& !empty($_POST['alunoRG']) && !empty($_POST['alunoCPF']) && !empty($_POST['alunoEstado']) && !empty($_POST['alunoCidade']) 
		&& !empty($_POST['alunoCEP']) && !empty($_POST['alunoBairro']) && !empty($_POST['alunoEmail']) && !empty($_POST['alunoPortadorDef'])
		&& !empty($_POST['alunoEstagObrig']) && !empty($_POST['alunoSemestre']) && !empty($_POST['alunoPeriodo']) && !empty($_POST['alunoSitMatricula']) && !empty($_POST['alunoCurso'])) {

			$ID_ALUNO = $_POST['IDAluno'];
			$RA = $_POST['alunoRA'];
			$NOME = $_POST['alunoNome'];
			$DATA_NASC = $_POST['alunoDataNasc'];
			$RG = $_POST['alunoRG'];
			$CPF = $_POST['alunoCPF'];
			$ESTADO = $_POST['alunoEstado'];
			$CIDADE = $_POST['alunoCidade'];
			$CEP = $_POST['alunoCEP'];
			$BAIRRO = $_POST['alunoBairro'];
			$EMAIL = $_POST['alunoEmail'];
			$PORTADOR_DEF = $_POST['alunoPortadorDef'];
			$ESTAG_OBRIG = $_POST['alunoEstagObrig'];
			$SEMESTRE = $_POST['alunoSemestre'];
			$PERIODO = $_POST['alunoPeriodo'];
			$SIT_MATRICULA = $_POST['alunoSitMatricula'];
			$ID_CURSO = $_POST['alunoCurso'];

			if (!empty($_POST['alunoTelefone'])) { 
				$TELEFONE = $_POST['alunoTelefone'];
			} else { 
				$TELEFONE = "Não Informado";
			};

			if (!empty($_POST['alunoCelular'])) { 
				$CELULAR = $_POST['alunoCelular'];
			} else { 
				$CELULAR = "Não Informado";
			};

			$aluno->setIDAluno($ID_ALUNO);
			$aluno->setRAAluno($RA);
			$aluno->setNomeAluno($NOME);
			$aluno->setDataNascAluno($DATA_NASC);
			$aluno->setRGAluno($RG);
			$aluno->setCPFAluno($CPF);
			$aluno->setEstadoAluno($ESTADO);
			$aluno->setCidadeAluno($CIDADE);
			$aluno->setCEPAluno($CEP);
			$aluno->setBairroAluno($BAIRRO);
			$aluno->setFoneAluno($TELEFONE);
			$aluno->setCelularAluno($CELULAR);
			$aluno->setEmailAluno($EMAIL);
			$aluno->setPortadorDefAluno($PORTADOR_DEF);
			$aluno->setEstagObrigAluno($ESTAG_OBRIG);
			$aluno->setSemestreAluno($SEMESTRE);
			$aluno->setPeriodoAluno($PERIODO);
			$aluno->setSitMatriculaAluno($SIT_MATRICULA);
			$aluno->setIDCursoAluno($ID_CURSO);

			$aluno->alterarAluno($aluno);
		
		} else {
			Application::redirect('?controle=Aluno&acao=visualizarAluno');

		} 
		
		Application::redirect('?controle=Aluno&acao=visualizarAluno');

	}

	public function cadastroAlunoAction() {

		session_start();

		$aluno = new AlunoModel();

		if(!empty($_POST['alunoRA']) && !empty($_POST['alunoSenha']) && !empty($_POST['alunoNome']) && !empty($_POST['alunoDataNasc'])
		&& !empty($_POST['alunoRG']) && !empty($_POST['alunoCPF']) && !empty($_POST['alunoEstado']) && !empty($_POST['alunoCidade']) 
		&& !empty($_POST['alunoCEP']) && !empty($_POST['alunoBairro']) && !empty($_POST['alunoEmail']) && !empty($_POST['alunoPortadorDef'])
		&& !empty($_POST['alunoEstagObrig']) && !empty($_POST['alunoSemestre']) && !empty($_POST['alunoPeriodo']) && !empty($_POST['alunoCurso'])) {

			$RA = $_POST['alunoRA'];
			$SENHA = $_POST['alunoSenha'];
			$NOME = $_POST['alunoNome'];
			$DATA_NASC = $_POST['alunoDataNasc'];
			$RG = $_POST['alunoRG'];
			$CPF = $_POST['alunoCPF'];
			$ESTADO = $_POST['alunoEstado'];
			$CIDADE = $_POST['alunoCidade'];
			$CEP = $_POST['alunoCEP'];
			$BAIRRO = $_POST['alunoBairro'];
			$EMAIL = $_POST['alunoEmail'];
			$PORTADOR_DEF = $_POST['alunoPortadorDef'];
			$ESTAG_OBRIG = $_POST['alunoEstagObrig'];
			$SEMESTRE = $_POST['alunoSemestre'];
			$PERIODO = $_POST['alunoPeriodo'];
			$SIT_MATRICULA = 1;
			$SIT_ESTAGIO = 1;

			if (!empty($_POST['alunoTelefone'])) { 
				$TELEFONE = $_POST['alunoTelefone'];
			} else { 
				$TELEFONE = "Não Informado";
			};

			if (!empty($_POST['alunoCelular'])) { 
				$CELULAR = $_POST['alunoCelular'];
			} else { 
				$CELULAR = "Não Informado";
			};

			$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];

			$ID_CURSO = $_POST['alunoCurso'];

			$aluno->setRAAluno($RA);
			$aluno->setSenhaAluno($SENHA);
			$aluno->setNomeAluno($NOME);
			$aluno->setDataNascAluno($DATA_NASC);
			$aluno->setRGAluno($RG);
			$aluno->setCPFAluno($CPF);
			$aluno->setEstadoAluno($ESTADO);
			$aluno->setCidadeAluno($CIDADE);
			$aluno->setCEPAluno($CEP);
			$aluno->setBairroAluno($BAIRRO);
			$aluno->setFoneAluno($TELEFONE);
			$aluno->setCelularAluno($CELULAR);
			$aluno->setEmailAluno($EMAIL);
			$aluno->setPortadorDefAluno($PORTADOR_DEF);
			$aluno->setEstagObrigAluno($ESTAG_OBRIG);
			$aluno->setSemestreAluno($SEMESTRE);
			$aluno->setPeriodoAluno($PERIODO);
			$aluno->setSitMatriculaAluno($SIT_MATRICULA);
			$aluno->setSitEstagioAluno($SIT_ESTAGIO);
			$aluno->setIDInstAluno($ID_INSTITUICAO);
			$aluno->setIDCursoAluno($ID_CURSO);

			$aluno->cadastrarAluno($aluno);
		
		} else {
			Application::redirect('?controle=Instituicao&acao=instituicao');

		} 
		
		Application::redirect('?controle=Instituicao&acao=instituicao');

	}
}
?>