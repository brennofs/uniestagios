<?php

class AdminController {
	
	public function adminAction() {
		//redirecionando para a pagina inicial
		$o_view = new View('view/painel-admin.php');
		$o_view->showContents();
	}

	public function loginAdminAction() {
		$o_view = new View('view/login-admin.php');
		$o_view->showContents();
	}
}
?>