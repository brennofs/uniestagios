<?php
require_once 'model/OfertaVagaModel.php';

class VagaDAO {
    
    public static $instance;
    
    public function __construct() {
    }
    
    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new VagaDAO();
            
            return self::$instance;
    }

    public function listarVagasDAO() {

        session_start();

        $sql = new Sql();

		$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];
            
        try {

            $result = $sql->result("SELECT *
			FROM vaga
            WHERE VAGA.ID_INSTITUICAO = :ID_INSTITUICAO
            ORDER BY TIPO_VAGA", array(
                ":ID_INSTITUICAO"=>$ID_INSTITUICAO));

            $vagas = array();

            foreach($result AS $vaga) {
                array_push($vagas, $this->populaVaga($vaga));
            }
           
            return  $vagas;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function listarVagasAlunoDAO() {

        session_start();

        $sql = new Sql();

        $oferta_vaga = new OfertaVagaModel();

        $ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];
        $ID_CURSO = $_SESSION['ID_CURSO'];

        try {

            $oferta_vaga_objeto = $oferta_vaga->buscarOfertaVagasAluno($ID_INSTITUICAO, $ID_CURSO);

            $vagas = array();

            foreach($oferta_vaga_objeto AS $oferta_vaga) {

                $result = $sql->result("SELECT *
                FROM vaga
                WHERE VAGA.ID_VAGA = :ID_VAGA AND VAGA.SIT_VAGA = 1", array(
                    ":ID_VAGA"=>$oferta_vaga->getIDVaga()));

                foreach($result AS $vaga) {
                    array_push($vagas, $this->populaVaga($vaga));
                }

            }
           
            return $vagas;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }
    
    private function populaVaga($vaga) {

        $objeto_vaga = new VagaModel();

        $objeto_vaga->setIDVaga($vaga['ID_VAGA']);
        $objeto_vaga->setNomeVaga($vaga['NOME']);
        $objeto_vaga->setEmpresaVaga($vaga['EMPRESA']);
        $objeto_vaga->setDescricaoVaga($vaga['DESCRICAO']);
        $objeto_vaga->setSalarioVaga($vaga['SALARIO']);
        $objeto_vaga->setHorEntradaVaga($vaga['HOR_ENTRADA']);
        $objeto_vaga->setHorSaidaVaga($vaga['HOR_SAIDA']);
        $objeto_vaga->setBeneficiosVaga($vaga['BENEFICIOS']);
        $objeto_vaga->setSituacaoVaga($vaga['SIT_VAGA']);
        $objeto_vaga->setTipoVaga($vaga['TIPO_VAGA']);
        $objeto_vaga->setIDInstituicaoVaga($vaga['ID_INSTITUICAO']);

        return $objeto_vaga;
    }
    
    public function visualizarAlterarVagaDAO($ID_VAGA) {

        session_start();

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM VAGA
            WHERE VAGA.ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$ID_VAGA));

            $info_vaga = array();

            foreach($result AS $vaga) {
                array_push($info_vaga, $this->populaVaga($vaga));
            }

            if ($info_vaga[0]->getIDInstituicaoVaga() === $_SESSION['ID_INSTITUICAO']) {
                return $info_vaga;
            } else {
                Application::redirect('?controle=Vaga&acao=visualizarVaga');
            }

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }
    
	public function cadastrarVagaDAO($vaga) {

        $sql = new Sql();
   
		try {

            $sql->result("INSERT INTO vaga (NOME, EMPRESA, DESCRICAO, SALARIO, HOR_ENTRADA, HOR_SAIDA, BENEFICIOS, SIT_VAGA, TIPO_VAGA, ID_INSTITUICAO) VALUES (:NOME, :EMPRESA, :DESCRICAO, :SALARIO, :HOR_ENTRADA, :HOR_SAIDA, :BENEFICIOS, :SIT_VAGA, :TIPO_VAGA, :ID_INSTITUICAO)", array(
                ":NOME"=>$vaga->getNomeVaga(),
                ":EMPRESA"=>$vaga->getEmpresaVaga(),
                ":DESCRICAO"=>$vaga->getDescricaoVaga(),
                ":SALARIO"=>$vaga->getSalarioVaga(),
                ":HOR_ENTRADA"=>$vaga->getHorEntradaVaga(),
                ":HOR_SAIDA"=>$vaga->getHorSaidaVaga(),
                ":BENEFICIOS"=>$vaga->getBeneficiosVaga(),
                ":SIT_VAGA"=>$vaga->getSituacaoVaga(),
                ":TIPO_VAGA"=>$vaga->getTipoVaga(),
                ":ID_INSTITUICAO"=>$vaga->getIDInstituicaoVaga()));

            $oferta_vaga = new OfertaVagaModel;

            $ofertas_cursos = $vaga->getOfertaVaga();
    
            foreach ($ofertas_cursos as $oferta_curso){
    
                $oferta_vaga->setIDVaga($sql->getLastInsertedID());
                $oferta_vaga->setIDCurso($oferta_curso);
                $oferta_vaga->setIDInstituicaoOfertaVaga($vaga->getIDInstituicaoVaga());
    
                $oferta_vaga->cadastrarOfertaVaga($oferta_vaga);    
            }
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
	}

    public function removerVagaDAO($ID_VAGA) {

        $sql = new Sql();

        try {

            $sql->result("DELETE FROM oferta_vaga WHERE ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$ID_VAGA));

            $sql->result("DELETE FROM vaga WHERE ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$ID_VAGA));

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function alterarVagaDAO($vaga) {
        
        $sql = new Sql();
   
		try {

            $sql->result("UPDATE vaga SET NOME = :NOME, EMPRESA = :EMPRESA, DESCRICAO = :DESCRICAO, SALARIO = :SALARIO, HOR_ENTRADA = :HOR_ENTRADA, HOR_SAIDA = :HOR_SAIDA, BENEFICIOS = :BENEFICIOS, TIPO_VAGA = :TIPO_VAGA WHERE ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$vaga->getIDVaga(),
                ":NOME"=>$vaga->getNomeVaga(),
                ":EMPRESA"=>$vaga->getEmpresaVaga(),
                ":DESCRICAO"=>$vaga->getDescricaoVaga(),
                ":SALARIO"=>$vaga->getSalarioVaga(),
                ":HOR_ENTRADA"=>$vaga->getHorEntradaVaga(),
                ":HOR_SAIDA"=>$vaga->getHorSaidaVaga(),
                ":BENEFICIOS"=>$vaga->getBeneficiosVaga(),
                ":TIPO_VAGA"=>$vaga->getTipoVaga()));
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }
	
}
?>