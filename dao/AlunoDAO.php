<?php

class AlunoDAO {
    
    public static $instance;
    
    public function __construct() {
    }
    
    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new AlunoDAO();
            
            return self::$instance;
    }

    public function listarAlunosDAO() {

        session_start();

        $sql = new Sql();

		$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];
            
        try {

            $result = $sql->result("SELECT *
			FROM aluno
            WHERE ALUNO.ID_INSTITUICAO = :ID_INSTITUICAO", array(
                ":ID_INSTITUICAO"=>$ID_INSTITUICAO));

            $alunos = array();

            foreach($result AS $aluno) {
                array_push($alunos, $this->populaAluno($aluno));
            }
           
            return  $alunos;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }
    
    private function populaAluno($aluno) {

        $objeto_aluno = new AlunoModel();

        $objeto_aluno->setIDAluno($aluno['ID_ALUNO']);
        $objeto_aluno->setRAAluno($aluno['RA']);
        $objeto_aluno->setSenhaAluno($aluno['SENHA']);
        $objeto_aluno->setNomeAluno($aluno['NOME']);
        $objeto_aluno->setDataNascAluno($aluno['DATA_NASC']);
		$objeto_aluno->setRGAluno($aluno['RG']);
		$objeto_aluno->setCPFAluno($aluno['CPF']);
		$objeto_aluno->setEstadoAluno($aluno['ESTADO']);
		$objeto_aluno->setCidadeAluno($aluno['CIDADE']);
		$objeto_aluno->setCEPAluno($aluno['CEP']);
		$objeto_aluno->setBairroAluno($aluno['BAIRRO']);
		$objeto_aluno->setFoneAluno($aluno['FONE']);
		$objeto_aluno->setCelularAluno($aluno['CEL']);
		$objeto_aluno->setEmailAluno($aluno['EMAIL']);
		$objeto_aluno->setPortadorDefAluno($aluno['PORTADOR_DEF']);
		$objeto_aluno->setEstagObrigAluno($aluno['ESTAG_OBRIG']);
        $objeto_aluno->setSemestreAluno($aluno['SEMESTRE']);
        $objeto_aluno->setPeriodoAluno($aluno['PERIODO']);
        $objeto_aluno->setSitMatriculaAluno($aluno['SIT_MAT']);
        $objeto_aluno->setSitEstagioAluno($aluno['SIT_ESTAG']);
        $objeto_aluno->setIDInstAluno($aluno['ID_INSTITUICAO']);
        $objeto_aluno->setIDCursoAluno($aluno['ID_CURSO']);
        $objeto_aluno->setNomeCursoAluno($objeto_aluno->buscarNomeCursoAluno($objeto_aluno->getIDCursoAluno()));

        return $objeto_aluno;
    }

    public function visualizarAlterarAlunoDAO($ID_ALUNO) {

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM aluno
            WHERE ALUNO.ID_ALUNO = :ID_ALUNO", array(
                ":ID_ALUNO"=>$ID_ALUNO));

            $info_aluno = array();

            foreach($result AS $aluno) {
                array_push($info_aluno, $this->populaAluno($aluno));
            }

            if ($info_aluno[0]->getIDInstAluno() === $_SESSION['ID_INSTITUICAO']) {
                return $info_aluno;
            } else {
                Application::redirect('?controle=Aluno&acao=visualizarAluno');
            }

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function alterarAlunoDAO($aluno) {
        
        $sql = new Sql();
   
		try {

            $sql->result("UPDATE aluno SET RA = :RA, NOME = :NOME, DATA_NASC = :DATA_NASC, RG = :RG, CPF = :CPF
            , ESTADO = :ESTADO, CIDADE = :CIDADE, CEP = :CEP, BAIRRO = :BAIRRO, FONE = :FONE, CEL = :CEL, EMAIL = :EMAIL
            , PORTADOR_DEF = :PORTADOR_DEF, ESTAG_OBRIG = :ESTAG_OBRIG, SEMESTRE = :SEMESTRE, PERIODO = :PERIODO
            , SIT_MAT = :SIT_MAT, ID_CURSO = :ID_CURSO WHERE ID_ALUNO = :ID_ALUNO", array(
                ":ID_ALUNO"=>$aluno->getIDAluno(),
                ":RA"=>$aluno->getRAAluno(),
                ":NOME"=>$aluno->getNomeAluno(),
                ":DATA_NASC"=>$aluno->getDataNascAluno(),
                ":RG"=>$aluno->getRGAluno(),
                ":CPF"=>$aluno->getCPFAluno(),
                ":ESTADO"=>$aluno->getEstadoAluno(),
                ":CIDADE"=>$aluno->getCidadeAluno(),
                ":CEP"=>$aluno->getCEPAluno(),
                ":BAIRRO"=>$aluno->getBairroAluno(),
                ":FONE"=>$aluno->getFoneAluno(),
                ":CEL"=>$aluno->getCelularAluno(),
                ":EMAIL"=>$aluno->getEmailAluno(),
                ":PORTADOR_DEF"=>$aluno->getPortadorDefAluno(),
                ":ESTAG_OBRIG"=>$aluno->getEstagObrigAluno(),
                ":SEMESTRE"=>$aluno->getSemestreAluno(),
                ":PERIODO"=>$aluno->getPeriodoAluno(),
                ":SIT_MAT"=>$aluno->getSitMatriculaAluno(),
                ":ID_CURSO"=>$aluno->getIDCursoAluno()));           
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function mudarSitEstagioEstagiandoAlunoDAO($ID_ALUNO) {
        
        $sql = new Sql();
   
		try {

            $sql->result("UPDATE aluno SET SIT_ESTAG = 2 WHERE ID_ALUNO = :ID_ALUNO", array(
                ":ID_ALUNO"=>$ID_ALUNO));
            
            $_SESSION['SIT_ESTAG'] = 2;
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function mudarSitEstagioSemEstagioAlunoDAO($ID_ALUNO) {
        
        $sql = new Sql();
   
		try {

            $sql->result("UPDATE aluno SET SIT_ESTAG = 1 WHERE ID_ALUNO = :ID_ALUNO", array(
                ":ID_ALUNO"=>$ID_ALUNO));
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }
    
	public function cadastrarAlunoDAO($aluno) {

        $sql = new Sql();
   
		try {

            $sql->result("INSERT INTO aluno (RA, SENHA, NOME, DATA_NASC, RG, CPF
            , ESTADO, CIDADE, CEP, BAIRRO, FONE, CEL, EMAIL, PORTADOR_DEF, ESTAG_OBRIG,
            SEMESTRE, PERIODO, SIT_MAT, SIT_ESTAG, ID_INSTITUICAO, ID_CURSO) 
            VALUES (:RA, :SENHA, :NOME, :DATA_NASC, :RG, :CPF, :ESTADO, :CIDADE, :CEP, :BAIRRO, :FONE,
             :CEL, :EMAIL, :PORTADOR_DEF, :ESTAG_OBRIG, :SEMESTRE, :PERIODO, :SIT_MAT, :SIT_ESTAG, :ID_INSTITUICAO, :ID_CURSO)", array(
                ":RA"=>$aluno->getRAAluno(),
                ":SENHA"=>$aluno->getSenhaAluno(),
                ":NOME"=>$aluno->getNomeAluno(),
                ":DATA_NASC"=>$aluno->getDataNascAluno(),
                ":RG"=>$aluno->getRGAluno(),
                ":CPF"=>$aluno->getCPFAluno(),
                ":ESTADO"=>$aluno->getEstadoAluno(),
                ":CIDADE"=>$aluno->getCidadeAluno(),
                ":CEP"=>$aluno->getCEPAluno(),
                ":BAIRRO"=>$aluno->getBairroAluno(),
                ":FONE"=>$aluno->getFoneAluno(),
                ":CEL"=>$aluno->getCelularAluno(),
                ":EMAIL"=>$aluno->getEmailAluno(),
                ":PORTADOR_DEF"=>$aluno->getPortadorDefAluno(),
                ":ESTAG_OBRIG"=>$aluno->getEstagObrigAluno(),
                ":SEMESTRE"=>$aluno->getSemestreAluno(),
                ":PERIODO"=>$aluno->getPeriodoAluno(),
                ":SIT_MAT"=>$aluno->getSitMatriculaAluno(),
                ":SIT_ESTAG"=>$aluno->getSitEstagioAluno(),
                ":ID_INSTITUICAO"=>$aluno->getIDInstAluno(),
                ":ID_CURSO"=>$aluno->getIDCursoAluno()));           
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
	}

    public function removerAlunoDAO($ID_ALUNO) {

        $sql = new Sql();

        try {

            $sql->result("DELETE FROM aluno WHERE ID_ALUNO = :ID_ALUNO", array(
                ":ID_ALUNO"=>$ID_ALUNO));

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function buscarNomeCursoAlunoDAO($ID_CURSO) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT CURSO.NOME FROM curso WHERE CURSO.ID_CURSO = :ID_CURSO", array(
                ":ID_CURSO"=>$ID_CURSO));
                
            return $result[0]["NOME"];
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function buscarIDCursoAlunoDAO($ID_ALUNO) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT ALUNO.ID_CURSO FROM aluno WHERE ALUNO.ID_ALUNO = :ID_ALUNO", array(
                ":ID_ALUNO"=>$ID_ALUNO));
                
            return $result[0]["ID_CURSO"];
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }
    
}
?>