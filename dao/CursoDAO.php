<?php

class CursoDAO {
    
    public static $instance;
    
    public function __construct() {
    }
    
    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new CursoDAO();
            
            return self::$instance;
    }

    public function listarCursosDAO() {

        session_start();

        $sql = new Sql();

        $ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];
            
        try {

            $result = $sql->result("SELECT *
			FROM curso
            WHERE CURSO.ID_INSTITUICAO = :ID_INSTITUICAO", array(
                ":ID_INSTITUICAO"=>$ID_INSTITUICAO));

            $cursos = array();

            foreach($result AS $curso) {
                array_push($cursos, $this->populaCurso($curso));
            }
           
            return  $cursos;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function visualizarAlterarCursoDAO($ID_CURSO) {

        session_start();

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM curso
            WHERE CURSO.ID_CURSO = :ID_CURSO", array(
                ":ID_CURSO"=>$ID_CURSO));

            $info_curso = array();

            foreach($result AS $curso) {
                array_push($info_curso, $this->populaCurso($curso));
            }

            if ($info_curso[0]->getIdInstCurso() === $_SESSION['ID_INSTITUICAO']) {
                return $info_curso;
            } else {
                Application::redirect('?controle=Curso&acao=visualizarCurso');
            }

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }
    
    private function populaCurso($curso) {

        $objeto_curso = new CursoModel();

        $objeto_curso->setIDCurso($curso['ID_CURSO']);
        $objeto_curso->setNomeCurso($curso['NOME']);
        $objeto_curso->setDuracaoCurso($curso['DURACAO']);
        $objeto_curso->setIDInstCurso($curso['ID_INSTITUICAO']);

        return $objeto_curso;
    }
    
	public function cadastrarCursoDAO($curso) {

        $sql = new Sql();
   
		try {

            $sql->result("INSERT INTO curso (NOME, DURACAO, ID_INSTITUICAO) VALUES (:NOME, :DURACAO, :ID_INSTITUICAO)", array(
                ":NOME"=>$curso->getNomeCurso(),
                ":DURACAO"=>$curso->getDuracaoCurso(),
                ":ID_INSTITUICAO"=>$curso->getIDInstCurso()));           
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }
    
    public function alterarCursoDAO($curso) {
        
        $sql = new Sql();
   
		try {

            $sql->result("UPDATE curso SET NOME = :NOME, DURACAO = :DURACAO WHERE ID_CURSO = :ID_CURSO", array(
                ":ID_CURSO"=>$curso->getIDCurso(),
                ":NOME"=>$curso->getNomeCurso(),
                ":DURACAO"=>$curso->getDuracaoCurso()));           
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function removerCursoDAO($ID_CURSO) {

        $sql = new Sql();

        try {

            $sql->result("DELETE FROM curso WHERE ID_CURSO = :ID_CURSO", array(
                ":ID_CURSO"=>$ID_CURSO));

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }
	
}
?>