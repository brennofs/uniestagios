<?php
require_once 'model/EstagioModel.php';
require_once 'model/AlunoModel.php';

class UnidadeConcedenteDAO {
    
    public static $instance;
    
    public function __construct() {
    }
    
    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new UnidadeConcedenteDAO();
            
            return self::$instance;
    }
    
    private function populaUnidadeConcedente($unidade_concedente) {

        $unidade_concedente_objeto = new UnidadeConcedenteModel();

        $unidade_concedente_objeto->setIDUnidadeConcedente($unidade_concedente['ID_UNIDADE_CONCEDENTE']);
        $unidade_concedente_objeto->setRazaoSocial($unidade_concedente['RAZAO_SOCIAL']);
        $unidade_concedente_objeto->setInscEstadual($unidade_concedente['INSCRICAO_ESTADUAL']);
        $unidade_concedente_objeto->setCNPJ($unidade_concedente['CNPJ']);
        $unidade_concedente_objeto->setCPF($unidade_concedente['CPF']);
        $unidade_concedente_objeto->setTelefone($unidade_concedente['TELEFONE']);
        $unidade_concedente_objeto->setEstado($unidade_concedente['ESTADO']);
        $unidade_concedente_objeto->setCidade($unidade_concedente['CIDADE']);
        $unidade_concedente_objeto->setCEP($unidade_concedente['CEP']);
        $unidade_concedente_objeto->setEndereco($unidade_concedente['ENDERECO']);
        $unidade_concedente_objeto->setBairro($unidade_concedente['BAIRRO']);

        return $unidade_concedente_objeto;
    }

    public function listarUnidadesConcedentesDAO() {

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM unidade_concedente", array());

            $unidades_concedentes = array();

            foreach($result AS $unidade) {
                array_push($unidades_concedentes, $this->populaUnidadeConcedente($unidade));
            }
           
            return $unidades_concedentes;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }

    }

    public function listarUnidadeConcedenteDAO($ID_UNIDADE_CONCEDENTE) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT * FROM unidade_concedente WHERE UNIDADE_CONCEDENTE.ID_UNIDADE_CONCEDENTE = :ID_UNIDADE_CONCEDENTE", array(
                ":ID_UNIDADE_CONCEDENTE"=>$ID_UNIDADE_CONCEDENTE));
                
            $unidades = array();

            foreach($result AS $unidade) {
                array_push($unidades, $this->populaUnidadeConcedente($unidade));
            }
           
            return $unidades;
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

	public function cadastrarUnidadeConcedenteDAO($unidade_concedente, $id_estagio) {

        $sql = new Sql();
   
		try {

            $sql->result("INSERT INTO unidade_concedente (RAZAO_SOCIAL, INSCRICAO_ESTADUAL, CNPJ, CPF
            , TELEFONE, ESTADO, CIDADE, CEP, ENDERECO, BAIRRO) 
            VALUES (:RAZAO_SOCIAL, :INSCRICAO_ESTADUAL, :CNPJ, :CPF, :TELEFONE, :ESTADO, :CIDADE, :CEP, :ENDERECO, :BAIRRO)", array(
                ":RAZAO_SOCIAL"=>$unidade_concedente->getRazaoSocial(),
                ":INSCRICAO_ESTADUAL"=>$unidade_concedente->getInscEstadual(),
                ":CNPJ"=>$unidade_concedente->getCNPJ(),
                ":CPF"=>$unidade_concedente->getCPF(),
                ":TELEFONE"=>$unidade_concedente->getTelefone(),
                ":ESTADO"=>$unidade_concedente->getEstado(),
                ":CIDADE"=>$unidade_concedente->getCidade(),
                ":CEP"=>$unidade_concedente->getCEP(),
                ":ENDERECO"=>$unidade_concedente->getEndereco(),
                ":BAIRRO"=>$unidade_concedente->getBairro()));
                
            $estagio = new EstagioModel;

            $estagio->setIDEstagio($id_estagio);
            $estagio->setIDUnidadeConcedenteEstagio($sql->getLastInsertedID());
    
            $estagio->AdicionarIDUnidadeConcedente($estagio);
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
	}
    
}
?>