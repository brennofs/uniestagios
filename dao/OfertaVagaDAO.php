<?php

class OfertaVagaDAO {
    
    public static $instance;
    
    public function __construct() {
    }
    
    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new OfertaVagaDAO();
            
            return self::$instance;
    }

    public function listarOfertaVagasDAO($ID_VAGA) {

        session_start();

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM oferta_vaga
            WHERE OFERTA_VAGA.ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$ID_VAGA));

            $oferta_vagas = array();

            foreach($result AS $oferta_vaga) {
                array_push($oferta_vagas, $this->populaOfertaVaga($oferta_vaga));
            }

            if (!empty($oferta_vagas)) {

                if ($oferta_vagas[0]->getIDInstituicaoOfertaVaga() === $_SESSION['ID_INSTITUICAO']) {
                    return $oferta_vagas;
                } else {
                    Application::redirect('?controle=Vaga&acao=visualizarVaga');
                }

            }

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function buscarOfertaVagasAlunoDAO($ID_INSTITUICAO, $ID_CURSO) {

        $sql = new Sql();

        try {

            $result = $sql->result("SELECT *
			FROM oferta_vaga
            WHERE OFERTA_VAGA.ID_INSTITUICAO = :ID_INSTITUICAO AND OFERTA_VAGA.ID_CURSO = :ID_CURSO", array(
                ":ID_INSTITUICAO"=>$ID_INSTITUICAO,
                ":ID_CURSO"=>$ID_CURSO));

            $oferta_vagas = array();

            foreach($result AS $oferta_vaga) {
                array_push($oferta_vagas, $this->populaOfertaVaga($oferta_vaga));
            }

            return $oferta_vagas;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function visualizarAdicionarDAO($ID_VAGA) {

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM oferta_vaga
            WHERE OFERTA_VAGA.ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$ID_VAGA));

            $info_oferta_vaga = array();

            foreach($result AS $oferta_vaga) {
                array_push($info_oferta_vaga, $this->populaOfertaVaga($oferta_vaga));
            }

            if (!empty($info_oferta_vaga)) {

                if ($info_oferta_vaga[0]->getIDInstituicaoOfertaVaga() === $_SESSION['ID_INSTITUICAO']) {
                    return $info_oferta_vaga;
                } else {
                    Application::redirect('?controle=Vaga&acao=visualizarVaga');
                }
                
            }

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    private function populaOfertaVaga($oferta_vaga) {

        $objeto_oferta_vaga = new OfertaVagaModel();

        $objeto_oferta_vaga->setIDOfertaVaga($oferta_vaga['ID_OFERTA_VAGA']);
        $objeto_oferta_vaga->setIDVaga($oferta_vaga['ID_VAGA']);
        $objeto_oferta_vaga->setIDCurso($oferta_vaga['ID_CURSO']);
        $objeto_oferta_vaga->setIDInstituicaoOfertaVaga($oferta_vaga['ID_INSTITUICAO']);
        $objeto_oferta_vaga->setNomeVaga($objeto_oferta_vaga->buscarNomeVaga($objeto_oferta_vaga->getIDVaga()));
        $objeto_oferta_vaga->setNomeCurso($objeto_oferta_vaga->buscarNomeCurso($objeto_oferta_vaga->getIDCurso()));

        return $objeto_oferta_vaga;
    }
    
	public function cadastrarOfertaVagaDAO($oferta_vaga) {

        $sql = new Sql();
   
		try {

            $sql->result("INSERT INTO oferta_vaga (ID_VAGA, ID_CURSO, ID_INSTITUICAO) VALUES (:ID_VAGA, :ID_CURSO, :ID_INSTITUICAO)", array(
                ":ID_VAGA"=>$oferta_vaga->getIDVaga(),
                ":ID_CURSO"=>$oferta_vaga->getIDCurso(),
                ":ID_INSTITUICAO"=>$oferta_vaga->getIDInstituicaoOfertaVaga()));           
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }
    
    public function adicionarOfertaVagaDAO($oferta_vaga) {

        $sql = new Sql();
   
		try {

            $sql->result("INSERT INTO oferta_vaga (ID_VAGA, ID_CURSO, ID_INSTITUICAO) VALUES (:ID_VAGA, :ID_CURSO, :ID_INSTITUICAO)", array(
                ":ID_VAGA"=>$oferta_vaga->getIDVaga(),
                ":ID_CURSO"=>$oferta_vaga->getIDCurso(),
                ":ID_INSTITUICAO"=>$oferta_vaga->getIDInstituicaoOfertaVaga()));           
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function removerOfertaVagaDAO($ID_OFERTA_VAGA) {

        $sql = new Sql();

        try {

            $ID_VAGA = $sql->result("SELECT OFERTA_VAGA.ID_VAGA FROM oferta_vaga WHERE ID_OFERTA_VAGA = :ID_OFERTA_VAGA", array(
                ":ID_OFERTA_VAGA"=>$ID_OFERTA_VAGA));

            $sql->result("DELETE FROM oferta_vaga WHERE ID_OFERTA_VAGA = :ID_OFERTA_VAGA", array(
                ":ID_OFERTA_VAGA"=>$ID_OFERTA_VAGA));
        
            Application::redirect('?controle=OfertaVaga&acao=visualizarOfertaVaga&v=' . $ID_VAGA[0]["ID_VAGA"]);

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function buscarNomeVagaDAO($ID_VAGA) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT VAGA.NOME FROM vaga WHERE VAGA.ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$ID_VAGA));
                
            return $result[0]["NOME"];
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function buscarNomeCursoDAO($ID_CURSO) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT CURSO.NOME FROM curso WHERE CURSO.ID_CURSO = :ID_CURSO", array(
                ":ID_CURSO"=>$ID_CURSO));
                
            return $result[0]["NOME"];
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }
	
}
?>