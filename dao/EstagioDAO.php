<?php

class EstagioDAO {
    
    public static $instance;
    
    public function __construct() {
    }
    
    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new EstagioDAO();
            
            return self::$instance;
    }

    private function populaEstagio($estagio) {

        $objeto_estagio = new EstagioModel();
        $objeto_aluno = new AlunoModel();

        $objeto_estagio->setIDEstagio($estagio['ID_ESTAGIO']);
        $objeto_estagio->setDuracao($estagio['DURACAO']);
        $objeto_estagio->setSitEstagio($estagio['SIT_ESTAG']);
        $objeto_estagio->setHorarioEntrada($estagio['HORARIO_ENTRADA']);
        $objeto_estagio->setHorarioSaida($estagio['HORARIO_SAIDA']);
        $objeto_estagio->setHorasSemanais($estagio['HORAS_SEMANAIS']);
        $objeto_estagio->setRepLegal($estagio['REPRESENTANTE_LEGAL']);
        $objeto_estagio->setCargoRep($estagio['CARGO_REPRESENTANTE']);
        $objeto_estagio->setSupEstagio($estagio['SUPERVISOR_ESTAGIO']);
        $objeto_estagio->setFormAcaSup($estagio['FORMACAO_ACADEMICA_SUPERVISOR']);
        $objeto_estagio->setRegProfSup($estagio['REGISTRO_PROFISSIONAL_SUPERVISOR']);
        $objeto_estagio->setOrgaoRegSup($estagio['ORGAO_REGISTRO_SUPERVISOR']);
        $objeto_estagio->setNumeroApolice($estagio['NUMERO_APOLICE']);
        $objeto_estagio->setNomeSeguradora($estagio['NOME_SEGURADORA']);
        $objeto_estagio->setValorSeguro($estagio['VALOR_SEGURO']);
        $objeto_estagio->setValorBolsa($estagio['VALOR_BOLSA']);
        $objeto_estagio->setValorValeTransporte($estagio['VALOR_VALE_TRANSPORTE']);
        $objeto_estagio->setDataInicio($estagio['DATA_INICIO']);
        $objeto_estagio->setDataFim($estagio['DATA_FIM']);
        $objeto_estagio->setDataCelebradaApartir($estagio['DATA_CELEBRADA_APARTIR']);
        $objeto_estagio->setMesesAditivo($estagio['MESES_ADITIVO']);
        $objeto_estagio->setNotificadaPor($estagio['NOTIFICADA_POR']);
        $objeto_estagio->setIDAlunoEstagio($estagio['ID_ALUNO']);
        $objeto_estagio->setIDVagaEstagio($estagio['ID_VAGA']);
        $objeto_estagio->setIDInstEstagio($estagio['ID_INSTITUICAO']);
        $objeto_estagio->setIDUnidadeConcedenteEstagio($estagio['ID_UNIDADE_CONCEDENTE']);
        $objeto_estagio->setNomeAluno($objeto_estagio->buscarNomeAluno($objeto_estagio->getIDAlunoEstagio()));
        $objeto_estagio->setNomeVaga($objeto_estagio->buscarNomeVaga($objeto_estagio->getIDVagaEstagio()));
        $objeto_estagio->setNomeCurso($objeto_estagio->buscarNomeCurso($objeto_aluno->buscarIDCursoAluno($objeto_estagio->getIDAlunoEstagio())));
        $objeto_estagio->setNomeEmpresa($objeto_estagio->buscarNomeEmpresa($objeto_estagio->getIDVagaEstagio()));

        return $objeto_estagio;
    }

    public function listarEstagiosDAO() {

        session_start();

        $sql = new Sql();

		$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];
            
        try {

            $result = $sql->result("SELECT *
			FROM estagio
            WHERE ESTAGIO.ID_INSTITUICAO = :ID_INSTITUICAO AND ESTAGIO.SIT_ESTAG = 1", array(
                ":ID_INSTITUICAO"=>$ID_INSTITUICAO));

            $estagios = array();

            foreach($result AS $estagio) {
                array_push($estagios, $this->populaEstagio($estagio));
            }
           
            return $estagios;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function listarEstagiosFinalizadosDAO() {

        session_start();

        $sql = new Sql();

		$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];
            
        try {

            $result = $sql->result("SELECT *
			FROM estagio
            WHERE ESTAGIO.ID_INSTITUICAO = :ID_INSTITUICAO AND ESTAGIO.SIT_ESTAG = 2", array(
                ":ID_INSTITUICAO"=>$ID_INSTITUICAO));

            $estagios = array();

            foreach($result AS $estagio) {
                array_push($estagios, $this->populaEstagio($estagio));
            }
           
            return $estagios;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function listarEstagiosCanceladosDAO() {

        session_start();

        $sql = new Sql();

		$ID_INSTITUICAO = $_SESSION['ID_INSTITUICAO'];
            
        try {

            $result = $sql->result("SELECT *
			FROM estagio
            WHERE ESTAGIO.ID_INSTITUICAO = :ID_INSTITUICAO AND ESTAGIO.SIT_ESTAG = 3", array(
                ":ID_INSTITUICAO"=>$ID_INSTITUICAO));

            $estagios = array();

            foreach($result AS $estagio) {
                array_push($estagios, $this->populaEstagio($estagio));
            }
           
            return $estagios;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function buscarNomeAlunoDAO($ID_ALUNO) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT ALUNO.NOME FROM aluno WHERE ALUNO.ID_ALUNO = :ID_ALUNO", array(
                ":ID_ALUNO"=>$ID_ALUNO));
                
            return $result[0]["NOME"];
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function buscarNomeVagaDAO($ID_VAGA) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT VAGA.NOME FROM vaga WHERE VAGA.ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$ID_VAGA));
                
            return $result[0]["NOME"];
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function buscarNomeCursoDAO($ID_CURSO) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT CURSO.NOME FROM curso WHERE CURSO.ID_CURSO = :ID_CURSO", array(
                ":ID_CURSO"=>$ID_CURSO));
                
            return $result[0]["NOME"];
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function buscarNomeEmpresaDAO($ID_VAGA) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT VAGA.EMPRESA FROM vaga WHERE VAGA.ID_VAGA = :ID_VAGA", array(
                ":ID_VAGA"=>$ID_VAGA));
                
            return $result[0]["EMPRESA"];
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function listarEstagioAlunoDAO($ID_ALUNO) {

        $sql = new Sql();
   
		try {

            $result = $sql->result("SELECT * FROM estagio WHERE ESTAGIO.ID_ALUNO = :ID_ALUNO AND ESTAGIO.SIT_ESTAG = 1", array(
                ":ID_ALUNO"=>$ID_ALUNO));
                
            $estagios = array();

            foreach($result AS $estagio) {
                array_push($estagios, $this->populaEstagio($estagio));
            }
           
            return $estagios;
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function visualizarAlterarEstagioDAO($ID_ESTAGIO) {

        session_start();

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM estagio
            WHERE ESTAGIO.ID_ESTAGIO = :ID_ESTAGIO", array(
                ":ID_ESTAGIO"=>$ID_ESTAGIO));

            $info_estagio = array();

            foreach($result AS $estagio) {
                array_push($info_estagio, $this->populaEstagio($estagio));
            }

            if ($info_estagio[0]->getIDInstEstagio() === $_SESSION['ID_INSTITUICAO']) {
                return $info_estagio;
            } else {
                Application::redirect('?controle=Estagio&acao=visualizarEstagio');
            }

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function cancelarEstagioDAO($ID_ESTAGIO) {

        $sql = new Sql();

        try {

            $sql->result("UPDATE estagio SET SIT_ESTAG = 3 WHERE ID_ESTAGIO = :ID_ESTAGIO", array(
                ":ID_ESTAGIO"=>$ID_ESTAGIO));

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function finalizarEstagioDAO($ID_ESTAGIO) {

        $sql = new Sql();

        try {

            $sql->result("UPDATE estagio SET SIT_ESTAG = 2 WHERE ID_ESTAGIO = :ID_ESTAGIO", array(
                ":ID_ESTAGIO"=>$ID_ESTAGIO));

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }
    
	public function cadastroEstagioDAO($estagio) {

        $sql = new Sql();
   
		try {

            $sql->result("INSERT INTO estagio (DURACAO, HORARIO_ENTRADA, HORARIO_SAIDA, HORAS_SEMANAIS, REPRESENTANTE_LEGAL, 
            CARGO_REPRESENTANTE, SUPERVISOR_ESTAGIO, FORMACAO_ACADEMICA_SUPERVISOR, REGISTRO_PROFISSIONAL_SUPERVISOR, 
            ORGAO_REGISTRO_SUPERVISOR, DATA_INICIO, DATA_FIM, SIT_ESTAG, ID_ALUNO, ID_VAGA, ID_INSTITUICAO)
            VALUES (:DURACAO, :HORARIO_ENTRADA, :HORARIO_SAIDA, :HORAS_SEMANAIS, :REPRESENTANTE_LEGAL, :CARGO_REPRESENTANTE, 
            :SUPERVISOR_ESTAGIO, :FORMACAO_ACADEMICA_SUPERVISOR, :REGISTRO_PROFISSIONAL_SUPERVISOR, :ORGAO_REGISTRO_SUPERVISOR, 
            :DATA_INICIO, :DATA_FIM, :SIT_ESTAG, :ID_ALUNO, :ID_VAGA, :ID_INSTITUICAO)", array(
                ":DURACAO"=>$estagio->getDuracao(),
                ":HORARIO_ENTRADA"=>$estagio->getHorarioEntrada(),
                ":HORARIO_SAIDA"=>$estagio->getHorarioSaida(),
                ":HORAS_SEMANAIS"=>$estagio->getHorasSemanais(),
                ":REPRESENTANTE_LEGAL"=>$estagio->getRepLegal(),
                ":CARGO_REPRESENTANTE"=>$estagio->getCargoRep(),
                ":SUPERVISOR_ESTAGIO"=>$estagio->getSupEstagio(),
                ":FORMACAO_ACADEMICA_SUPERVISOR"=>$estagio->getFormAcaSup(),
                ":REGISTRO_PROFISSIONAL_SUPERVISOR"=>$estagio->getRegProfSup(),
                ":ORGAO_REGISTRO_SUPERVISOR"=>$estagio->getOrgaoRegSup(),
                ":DATA_INICIO"=>$estagio->getDataInicio(),
                ":DATA_FIM"=>$estagio->getDataFim(),
                ":SIT_ESTAG"=>$estagio->getSitEstagio(),
                ":ID_ALUNO"=>$estagio->getIDAlunoEstagio(),
                ":ID_VAGA"=>$estagio->getIDVagaEstagio(),
                ":ID_INSTITUICAO"=>$estagio->getIDInstEstagio()));
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }

    public function alterarEstagioDAO($estagio) {
        
        $sql = new Sql();
   
		try {

            $sql->result("UPDATE estagio SET DURACAO = :DURACAO WHERE ID_ESTAGIO = :ID_ESTAGIO", array(
                ":ID_ESTAGIO"=>$estagio->getIDEstagio(),
                ":DURACAO"=>$estagio->getDuracao()));
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
    }
    
    public function AdicionarIDUnidadeConcedenteDAO($estagio) {

        $sql = new Sql();
   
		try {

            $sql->result("UPDATE estagio SET ID_UNIDADE_CONCEDENTE = :ID_UNIDADE_CONCEDENTE WHERE ID_ESTAGIO = :ID_ESTAGIO", array(
                ":ID_ESTAGIO"=>$estagio->getIDEstagio(),
                ":ID_UNIDADE_CONCEDENTE"=>$estagio->getIDUnidadeConcedenteEstagio()));
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function AdicionarDocumentoRescisaoDAO($estagio) {

        $sql = new Sql();
   
		try {

            $sql->result("UPDATE estagio SET NOTIFICADA_POR = :NOTIFICADA_POR, DATA_CELEBRADA_APARTIR = :DATA_CELEBRADA_APARTIR WHERE ID_ESTAGIO = :ID_ESTAGIO", array(
                ":ID_ESTAGIO"=>$estagio->getIDEstagio(),
                ":NOTIFICADA_POR"=>$estagio->getNotificadaPor(),
                ":DATA_CELEBRADA_APARTIR"=>$estagio->getDataCelebradaApartir()));
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function AdicionarDocumentoAditivoDAO($estagio) {

        $sql = new Sql();
   
		try {

            $sql->result("UPDATE estagio SET MESES_ADITIVO = :MESES_ADITIVO, DATA_CELEBRADA_APARTIR = :DATA_CELEBRADA_APARTIR WHERE ID_ESTAGIO = :ID_ESTAGIO", array(
                ":ID_ESTAGIO"=>$estagio->getIDEstagio(),
                ":MESES_ADITIVO"=>$estagio->getMesesAditivo(),
                ":DATA_CELEBRADA_APARTIR"=>$estagio->getDataCelebradaApartir()));
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function AdicionarDocumentoAceiteDAO($estagio) {

        $sql = new Sql();
   
		try {

            $sql->result("UPDATE estagio SET NUMERO_APOLICE = :NUMERO_APOLICE, NOME_SEGURADORA = :NOME_SEGURADORA, VALOR_SEGURO = :VALOR_SEGURO, 
            VALOR_BOLSA = :VALOR_BOLSA, VALOR_VALE_TRANSPORTE = :VALOR_VALE_TRANSPORTE, DATA_CELEBRADA_APARTIR = :DATA_CELEBRADA_APARTIR 
            WHERE ID_ESTAGIO = :ID_ESTAGIO", array(
                ":ID_ESTAGIO"=>$estagio->getIDEstagio(),
                ":NUMERO_APOLICE"=>$estagio->getNumeroApolice(),
                ":NOME_SEGURADORA"=>$estagio->getNomeSeguradora(),
                ":VALOR_SEGURO"=>$estagio->getValorSeguro(),
                ":VALOR_BOLSA"=>$estagio->getValorBolsa(),
                ":VALOR_VALE_TRANSPORTE"=>$estagio->getValorValeTransporte(),
                ":DATA_CELEBRADA_APARTIR"=>$estagio->getDataCelebradaApartir()));
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

}
?>