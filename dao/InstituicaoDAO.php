<?php

class InstituicaoDAO {
    
    public static $instance;
    
    public function __construct() {
    }
    
    public static function getInstance() {
        if (!isset(self::$instance))
            self::$instance = new InstituicaoDAO();
            
            return self::$instance;
    }
    
    public function listarInstituicoesNaoValidadasDAO() {

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM instituicao
            WHERE INSTITUICAO.INST_VALIDADA = 1", array());

            $instituicoes = array();

            foreach($result AS $instituicao) {
                array_push($instituicoes, $this->populaInstituicao($instituicao));
            }
           
            return  $instituicoes;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function listarInstituicoesRejeitadasDAO() {

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM instituicao
            WHERE INSTITUICAO.INST_VALIDADA = 2", array());

            $instituicoes = array();

            foreach($result AS $instituicao) {
                array_push($instituicoes, $this->populaInstituicao($instituicao));
            }
           
            return  $instituicoes;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    public function listarInstituicoesValidadasDAO() {

        $sql = new Sql();
            
        try {

            $result = $sql->result("SELECT *
			FROM instituicao
            WHERE INSTITUICAO.INST_VALIDADA = 3", array());

            $instituicoes = array();

            foreach($result AS $instituicao) {
                array_push($instituicoes, $this->populaInstituicao($instituicao));
            }
           
            return  $instituicoes;

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação";
            
        }
    }

    
//--------------------------------------------------------------------     
    
    private function populaInstituicao($instituicao) {

        $objeto_instituicao = new InstituicaoModel();

        $objeto_instituicao->setIDInst($instituicao['ID_INSTITUICAO']);
        $objeto_instituicao->setLoginInst($instituicao['LOGIN_INST']);
        $objeto_instituicao->setSenhaInst($instituicao['SENHA']);
        $objeto_instituicao->setNomeInst($instituicao['NOME']);
        $objeto_instituicao->setCNPJInst($instituicao['CNPJ']);
        $objeto_instituicao->setEstInst($instituicao['ESTADO']);
        $objeto_instituicao->setCidInst($instituicao['CIDADE']);
        $objeto_instituicao->setEndInst($instituicao['ENDERECO']);
        $objeto_instituicao->setNumInst($instituicao['NUMERO']);
        $objeto_instituicao->setBairroInst($instituicao['BAIRRO']);
        $objeto_instituicao->setTelefoneInst($instituicao['TELEFONE']);
        $objeto_instituicao->setDataCadastroInst($instituicao['DATA_CADASTRO']);
        $objeto_instituicao->setValidadaInst($instituicao['INST_VALIDADA']);

        return $objeto_instituicao;
    }
    
   
 //--------------------------------------------------------------------      
    
	public function cadastrarInstituicaoDAO($instituicao) {

        $sql = new Sql();
   
		try {

            $sql->result("INSERT INTO instituicao (LOGIN_INST, SENHA, NOME, CNPJ, ESTADO, CIDADE, ENDERECO, NUMERO, BAIRRO, TELEFONE, DATA_CADASTRO, INST_VALIDADA) VALUES (:LOGIN_INST, :SENHA, :NOME, :CNPJ, :ESTADO, :CIDADE, :ENDERECO, :NUMERO, :BAIRRO, :TELEFONE, :DATA_CADASTRO, :INST_VALIDADA)", array(
                ":LOGIN_INST"=>$instituicao->getLoginInst(),
                ":SENHA"=>$instituicao->getSenhaInst(),
                ":NOME"=>$instituicao->getNomeInst(),
                ":CNPJ"=>$instituicao->getCNPJInst(),
                ":ESTADO"=>$instituicao->getEstInst(),
                ":CIDADE"=>$instituicao->getCidInst(),
                ":ENDERECO"=>$instituicao->getEndInst(),
                ":NUMERO"=>$instituicao->getNumInst(),
                ":BAIRRO"=>$instituicao->getBairroInst(),
                ":TELEFONE"=>$instituicao->getTelefoneInst(),
                ":DATA_CADASTRO"=>$instituicao->getDataCadastroInst(),
                ":INST_VALIDADA"=>$instituicao->getValidadaInst()));           
            
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    
	}
   
    public function validarInstituicaoDAO($ID_INST) {

        $sql = new Sql();

        try {

            $sql->result("UPDATE instituicao SET INST_VALIDADA = 3 WHERE ID_INSTITUICAO = :ID_INSTITUICAO", array(
                ":ID_INSTITUICAO"=>$ID_INST));

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function rejeitarInstituicaoDAO($ID_INST) {

        $sql = new Sql();

        try {

            $sql->result("UPDATE instituicao SET INST_VALIDADA = 2 WHERE ID_INSTITUICAO = :ID_INSTITUICAO", array(
                ":ID_INSTITUICAO"=>$ID_INST));

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }

    public function removerInstituicaoDAO($ID_INST) {

        $sql = new Sql();

        try {

            $sql->result("DELETE FROM instituicao WHERE ID_INSTITUICAO = :ID_INSTITUICAO", array(
                ":ID_INSTITUICAO"=>$ID_INST));

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação" . $e;
           
           exit;
        }
    }
	
}
?>