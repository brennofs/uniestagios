<?php

class Sql extends PDO {

	private $conn;

	public function __construct(){

		$this->conn = new PDO("mysql:host=localhost;dbname=uniestagios", "root", "", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	}

	private function setParams($stmt, $params = array()){

		foreach ($params as $key => $value) {
			
			$this->setParam($stmt, $key, $value);
		}
	}

	private function setParam($stmt, $key, $value){

		$stmt->bindParam($key, $value);
	}

	public function query($rawQuery, $params = array()){

		$stmt = $this->conn->prepare($rawQuery);

		$this->setParams($stmt, $params);

		$stmt->execute();

		return $stmt;
	}

	public function result($rawQuery, $params = array()):array{
		
		$stmt = $this->query($rawQuery, $params);

		return $stmt->fetchAll(PDO::FETCH_ASSOC);

	}

	public function getLastInsertedID () {

		$lastInsertedID = $this->conn->lastInsertId();

		return $lastInsertedID;
	}
}

?>