<?php
 /**
 * Classe designada a filtragem de dados
 * @author DigitalDev
* @version 0.1.1
 * Diretório Pai - lib
 */
class DataFilter
{
	/**
	* Retira pontuacao da string 
	* @param string $st_data
	* @return string
	*/
	static function alphaNum( $st_data )
	{
		$st_data = preg_replace("([[:punct:]]| )",'',$st_data);
		return $st_data;
	}
	
	/**
	* Retira caracteres nao numericos da string
	* @param string $st_data
	* @return string
	*/
	static function numeric( $st_data )
	{
		$st_data = preg_replace("([[:punct:]]|[[:alpha:]]| )",'',$st_data);
		return $st_data;	
	}
	
	
	/**
	 * 
	 * Retira tags HTML / XML e adiciona "\" antes
	 * de aspas simples e aspas duplas
	 * @param unknown_type $st_string
	 */
	static function cleanString( $st_string )
	{
		
		// OUTRA VALIDACOES
		$st_string = str_replace("select","",$st_string);
		$st_string = str_replace("delete","",$st_string);
		$st_string = str_replace("update","",$st_string);
		$st_string = str_replace("grant","",$st_string);
		$st_string = str_replace("revoke","",$st_string);
		$st_string = str_replace("drop","",$st_string);
		$st_string = str_replace("from","",$st_string);
		$st_string = str_replace("alter","",$st_string);
		$st_string = str_replace("*","",$st_string);
		$st_string = str_replace("\\","",$st_string);
		$st_string = str_replace("-","",$st_string);
		$st_string = str_replace("=","",$st_string);
		$st_string = str_replace("==","",$st_string);
		$st_string = str_replace("table","",$st_string);
		$st_string = str_replace("from","",$st_string);
		$st_string = str_replace("--","",$st_string);
		$st_string = str_replace(";","",$st_string);
		
		$st_string = str_replace("'","",$st_string);
		$st_string = str_replace("<script>","",$st_string);
		$st_string = str_replace("</script>","",$st_string);
		$st_string = str_replace("<a","",$st_string);
		//$st_string = str_replace("<","&lt;",$st_string);
		//$st_string = str_replace(">","&gt;",$st_string);
		
		$st_string 	= addslashes(strip_tags($st_string));
		
		//$st_string = str_replace("&lt;","<",$st_string);
		//$st_string = str_replace("&gt;",">",$st_string);
		
		
		//##b b## BOLD
		//$st_string = str_replace("#b","<span style='font-weight: bold;'>",$st_string);
		//$st_string = str_replace("b#","</span>",$st_string);
				
			
		//##r r##
		//$st_string = str_replace("#r","<span style='color:red;'>",$st_string);
		//$st_string = str_replace("r#","</span>",$st_string);
		
		
		
		return $st_string;
	}
}
?>